<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;

class ViewController extends Controller
{

    public function show_app_calendar()
    {
        return view('app_calendar', ['title' => 'Calendar']);
    }

    public function show_app_inbox()
    {
        return view('app_inbox', ['title' => 'Mail > Inbox']);
    }

    public function show_app_inbox_compose(Request $request)
    {
        if($request->ajax()) 
            return (string) View::make('app_inbox_compose', []);

        return response()->view('errors.page_system_404_1', ['title' => '404 Error #1'], 404);
    }

    public function show_app_inbox_inbox(Request $request)
    {
        if($request->ajax()) 
            return (string) View::make('app_inbox_inbox', []);

        return response()->view('errors.page_system_404_1', ['title' => '404 Error #1'], 404);
    }

    public function show_app_inbox_reply(Request $request)
    {
        if($request->ajax()) 
            return (string) View::make('app_inbox_reply', []);

        return response()->view('errors.page_system_404_1', ['title' => '404 Error #1'], 404);
    }

    public function show_app_inbox_view(Request $request)
    {
        if($request->ajax()) 
            return (string) View::make('app_inbox_view', []);

        return response()->view('errors.page_system_404_1', ['title' => '404 Error #1'], 404);
    }

    public function show_app_ticket_config()
    {
        return view('app_ticket_config', ['title' => 'Ticket > Config']);
    }

    public function show_app_ticket_details()
    {
        return view('app_ticket_details', ['title' => 'Ticket > Details']);
    }

    public function show_app_ticket()
    {
        return view('app_ticket', ['title' => 'Ticket']);
    }

    public function show_app_ticket_staff()
    {
        return view('app_ticket_staff', ['title' => 'Ticket > Staff']);
    }

    public function show_app_todo_2()
    {
        return view('app_todo_2', ['title' => 'Todo#2']);
    }

    public function show_app_todo()
    {
        return view('app_todo', ['title' => 'Todo']);
    }

    public function show_charts_amcharts()
    {
        return view('charts_amcharts', ['title' => 'amCharts']);
    }

    public function show_charts_echarts()
    {
        return view('charts_echarts', ['title' => 'eCharts']);
    }

    public function show_charts_flotcharts()
    {
        return view('charts_flotcharts', ['title' => 'flotCharts']);
    }

    public function show_charts_flowchart()
    {
        return view('charts_flowchart', ['title' => 'flowCharts']);
    }

    public function show_charts_google()
    {
        return view('charts_google', ['title' => 'googleCharts']);
    }

    public function show_charts_highcharts()
    {
        return view('charts_highcharts', ['title' => 'highCharts']);
    }

    public function show_charts_highmaps()
    {
        return view('charts_highmaps', ['title' => 'highmapCharts']);
    }

    public function show_charts_highstock()
    {
        return view('charts_highstock', ['title' => 'highstockCharts']);
    }

    public function show_charts_morris()
    {
        return view('charts_morris', ['title' => 'morrisCharts']);
    }

    public function show_components_bootstrap_fileinput()
    {
        return view('components_bootstrap_fileinput', ['title' => 'File Input > Bootstrap']);
    }

    public function show_components_bootstrap_maxlength()
    {
        return view('components_bootstrap_maxlength', ['title' => 'Max Length > Bootstrap']);
    }

    public function show_components_bootstrap_select()
    {
        return view('components_bootstrap_select', ['title' => 'Select > Bootstrap']);
    }

    public function show_components_bootstrap_select_splitter()
    {
        return view('components_bootstrap_select_splitter', ['title' => 'Select Splitter > Bootstrap']);
    }

    public function show_components_bootstrap_switch()
    {
        return view('components_bootstrap_switch', ['title' => 'Switch > Bootstrap']);
    }

    public function show_components_bootstrap_tagsinput()
    {
        return view('components_bootstrap_tagsinput', ['title' => 'Tags Input > Bootstrap']);
    }

    public function show_components_bootstrap_touchspin()
    {
        return view('components_bootstrap_touchspin', ['title' => 'Touch Spin > Bootstrap']);
    }

    public function show_components_code_editors()
    {
        return view('components_code_editors', ['title' => 'Code Editors']);
    }

    public function show_components_color_pickers()
    {
        return view('components_color_pickers', ['title' => 'Color Pickers']);
    }

    public function show_components_context_menu()
    {
        return view('components_context_menu', ['title' => 'Context Menu']);
    }

    public function show_components_date_time_pickers()
    {
        return view('components_date_time_pickers', ['title' => 'Date Time Pickers']);
    }

    public function show_components_editors()
    {
        return view('components_editors', ['title' => 'Editors']);
    }

    public function show_components_form_tools()
    {
        return view('components_form_tools', ['title' => 'Form Tools']);
    }

    public function show_components_ion_sliders()
    {
        return view('components_ion_sliders', ['title' => 'Ion Sliders']);
    }

    public function show_components_knob_dials()
    {
        return view('components_knob_dials', ['title' => 'Knob Dials']);
    }

    public function show_components_multi_select()
    {
        return view('components_multi_select', ['title' => 'Multi-Select']);
    }

    public function show_components_noui_sliders()
    {
        return view('components_noui_sliders', ['title' => 'Noui Sliders']);
    }

    public function show_components_select2()
    {
        return view('components_select2', ['title' => 'Selects#2']);
    }

    public function show_components_typeahead()
    {
        return view('components_typeahead', ['title' => 'TypeAhead']);
    }

    public function show_dashboard_2()
    {
        return view('dashboard_2', ['title' => 'Dashboard#2']);
    }

    public function show_dashboard_3()
    {
        return view('dashboard_3', ['title' => 'Dashboard#3']);
    }

    public function show_ecommerce_index()
    {
        return view('ecommerce_index', ['title' => 'Ecommerce']);
    }

    public function show_ecommerce_orders()
    {
        return view('ecommerce_orders', ['title' => 'Orders > Ecommerce']);
    }

    public function show_ecommerce_orders_view()
    {
        return view('ecommerce_orders_view', ['title' => 'View Orders > Ecommerce']);
    }

    public function show_ecommerce_products_edit()
    {
        return view('ecommerce_products_edit', ['title' => 'Edit Products > Ecommerce']);
    }

    public function show_ecommerce_products()
    {
        return view('ecommerce_products', ['title' => 'Products > Ecommerce']);
    }

    public function show_elements_cards()
    {
        return view('elements_cards', ['title' => 'Cards']);
    }

    public function show_elements_lists()
    {
        return view('elements_lists', ['title' => 'Lists']);
    }

    public function show_elements_overlay()
    {
        return view('elements_overlay', ['title' => 'Overlay']);
    }

    public function show_elements_ribbons()
    {
        return view('elements_ribbons', ['title' => 'Ribbons']);
    }

    public function show_elements_steps()
    {
        return view('elements_steps', ['title' => 'Steps']);
    }

    public function show_form_controls()
    {
        return view('form_controls', ['title' => 'Form > Controls']);
    }

    public function show_form_controls_md()
    {
        return view('form_controls_md', ['title' => 'Form > Controls #2']);
    }

    public function show_form_dropzone()
    {
        return view('form_dropzone', ['title' => 'Form > Dropzone']);
    }

    public function show_form_editable()
    {
        return view('form_editable', ['title' => 'Form > Editable']);
    }

    public function show_form_fileupload()
    {
        return view('form_fileupload', ['title' => 'Form > File Upload']);
    }

    public function show_form_icheck()
    {
        return view('form_icheck', ['title' => 'Form > iCheck']);
    }

    public function show_form_image_crop()
    {
        return view('form_image_crop', ['title' => 'Form > Image Crop']);
    }

    public function show_form_input_mask()
    {
        return view('form_input_mask', ['title' => 'Form > Input Masks']);
    }

    public function show_form_layouts()
    {
        return view('form_layouts', ['title' => 'Form > Layouts']);
    }

    public function show_form_validation()
    {
        return view('form_validation', ['title' => 'Form > Validation']);
    }

    public function show_form_validation_md()
    {
        return view('form_validation_md', ['title' => 'Form > Validation #2']);
    }

    public function show_form_validation_states_md()
    {
        return view('form_validation_states_md', ['title' => 'Form > Validation #3']);
    }

    public function show_form_wizard()
    {
        return view('form_wizard', ['title' => 'Form > Wizard']);
    }

    public function show_index()
    {
        return view('index', ['title' => 'Metronic Laravel Template']);
    }

    public function show_layout_ajax_page_content_1()
    {
        return view('layout_ajax_page_content_1', ['title' => 'Ajax Page #1']);
    }

    public function show_layout_ajax_page_content_2()
    {
        return view('layout_ajax_page_content_2', ['title' => 'Ajax Page #2']);
    }

    public function show_layout_ajax_page_content_3()
    {
        return view('layout_ajax_page_content_3', ['title' => 'Ajax Page #3']);
    }

    public function show_layout_ajax_page_content_4()
    {
        return view('layout_ajax_page_content_4', ['title' => 'Ajax Page #4']);
    }

    public function show_layout_ajax_page_content_5()
    {
        return view('layout_ajax_page_content_5', ['title' => 'Ajax Page #5']);
    }

    public function show_layout_ajax_page()
    {
        return view('layout_ajax_page', ['title' => 'Ajax Page']);
    }

    public function show_layout_blank_page()
    {
        return view('layout_blank_page', ['title' => 'Blank Page']);
    }

    public function show_layout_disabled_menu()
    {
        return view('layout_disabled_menu', ['title' => 'Disabled Menu']);
    }

    public function show_layout_fluid_page()
    {
        return view('layout_fluid_page', ['title' => 'Fluid Page']);
    }

    public function show_layout_mega_menu_fixed()
    {
        return view('layout_mega_menu_fixed', ['title' => 'Fixed Mega Menu']);
    }

    public function show_layout_mega_menu_light()
    {
        return view('layout_mega_menu_light', ['title' => 'Light Mega Menu']);
    }

    public function show_layout_top_bar_fixed()
    {
        return view('layout_top_bar_fixed', ['title' => 'Fixed Top Bar']);
    }

    public function show_layout_top_bar_light()
    {
        return view('layout_top_bar_light', ['title' => 'Light Top Bar']);
    }

    public function show_maps_google()
    {
        return view('maps_google', ['title' => 'Google Maps']);
    }

    public function show_maps_vector()
    {
        return view('maps_vector', ['title' => 'Vector Maps']);
    }

    public function show_page_cookie_consent_1()
    {
        return view('page_cookie_consent_1', ['title' => 'Cookies Consent #1']);
    }

    public function show_page_cookie_consent_2()
    {
        return view('page_cookie_consent_2', ['title' => 'Cookies Consent #2']);
    }

    public function show_page_general_about()
    {
        return view('page_general_about', ['title' => 'About']);
    }

    public function show_page_general_blog()
    {
        return view('page_general_blog', ['title' => 'Blog']);
    }

    public function show_page_general_blog_post()
    {
        return view('page_general_blog_post', ['title' => 'Blog - Post']);
    }

    public function show_page_general_contact()
    {
        return view('page_general_contact', ['title' => 'Contact']);
    }

    public function show_page_general_faq()
    {
        return view('page_general_faq', ['title' => 'FAQ']);
    }

    public function show_page_general_help()
    {
        return view('page_general_help', ['title' => 'Help']);
    }

    public function show_page_general_invoice_2()
    {
        return view('page_general_invoice_2', ['title' => 'Invoice #2']);
    }

    public function show_page_general_invoice()
    {
        return view('page_general_invoice', ['title' => 'Invoice #1']);
    }

    public function show_page_general_portfolio_1()
    {
        return view('page_general_portfolio_1', ['title' => 'Portfolio #1']);
    }

    public function show_page_general_portfolio_2()
    {
        return view('page_general_portfolio_2', ['title' => 'Portfolio #2']);
    }

    public function show_page_general_portfolio_3()
    {
        return view('page_general_portfolio_3', ['title' => 'Portfolio #3']);
    }

    public function show_page_general_portfolio_4()
    {
        return view('page_general_portfolio_4', ['title' => 'Portfolio #4']);
    }

    public function show_page_general_pricing_2()
    {
        return view('page_general_pricing_2', ['title' => 'Pricing #2']);
    }

    public function show_page_general_pricing()
    {
        return view('page_general_pricing', ['title' => 'Pricing']);
    }

    public function show_page_general_search_2()
    {
        return view('page_general_search_2', ['title' => 'Search #2']);
    }

    public function show_page_general_search_3()
    {
        return view('page_general_search_3', ['title' => 'Search #3']);
    }

    public function show_page_general_search_4()
    {
        return view('page_general_search_4', ['title' => 'Search #4']);
    }

    public function show_page_general_search_5()
    {
        return view('page_general_search_5', ['title' => 'Search #5']);
    }

    public function show_page_general_search()
    {
        return view('page_general_search', ['title' => 'Search']);
    }

    public function show_page_system_404_1()
    {
        return view('errors.page_system_404_1', ['title' => '404 Error #1']);
    }

    public function show_page_system_404_2()
    {
        return view('errors.page_system_404_2', ['title' => '404 Error #2', 'body_cls' => 'page-404-full-page']);
    }

    public function show_page_system_404_3()
    {
        return view('errors.page_system_404_3', ['title' => '404 Error #3', 'body_cls' => 'page-404-3']);
    }

    public function show_page_system_500_1()
    {
        return view('errors.page_system_500_1', ['title' => '500 Error #1']);
    }

    public function show_page_system_500_2()
    {
        return view('errors.page_system_500_2', ['title' => '500 Error #2', 'body_cls' => 'page-500-full-page']);
    }

    public function show_page_system_coming_soon()
    {
        return view('page_system_coming_soon', ['title' => 'Coming Soon']);
    }

    public function show_page_user_lock_1()
    {
        return view('page_user_lock_1', ['title' => 'Lock Screen #1']);
    }

    public function show_page_user_lock_2()
    {
        return view('page_user_lock_2', ['title' => 'Lock Screen #2']);
    }

    public function show_page_user_login_1()
    {
        return view('page_user_login_1', ['title' => 'User Login #1', 'body_cls' => 'login']);
    }

    public function show_page_user_login_2()
    {
        return view('page_user_login_2', ['title' => 'User Login #2', 'body_cls' => 'login']);
    }

    public function show_page_user_login_3()
    {
        return view('page_user_login_3', ['title' => 'User Login #3', 'body_cls' => 'login']);
    }

    public function show_page_user_login_4()
    {
        return view('page_user_login_4', ['title' => 'User Login #4', 'body_cls' => 'login']);
    }

    public function show_page_user_login_5()
    {
        return view('page_user_login_5', ['title' => 'User Login #5', 'body_cls' => 'login']);
    }

    public function show_page_user_login_6()
    {
        return view('page_user_login_6', ['title' => 'User Login #6', 'body_cls' => 'login']);
    }

    public function show_page_user_profile_1_account()
    {
        return view('page_user_profile_1_account', ['title' => 'User Profile - Account']);
    }

    public function show_page_user_profile_1_help()
    {
        return view('page_user_profile_1_help', ['title' => 'User Profile - Help']);
    }

    public function show_page_user_profile_1()
    {
        return view('page_user_profile_1', ['title' => 'User Profile #1']);
    }

    public function show_page_user_profile_2()
    {
        return view('page_user_profile_2', ['title' => 'User Profile #2']);
    }

    public function show_portlet_ajax_content_1()
    {
        return view('portlet_ajax_content_1', ['title' => 'Ajax Portlet #1']);
    }

    public function show_portlet_ajax_content_2()
    {
        return view('portlet_ajax_content_2', ['title' => 'Ajax Portlet #2']);
    }

    public function show_portlet_ajax_content_3()
    {
        return view('portlet_ajax_content_3', ['title' => 'Ajax Portlet #3']);
    }

    public function show_portlet_ajax_content_error()
    {
        return view('portlet_ajax_content_error', ['title' => 'Ajax Portlet Error']);
    }

    public function show_portlet_ajax()
    {
        return view('portlet_ajax', ['title' => 'Ajax Portlet']);
    }

    public function show_portlet_boxed()
    {
        return view('portlet_boxed', ['title' => 'Boxed Portlet']);
    }

    public function show_portlet_draggable()
    {
        return view('portlet_draggable', ['title' => 'Draggable Portlet']);
    }

    public function show_portlet_light()
    {
        return view('portlet_light', ['title' => 'light Portlet']);
    }

    public function show_portlet_solid()
    {
        return view('portlet_solid', ['title' => 'Solid Portlet']);
    }

    public function show_table_bootstrap()
    {
        return view('table_bootstrap', ['title' => 'Bootstrap Tables']);
    }

    public function show_table_datatables_ajax()
    {
        return view('table_datatables_ajax', ['title' => 'Ajax Datatables']);
    }

    public function show_table_datatables_buttons()
    {
        return view('table_datatables_buttons', ['title' => 'Datatables Buttons']);
    }

    public function show_table_datatables_colreorder()
    {
        return view('table_datatables_colreorder', ['title' => 'Datatables Col Reorder']);
    }

    public function show_table_datatables_editable()
    {
        return view('table_datatables_editable', ['title' => 'Datatables Editable']);
    }

    public function show_table_datatables_fixedheader()
    {
        return view('table_datatables_fixedheader', ['title' => 'Datatables Fixed Header']);
    }

    public function show_table_datatables_managed()
    {
        return view('table_datatables_managed', ['title' => 'Datatables Managed']);
    }

    public function show_table_datatables_responsive()
    {
        return view('table_datatables_responsive', ['title' => 'Datatables Responsive']);
    }

    public function show_table_datatables_rowreorder()
    {
        return view('table_datatables_rowreorder', ['title' => 'Datatables Row Reorder']);
    }

    public function show_table_datatables_scroller()
    {
        return view('table_datatables_scroller', ['title' => 'Datatables Scroller']);
    }

    public function show_table_static_basic()
    {
        return view('table_static_basic', ['title' => 'Static Tables']);
    }

    public function show_table_static_responsive()
    {
        return view('table_static_responsive', ['title' => 'Static Responsive Tables']);
    }

    public function show_ui_alerts_api()
    {
        return view('ui_alerts_api', ['title' => 'Alerts API']);
    }

    public function show_ui_blockui()
    {
        return view('ui_blockui', ['title' => 'Block UI']);
    }

    public function show_ui_bootbox()
    {
        return view('ui_bootbox', ['title' => 'Bootbox']);
    }

    public function show_ui_bootstrap_growl()
    {
        return view('ui_bootstrap_growl', ['title' => 'Bootstrap Growl']);
    }

    public function show_ui_buttons()
    {
        return view('ui_buttons', ['title' => 'Buttons']);
    }

    public function show_ui_buttons_spinner()
    {
        return view('ui_buttons_spinner', ['title' => 'Buttons Spinner']);
    }

    public function show_ui_colors()
    {
        return view('ui_colors', ['title' => 'Colors']);
    }

    public function show_ui_confirmations()
    {
        return view('ui_confirmations', ['title' => 'Confirmations']);
    }

    public function show_ui_datepaginator()
    {
        return view('ui_datepaginator', ['title' => 'Date Pagination']);
    }

    public function show_ui_extended_modals_ajax_sample(Request $request)
    {
        if($request->ajax())
            return (string) View::make('ui_extended_modals_ajax_sample', []);

        return response()->view('errors.page_system_404_1', ['title' => '404 Error #1'], 404);
    }

    public function show_ui_extended_modals()
    {
        return view('ui_extended_modals', ['title' => 'Modals - Extended']);
    }

    public function show_ui_general()
    {
        return view('ui_general', ['title' => 'General']);
    }

    public function show_ui_icons()
    {
        return view('ui_icons', ['title' => 'Icons']);
    }

    public function show_ui_idle_timeout()
    {
        return view('ui_idle_timeout', ['title' => 'Idle Timeout']);
    }

    public function show_ui_modals_ajax_sample(Request $request)
    {
        if($request->ajax())
            return (string) View::make('ui_modals_ajax_sample', []);

        return response()->view('errors.page_system_404_1', ['title' => '404 Error #1'], 404);
    }

    public function show_ui_modals()
    {
        return view('ui_modals', ['title' => 'Modals > UI']);
    }

    public function show_ui_nestable()
    {
        return view('ui_nestable', ['title' => 'Nestable > UI']);
    }

    public function show_ui_notific8()
    {
        return view('ui_notific8', ['title' => 'Notific8 > UI']);
    }

    public function show_ui_page_progress_style_1()
    {
        return view('ui_page_progress_style_1', ['title' => 'Page Progress > UI']);
    }

    public function show_ui_page_progress_style_2()
    {
        return view('ui_page_progress_style_2', ['title' => 'Page Progress #2 > UI']);
    }

    public function show_ui_session_timeout()
    {
        return view('ui_session_timeout', ['title' => 'Session Timeout > UI']);
    }

    public function show_ui_socicons()
    {
        return view('ui_socicons', ['title' => 'Socicons > UI']);
    }

    public function show_ui_tabs_accordions_navs()
    {
        return view('ui_tabs_accordions_navs', ['title' => 'Tabs . Accordions . Navs > UI']);
    }

    public function show_ui_tiles()
    {
        return view('ui_tiles', ['title' => 'Tiles > UI']);
    }

    public function show_ui_timeline_2()
    {
        return view('ui_timeline_2', ['title' => 'Timeline #2 > UI']);
    }

    public function show_ui_timeline_horizontal()
    {
        return view('ui_timeline_horizontal', ['title' => 'Horizontal Timeline > UI']);
    }

    public function show_ui_timeline()
    {
        return view('ui_timeline', ['title' => 'Timeline > UI']);
    }

    public function show_ui_toastr()
    {
        return view('ui_toastr', ['title' => 'Toastr > UI']);
    }

    public function show_ui_tree()
    {
        return view('ui_tree', ['title' => 'Tree > UI']);
    }

    public function show_ui_typography()
    {
        return view('ui_typography', ['title' => 'Typography > UI']);
    }

}