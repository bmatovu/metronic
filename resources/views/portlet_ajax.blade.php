@extends ("layouts.base")

@section('extra-css')
    @parent
        {{-- BEGIN THEME GLOBAL STYLES --}}
<link href="{{ asset('css/plugins-md.min.css') }}" rel="stylesheet" type="text/css" />
{{-- END THEME GLOBAL STYLES --}}
        {{-- BEGIN PAGE LEVEL PLUGINS --}}
<link href="{{ asset('plugins/jquery-notific8/jquery.notific8.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('plugins/bootstrap-toastr/toastr.min.css') }}" rel="stylesheet" type="text/css" />
{{-- END PAGE LEVEL PLUGINS --}}
@endsection

@push('extra-js')
        {{-- BEGIN PAGE LEVEL PLUGINS --}}
<script src="{{ asset('plugins/jquery-notific8/jquery.notific8.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/bootstrap-toastr/toastr.min.js') }}" type="text/javascript"></script>
{{-- END PAGE LEVEL PLUGINS --}}
{{-- BEGIN PAGE LEVEL SCRIPTS --}}
<script src="{{ asset('pages/js/portlet-ajax.min.js') }}" type="text/javascript"></script>
{{-- END PAGE LEVEL SCRIPTS --}}
@endpush

@section('main-content')
    <div class="page-wrapper-row full-height">
        <div class="page-wrapper-middle">
            {{-- BEGIN CONTAINER --}}
            <div class="page-container">
                {{-- BEGIN CONTENT --}}
                <div class="page-content-wrapper">
                    {{-- BEGIN CONTENT BODY --}}
                    {{-- BEGIN PAGE HEAD--}}
                    <div class="page-head">
                        <div class="container">
                            {{-- BEGIN PAGE TITLE --}}
                            <div class="page-title">
                                <h1>Ajax Portlets
                                    <small>ajax portlet samples</small>
                                </h1>
                            </div>
                            {{-- END PAGE TITLE --}}
                            {{-- BEGIN PAGE TOOLBAR --}}
                            @include('includes.toolbar')
                            {{-- END PAGE TOOLBAR --}}
                        </div>
                    </div>
                    {{-- END PAGE HEAD--}}
                    {{-- BEGIN PAGE CONTENT BODY --}}
                    <div class="page-content">
                        <div class="container">
                            {{-- BEGIN PAGE BREADCRUMBS --}}
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <a href="{{ url('/') }}">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <a href="#">More</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Portlets</span>
                                </li>
                            </ul>
                            {{-- END PAGE BREADCRUMBS --}}
                            {{-- BEGIN PAGE CONTENT INNER --}}
                            <div class="page-content-inner">
                                <div class="note note-info">
                                    <h4 class="block">Ajax Portlets</h4>
                                    <p> Use <code>&lt;a href=&quot;#&quot; data-load=&quot;true&quot; data-url=&quot;portlet_ajax_content_1.html&quot; class=&quot;reload&quot;&gt;&lt;/a&gt;</code> markup to enable the ajax portlet. </p>
                                    <p> Remove <code>data-load=&quot;true&quot;</code> to prevent the initial loading of the content so the content will be loaded only upon clicking on the reload icon. </p>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        {{-- BEGIN Portlet PORTLET--}}
                                        <div class="portlet box blue">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Scrollable Content </div>
                                                <div class="tools">
                                                    <a href="javascript:;" class="collapse"> </a>
                                                    <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                    <a href="javascript:;" data-load="true" data-url="{{ url('portlet_ajax_content_1') }}" class="reload">
                                                    </a>
                                                    <a href="javascript:;" class="fullscreen"> </a>
                                                    <a href="javascript:;" class="remove"> </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body portlet-empty"> </div>
                                        </div>
                                        {{-- END Portlet PORTLET--}}
                                    </div>
                                    <div class="col-md-6">
                                        {{-- BEGIN Portlet PORTLET--}}
                                        <div class="portlet box red">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Advance Form </div>
                                                <div class="tools">
                                                    <a href="javascript:;" class="collapse"> </a>
                                                    <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                    <a href="javascript:;" class="reload" data-load="true" data-url="{{
                                                    url('portlet_ajax_content_2') }}">
                                                    </a>
                                                    <a href="javascript:;" class="fullscreen"> </a>
                                                    <a href="javascript:;" class="remove"> </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body form portlet-empty"> </div>
                                        </div>
                                        {{-- END Portlet PORTLET--}}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        {{-- BEGIN Portlet PORTLET--}}
                                        <div class="portlet">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Load On Demand </div>
                                                <div class="tools">
                                                    <a href="javascript:;" class="collapse"> </a>
                                                    <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                    <a href="javascript:;" data-url="{{ url('portlet_ajax_content_3') }}" class="reload"> </a>
                                                    <a href="javascript:;" class="fullscreen"> </a>
                                                    <a href="javascript:;" class="remove"> </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body portlet-empty"> Defualt content goes here. Click the reload icon above to reload the content from ajax source. </div>
                                        </div>
                                        {{-- END Portlet PORTLET--}}
                                    </div>
                                    <div class="col-md-6">
                                        {{-- BEGIN Portlet PORTLET--}}
                                        <div class="portlet solid green" id="my_portlet">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Custom Reload Handler </div>
                                                <div class="tools">
                                                    <a href="javascript:;" class="collapse"> </a>
                                                    <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                    <a href="javascript:;" class="reload"> </a>
                                                    <a href="javascript:;" class="fullscreen"> </a>
                                                    <a href="javascript:;" class="remove"> </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <p> Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum. Duis mollis, est non commodo luctus, nisi erat porttitor
                                                    ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. </p>
                                            </div>
                                        </div>
                                        {{-- END Portlet PORTLET--}}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        {{-- BEGIN Portlet PORTLET--}}
                                        <div class="portlet box yellow">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Error Message On Reload Using Notific8 </div>
                                                <div class="tools">
                                                    <a href="javascript:;" class="collapse"> </a>
                                                    <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                    <a href="javascript:;" data-error-display="notific8" data-url="error" class="reload"> </a>
                                                    <a href="javascript:;" class="fullscreen"> </a>
                                                    <a href="javascript:;" class="remove"> </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body portlet-empty"> Try to reload the content to see how the error message is displayed. </div>
                                        </div>
                                        {{-- END Portlet PORTLET--}}
                                    </div>
                                    <div class="col-md-6">
                                        {{-- BEGIN Portlet PORTLET--}}
                                        <div class="portlet box purple">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-gift"></i>Error Message On Reload using Toastr </div>
                                                <div class="tools">
                                                    <a href="javascript:;" class="collapse"> </a>
                                                    <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                    <a href="javascript:;" data-error-display="toastr" data-url="error" class="reload"> </a>
                                                    <a href="javascript:;" class="fullscreen"> </a>
                                                    <a href="javascript:;" class="remove"> </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body portlet-empty"> Try to reload the content to see how the error message is displayed. </div>
                                        </div>
                                        {{-- END Portlet PORTLET--}}
                                    </div>
                                </div>
                            </div>
                            {{-- END PAGE CONTENT INNER --}}
                        </div>
                    </div>
                    {{-- END PAGE CONTENT BODY --}}
                    {{-- END CONTENT BODY --}}
                </div>
                {{-- END CONTENT --}}
                {{-- BEGIN QUICK SIDEBAR --}}
                @include('includes.sidebar')
                {{-- END QUICK SIDEBAR --}}
            </div>
            {{-- END CONTAINER --}}
        </div>
    </div>
@endsection