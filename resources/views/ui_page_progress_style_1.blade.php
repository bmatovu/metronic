@extends ("layouts.base")

@section('extra-css')
    {{-- BEGIN PAGE FIRST SCRIPTS --}}
    <script src="{{ asset('plugins/pace/pace.min.js') }}" type="text/javascript"></script>
    {{-- END PAGE FIRST SCRIPTS --}}
    {{-- BEGIN PAGE TOP STYLES --}}
    <link href="{{ asset('plugins/pace/themes/pace-theme-flash.css') }}" rel="stylesheet" type="text/css"/>
    {{-- END PAGE TOP STYLES --}}
    @parent
    {{-- BEGIN THEME GLOBAL STYLES --}}
    <link href="{{ asset('css/plugins-md.min.css') }}" rel="stylesheet" type="text/css"/>
    {{-- END THEME GLOBAL STYLES --}}
@endsection

@section('main-content')
    <div class="page-wrapper-row full-height">
        <div class="page-wrapper-middle">
            {{-- BEGIN CONTAINER --}}
            <div class="page-container">
                {{-- BEGIN CONTENT --}}
                <div class="page-content-wrapper">
                    {{-- BEGIN CONTENT BODY --}}
                    {{-- BEGIN PAGE HEAD--}}
                    <div class="page-head">
                        <div class="container">
                            {{-- BEGIN PAGE TITLE --}}
                            <div class="page-title">
                                <h1>Page Progress Bar Style 1
                                    <small>page progress bar style 1</small>
                                </h1>
                            </div>
                            {{-- END PAGE TITLE --}}
                            {{-- BEGIN PAGE TOOLBAR --}}
                            @include('includes.toolbar')
                            {{-- END PAGE TOOLBAR --}}
                        </div>
                    </div>
                    {{-- END PAGE HEAD--}}
                    {{-- BEGIN PAGE CONTENT BODY --}}
                    <div class="page-content">
                        <div class="container">
                            {{-- BEGIN PAGE BREADCRUMBS --}}
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <a href="{{ url('/') }}">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <a href="#">UI Features</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Page Progress Bar - Flash</span>
                                </li>
                            </ul>
                            {{-- END PAGE BREADCRUMBS --}}
                            {{-- BEGIN PAGE CONTENT INNER --}}
                            <div class="page-content-inner">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="note note-danger note-bordered">
                                            <h4 class="block">Pace</h4>

                                            <p> An automatic web page progress bar. Pace will automatically monitor your Ajax
                                                requests, event loop lag, document ready state and elements on your page to
                                                decide on the progress. For more info check the
                                                plugin's documentation
                                                <a href="http://github.hubspot.com/pace" class="btn btn-outline green"
                                                   target="_blank"> documentation</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- END PAGE CONTENT INNER --}}
                        </div>
                    </div>
                    {{-- END PAGE CONTENT BODY --}}
                    {{-- END CONTENT BODY --}}
                </div>
                {{-- END CONTENT --}}
                {{-- BEGIN QUICK SIDEBAR --}}
                @include('includes.sidebar')
                {{-- END QUICK SIDEBAR --}}
            </div>
            {{-- END CONTAINER --}}
        </div>
    </div>
@endsection