@extends ("layouts.base")

@section('extra-css')
    @parent
    {{-- BEGIN PAGE LEVEL PLUGINS --}}
    <link href="{{ asset('plugins/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('plugins/typeahead/typeahead.css') }}" rel="stylesheet" type="text/css"/>
    {{-- END PAGE LEVEL PLUGINS --}}
    {{-- BEGIN THEME GLOBAL STYLES --}}
    <link href="{{ asset('css/plugins-md.min.css') }}" rel="stylesheet" type="text/css"/>
    {{-- END THEME GLOBAL STYLES --}}
@endsection

@push('extra-js')
{{-- BEGIN PAGE LEVEL PLUGINS --}}
<script src="{{ asset('plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/typeahead/handlebars.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/typeahead/typeahead.bundle.min.js') }}" type="text/javascript"></script>
{{-- END PAGE LEVEL PLUGINS --}}
{{-- BEGIN PAGE LEVEL SCRIPTS --}}
<script src="{{ asset('pages/js/components-bootstrap-tagsinput.min.js') }}" type="text/javascript"></script>
{{-- END PAGE LEVEL SCRIPTS --}}
@endpush

@section('main-content')
    <div class="page-wrapper-row full-height">
        <div class="page-wrapper-middle">
            {{-- BEGIN CONTAINER --}}
            <div class="page-container">
                {{-- BEGIN CONTENT --}}
                <div class="page-content-wrapper">
                    {{-- BEGIN CONTENT BODY --}}
                    {{-- BEGIN PAGE HEAD--}}
                    <div class="page-head">
                        <div class="container">
                            {{-- BEGIN PAGE TITLE --}}
                            <div class="page-title">
                                <h1>Bootstrap Tags Input
                                    <small>jquery plugin providing a twitter bootstrap user interface for managing tags
                                    </small>
                                </h1>
                            </div>
                            {{-- END PAGE TITLE --}}
                            {{-- BEGIN PAGE TOOLBAR --}}
                            @include('includes.toolbar')
                            {{-- END PAGE TOOLBAR --}}
                        </div>
                    </div>
                    {{-- END PAGE HEAD--}}
                    {{-- BEGIN PAGE CONTENT BODY --}}
                    <div class="page-content">
                        <div class="container">
                            {{-- BEGIN PAGE BREADCRUMBS --}}
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <a href="{{ url('/') }}">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <a href="#">Components</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Bootstrap Tagsinput</span>
                                </li>
                            </ul>
                            {{-- END PAGE BREADCRUMBS --}}
                            {{-- BEGIN PAGE CONTENT INNER --}}
                            <div class="page-content-inner">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="note note-success">
                                            <h3>Bootstrap Tags Input</h3>

                                            <p> jQuery plugin providing a Twitter Bootstrap user interface for managing tags.
                                                For more info please check out.
                                                <a href="http://timschlechter.github.io/bootstrap-tagsinput/examples/"
                                                   target="_blank">the official documentation</a>. </p>
                                        </div>
                                        {{-- BEGIN PORTLET--}}
                                        <div class="portlet light form-fit ">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="icon-social-dribbble font-green"></i>
                                                    <span class="caption-subject font-green bold uppercase">Bootstrap Tags Input</span>
                                                </div>
                                                <div class="actions">
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-cloud-upload"></i>
                                                    </a>
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-wrench"></i>
                                                    </a>
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-trash"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body form">
                                                {{-- BEGIN FORM--}}
                                                <form action="#" class="form-horizontal form-bordered ">
                                                    <div class="form-body">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Basic</label>

                                                            <div class="col-md-9">
                                                                <input type="text"
                                                                       value="Amsterdam,Washington,Sydney,Beijing,Cairo"
                                                                       data-role="tagsinput"></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Multi value</label>

                                                            <div class="col-md-9">
                                                                <select multiple data-role="tagsinput">
                                                                    <option value="Amsterdam">Amsterdam</option>
                                                                    <option value="Washington">Washington</option>
                                                                    <option value="Sydney">Sydney</option>
                                                                    <option value="Beijing">Beijing</option>
                                                                    <option value="Cairo">Cairo</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Objects as tags</label>

                                                            <div class="col-md-9">
                                                                <input type="text" value="" id="object_tagsinput">

                                                                <div class="margin-top-10">
                                                                    <input type="text" class="form-control input-large"
                                                                           placeholder="Tag value"
                                                                           id="object_tagsinput_value"></div>
                                                                <div class="margin-top-10">
                                                                    <select class="form-control input-large"
                                                                            id="object_tagsinput_continent">
                                                                        <option value="America">Continent...</option>
                                                                        <option value="America">America</option>
                                                                        <option value="Europe">Europe</option>
                                                                        <option value="Australia">Australia</option>
                                                                        <option value="Africa">Africa</option>
                                                                        <option value="Asia">Asia</option>
                                                                    </select>
                                                                </div>
                                                                <div class="margin-top-10">
                                                                    <input type="text" class="form-control input-large"
                                                                           placeholder="City" id="object_tagsinput_city">
                                                                </div>
                                                                <div class="margin-top-10">
                                                                    <a href="javascript:;" class="btn red"
                                                                       id="object_tagsinput_add">Add tag</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group last">
                                                            <label class="control-label col-md-3">Tag States</label>

                                                            <div class="col-md-9">
                                                                <input type="text" value="" id="state_tagsinput">

                                                                <div class="margin-top-10">
                                                                    <input type="text" class="form-control input-large"
                                                                           placeholder="Tag value"
                                                                           id="state_tagsinput_value"></div>
                                                                <div class="margin-top-10">
                                                                    <select class="form-control input-large"
                                                                            id="state_tagsinput_continent">
                                                                        <option value="America">Continent...</option>
                                                                        <option value="America">America</option>
                                                                        <option value="Europe">Europe</option>
                                                                        <option value="Australia">Australia</option>
                                                                        <option value="Africa">Africa</option>
                                                                        <option value="Asia">Asia</option>
                                                                    </select>
                                                                </div>
                                                                <div class="margin-top-10">
                                                                    <input type="text" class="form-control input-large"
                                                                           placeholder="City" id="state_tagsinput_city">
                                                                </div>
                                                                <div class="margin-top-10">
                                                                    <a href="javascript:;" class="btn red"
                                                                       id="state_tagsinput_add">Add tag</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-offset-3 col-md-9">
                                                                <button type="button" class="btn green">
                                                                    <i class="fa fa-check"></i> Submit
                                                                </button>
                                                                <button type="button" class="btn default">Cancel</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                                {{-- END FORM--}}
                                            </div>
                                        </div>
                                        {{-- END PORTLET--}}
                                    </div>
                                </div>
                            </div>
                            {{-- END PAGE CONTENT INNER --}}
                        </div>
                    </div>
                    {{-- END PAGE CONTENT BODY --}}
                    {{-- END CONTENT BODY --}}
                </div>
                {{-- END CONTENT --}}
                {{-- BEGIN QUICK SIDEBAR --}}
                @include('includes.sidebar')
                {{-- END QUICK SIDEBAR --}}
            </div>
            {{-- END CONTAINER --}}
        </div>
    </div>
@endsection