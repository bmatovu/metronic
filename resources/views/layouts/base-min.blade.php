<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]> <!--><html lang="en"> <!--<![endif]-->

{{-- BEGIN HEAD --}}
<head>
    @include('includes.head-min')
</head>
{{-- END HEAD --}}

@section('body')

    <body @if(!empty($body_cls))class="{{ $body_cls }}"@endif >

    @yield('main-content')

    @include('includes.scripts-min')

    </body>

@show

</html>