<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]> <!-->
<html lang="en"> <!--<![endif]-->

{{-- BEGIN HEAD --}}
<head>
    @include('includes.head')
</head>
{{-- END HEAD --}}

@section('body')
    <body class="page-container-bg-solid page-md @if(\Request::is('layout_top_bar_fixed')) page-header-top-fixed @endif @if(\Request::is('layout_mega_menu_fixed')) page-header-menu-fixed @endif">

    <div class="page-wrapper">

        @include('includes.header')

        @yield('main-content')

        @include('includes.footer')

    </div>

    @include('includes.scripts')

    </body>
@show

</html>