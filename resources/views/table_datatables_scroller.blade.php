@extends ("layouts.base")

@section('extra-css')
    @parent
    {{-- BEGIN PAGE LEVEL PLUGINS --}}
    <link href="{{ asset('plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet"
          type="text/css"/>
    {{-- END PAGE LEVEL PLUGINS --}}
    {{-- BEGIN THEME GLOBAL STYLES --}}
    <link href="{{ asset('css/plugins-md.min.css') }}" rel="stylesheet" type="text/css"/>
    {{-- END THEME GLOBAL STYLES --}}
@endsection

@push('extra-js')
{{-- BEGIN PAGE LEVEL PLUGINS --}}
<script src="{{ asset('js/datatable.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
{{-- END PAGE LEVEL PLUGINS --}}
{{-- BEGIN PAGE LEVEL SCRIPTS --}}
<script src="{{ asset('pages/js/table-datatables-scroller.min.js') }}" type="text/javascript"></script>
{{-- END PAGE LEVEL SCRIPTS --}}
@endpush

@section('main-content')
    <div class="page-wrapper-row full-height">
        <div class="page-wrapper-middle">
            {{-- BEGIN CONTAINER --}}
            <div class="page-container">
                {{-- BEGIN CONTENT --}}
                <div class="page-content-wrapper">
                    {{-- BEGIN CONTENT BODY --}}
                    {{-- BEGIN PAGE HEAD--}}
                    <div class="page-head">
                        <div class="container">
                            {{-- BEGIN PAGE TITLE --}}
                            <div class="page-title">
                                <h1>Scroller Extension
                                    <small>scroller extension demos</small>
                                </h1>
                            </div>
                            {{-- END PAGE TITLE --}}
                            {{-- BEGIN PAGE TOOLBAR --}}
                            <div class="page-toolbar">
                                {{-- BEGIN THEME PANEL --}}
                                <div class="btn-group btn-theme-panel">
                                    <a href="javascript:;" class="btn dropdown-toggle" data-toggle="dropdown">
                                        <i class="icon-settings"></i>
                                    </a>

                                    <div class="dropdown-menu theme-panel pull-right dropdown-custom hold-on-click">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <h3>THEME COLORS</h3>

                                                <div class="row">
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <ul class="theme-colors">
                                                            <li class="theme-color theme-color-default" data-theme="default">
                                                                <span class="theme-color-view"></span>
                                                                <span class="theme-color-name">Default</span>
                                                            </li>
                                                            <li class="theme-color theme-color-blue-hoki"
                                                                data-theme="blue-hoki">
                                                                <span class="theme-color-view"></span>
                                                                <span class="theme-color-name">Blue Hoki</span>
                                                            </li>
                                                            <li class="theme-color theme-color-blue-steel"
                                                                data-theme="blue-steel">
                                                                <span class="theme-color-view"></span>
                                                                <span class="theme-color-name">Blue Steel</span>
                                                            </li>
                                                            <li class="theme-color theme-color-yellow-orange"
                                                                data-theme="yellow-orange">
                                                                <span class="theme-color-view"></span>
                                                                <span class="theme-color-name">Orange</span>
                                                            </li>
                                                            <li class="theme-color theme-color-yellow-crusta"
                                                                data-theme="yellow-crusta">
                                                                <span class="theme-color-view"></span>
                                                                <span class="theme-color-name">Yellow Crusta</span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <ul class="theme-colors">
                                                            <li class="theme-color theme-color-green-haze"
                                                                data-theme="green-haze">
                                                                <span class="theme-color-view"></span>
                                                                <span class="theme-color-name">Green Haze</span>
                                                            </li>
                                                            <li class="theme-color theme-color-red-sunglo"
                                                                data-theme="red-sunglo">
                                                                <span class="theme-color-view"></span>
                                                                <span class="theme-color-name">Red Sunglo</span>
                                                            </li>
                                                            <li class="theme-color theme-color-red-intense"
                                                                data-theme="red-intense">
                                                                <span class="theme-color-view"></span>
                                                                <span class="theme-color-name">Red Intense</span>
                                                            </li>
                                                            <li class="theme-color theme-color-purple-plum"
                                                                data-theme="purple-plum">
                                                                <span class="theme-color-view"></span>
                                                                <span class="theme-color-name">Purple Plum</span>
                                                            </li>
                                                            <li class="theme-color theme-color-purple-studio"
                                                                data-theme="purple-studio">
                                                                <span class="theme-color-view"></span>
                                                                <span class="theme-color-name">Purple Studio</span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12 seperator">
                                                <h3>LAYOUT</h3>
                                                <ul class="theme-settings">
                                                    <li> Layout
                                                        <select class="theme-setting theme-setting-layout form-control input-sm input-small input-inline tooltips"
                                                                data-original-title="Change layout type"
                                                                data-container="body" data-placement="left">
                                                            <option value="boxed" selected="selected">Boxed</option>
                                                            <option value="fluid">Fluid</option>
                                                        </select>
                                                    </li>
                                                    <li> Top Menu Style
                                                        <select class="theme-setting theme-setting-top-menu-style form-control input-sm input-small input-inline tooltips"
                                                                data-original-title="Change top menu dropdowns style"
                                                                data-container="body"
                                                                data-placement="left">
                                                            <option value="dark" selected="selected">Dark</option>
                                                            <option value="light">Light</option>
                                                        </select>
                                                    </li>
                                                    <li> Top Menu Mode
                                                        <select class="theme-setting theme-setting-top-menu-mode form-control input-sm input-small input-inline tooltips"
                                                                data-original-title="Enable fixed(sticky) top menu"
                                                                data-container="body"
                                                                data-placement="left">
                                                            <option value="fixed">Fixed</option>
                                                            <option value="not-fixed" selected="selected">Not Fixed</option>
                                                        </select>
                                                    </li>
                                                    <li> Mega Menu Style
                                                        <select class="theme-setting theme-setting-mega-menu-style form-control input-sm input-small input-inline tooltips"
                                                                data-original-title="Change mega menu dropdowns style"
                                                                data-container="body"
                                                                data-placement="left">
                                                            <option value="dark" selected="selected">Dark</option>
                                                            <option value="light">Light</option>
                                                        </select>
                                                    </li>
                                                    <li> Mega Menu Mode
                                                        <select class="theme-setting theme-setting-mega-menu-mode form-control input-sm input-small input-inline tooltips"
                                                                data-original-title="Enable fixed(sticky) mega menu"
                                                                data-container="body"
                                                                data-placement="left">
                                                            <option value="fixed" selected="selected">Fixed</option>
                                                            <option value="not-fixed">Not Fixed</option>
                                                        </select>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- END THEME PANEL --}}
                            </div>
                            {{-- END PAGE TOOLBAR --}}
                        </div>
                    </div>
                    {{-- END PAGE HEAD--}}
                    {{-- BEGIN PAGE CONTENT BODY --}}
                    <div class="page-content">
                        <div class="container">
                            {{-- BEGIN PAGE BREADCRUMBS --}}
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <a href="{{ url('/') }}">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <a href="#">More</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <a href="#">Tables</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Datatables</span>
                                </li>
                            </ul>
                            {{-- END PAGE BREADCRUMBS --}}
                            {{-- BEGIN PAGE CONTENT INNER --}}
                            <div class="page-content-inner">
                                <div class="m-heading-1 border-green m-bordered">
                                    <h3>DataTables Scroller Extension</h3>

                                    <p> Scroller is a virtual rendering plug-in for DataTables which allows large datasets to
                                        be drawn on screen every quickly. What the virtual rendering means is that only the
                                        visible portion of the table (and a bit
                                        to either side to make the scrolling smooth) is drawn, while the scrolling container
                                        gives the visual impression that the whole table is visible. </p>

                                    <p> For more info please check out
                                        <a class="btn red btn-outline" href="http://datatables.net/extensions/scroller/"
                                           target="_blank">the official documentation</a>
                                    </p>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        {{-- BEGIN EXAMPLE TABLE PORTLET--}}
                                        <div class="portlet light ">
                                            <div class="portlet-title">
                                                <div class="caption font-dark">
                                                    <i class="icon-settings font-dark"></i>
                                                    <span class="caption-subject bold uppercase">Default Scroller</span>
                                                </div>
                                                <div class="tools"></div>
                                            </div>
                                            <div class="portlet-body">
                                                <table class="table table-striped table-bordered table-hover order-column"
                                                       id="sample_1">
                                                    <thead>
                                                    <tr>
                                                        <th>First&nbsp;name</th>
                                                        <th>Lastname</th>
                                                        <th>Position</th>
                                                        <th>Office</th>
                                                        <th>Age</th>
                                                        <th>Start&nbsp;date</th>
                                                        <th>Salary</th>
                                                        <th>Extn.</th>
                                                        <th>E-mail</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>Tiger</td>
                                                        <td>Nixon</td>
                                                        <td>System Architect</td>
                                                        <td>Edinburgh</td>
                                                        <td>61</td>
                                                        <td>2011/04/25</td>
                                                        <td>$320,800</td>
                                                        <td>5421</td>
                                                        <td>t.nixon@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Garrett</td>
                                                        <td>Winters</td>
                                                        <td>Accountant</td>
                                                        <td>Tokyo</td>
                                                        <td>63</td>
                                                        <td>2011/07/25</td>
                                                        <td>$170,750</td>
                                                        <td>8422</td>
                                                        <td>g.winters@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Ashton</td>
                                                        <td>Cox</td>
                                                        <td>Junior Technical Author</td>
                                                        <td>San Francisco</td>
                                                        <td>66</td>
                                                        <td>2009/01/12</td>
                                                        <td>$86,000</td>
                                                        <td>1562</td>
                                                        <td>a.cox@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Cedric</td>
                                                        <td>Kelly</td>
                                                        <td>Senior Javascript Developer</td>
                                                        <td>Edinburgh</td>
                                                        <td>22</td>
                                                        <td>2012/03/29</td>
                                                        <td>$433,060</td>
                                                        <td>6224</td>
                                                        <td>c.kelly@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Airi</td>
                                                        <td>Satou</td>
                                                        <td>Accountant</td>
                                                        <td>Tokyo</td>
                                                        <td>33</td>
                                                        <td>2008/11/28</td>
                                                        <td>$162,700</td>
                                                        <td>5407</td>
                                                        <td>a.satou@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Brielle</td>
                                                        <td>Williamson</td>
                                                        <td>Integration Specialist</td>
                                                        <td>New York</td>
                                                        <td>61</td>
                                                        <td>2012/12/02</td>
                                                        <td>$372,000</td>
                                                        <td>4804</td>
                                                        <td>b.williamson@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Herrod</td>
                                                        <td>Chandler</td>
                                                        <td>Sales Assistant</td>
                                                        <td>San Francisco</td>
                                                        <td>59</td>
                                                        <td>2012/08/06</td>
                                                        <td>$137,500</td>
                                                        <td>9608</td>
                                                        <td>h.chandler@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Rhona</td>
                                                        <td>Davidson</td>
                                                        <td>Integration Specialist</td>
                                                        <td>Tokyo</td>
                                                        <td>55</td>
                                                        <td>2010/10/14</td>
                                                        <td>$327,900</td>
                                                        <td>6200</td>
                                                        <td>r.davidson@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Colleen</td>
                                                        <td>Hurst</td>
                                                        <td>Javascript Developer</td>
                                                        <td>San Francisco</td>
                                                        <td>39</td>
                                                        <td>2009/09/15</td>
                                                        <td>$205,500</td>
                                                        <td>2360</td>
                                                        <td>c.hurst@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Sonya</td>
                                                        <td>Frost</td>
                                                        <td>Software Engineer</td>
                                                        <td>Edinburgh</td>
                                                        <td>23</td>
                                                        <td>2008/12/13</td>
                                                        <td>$103,600</td>
                                                        <td>1667</td>
                                                        <td>s.frost@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jena</td>
                                                        <td>Gaines</td>
                                                        <td>Office Manager</td>
                                                        <td>London</td>
                                                        <td>30</td>
                                                        <td>2008/12/19</td>
                                                        <td>$90,560</td>
                                                        <td>3814</td>
                                                        <td>j.gaines@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Quinn</td>
                                                        <td>Flynn</td>
                                                        <td>Support Lead</td>
                                                        <td>Edinburgh</td>
                                                        <td>22</td>
                                                        <td>2013/03/03</td>
                                                        <td>$342,000</td>
                                                        <td>9497</td>
                                                        <td>q.flynn@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Charde</td>
                                                        <td>Marshall</td>
                                                        <td>Regional Director</td>
                                                        <td>San Francisco</td>
                                                        <td>36</td>
                                                        <td>2008/10/16</td>
                                                        <td>$470,600</td>
                                                        <td>6741</td>
                                                        <td>c.marshall@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Haley</td>
                                                        <td>Kennedy</td>
                                                        <td>Senior Marketing Designer</td>
                                                        <td>London</td>
                                                        <td>43</td>
                                                        <td>2012/12/18</td>
                                                        <td>$313,500</td>
                                                        <td>3597</td>
                                                        <td>h.kennedy@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Tatyana</td>
                                                        <td>Fitzpatrick</td>
                                                        <td>Regional Director</td>
                                                        <td>London</td>
                                                        <td>19</td>
                                                        <td>2010/03/17</td>
                                                        <td>$385,750</td>
                                                        <td>1965</td>
                                                        <td>t.fitzpatrick@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Michael</td>
                                                        <td>Silva</td>
                                                        <td>Marketing Designer</td>
                                                        <td>London</td>
                                                        <td>66</td>
                                                        <td>2012/11/27</td>
                                                        <td>$198,500</td>
                                                        <td>1581</td>
                                                        <td>m.silva@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Paul</td>
                                                        <td>Byrd</td>
                                                        <td>Chief Financial Officer (CFO)</td>
                                                        <td>New York</td>
                                                        <td>64</td>
                                                        <td>2010/06/09</td>
                                                        <td>$725,000</td>
                                                        <td>3059</td>
                                                        <td>p.byrd@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Gloria</td>
                                                        <td>Little</td>
                                                        <td>Systems Administrator</td>
                                                        <td>New York</td>
                                                        <td>59</td>
                                                        <td>2009/04/10</td>
                                                        <td>$237,500</td>
                                                        <td>1721</td>
                                                        <td>g.little@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Bradley</td>
                                                        <td>Greer</td>
                                                        <td>Software Engineer</td>
                                                        <td>London</td>
                                                        <td>41</td>
                                                        <td>2012/10/13</td>
                                                        <td>$132,000</td>
                                                        <td>2558</td>
                                                        <td>b.greer@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Dai</td>
                                                        <td>Rios</td>
                                                        <td>Personnel Lead</td>
                                                        <td>Edinburgh</td>
                                                        <td>35</td>
                                                        <td>2012/09/26</td>
                                                        <td>$217,500</td>
                                                        <td>2290</td>
                                                        <td>d.rios@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jenette</td>
                                                        <td>Caldwell</td>
                                                        <td>Development Lead</td>
                                                        <td>New York</td>
                                                        <td>30</td>
                                                        <td>2011/09/03</td>
                                                        <td>$345,000</td>
                                                        <td>1937</td>
                                                        <td>j.caldwell@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Yuri</td>
                                                        <td>Berry</td>
                                                        <td>Chief Marketing Officer (CMO)</td>
                                                        <td>New York</td>
                                                        <td>40</td>
                                                        <td>2009/06/25</td>
                                                        <td>$675,000</td>
                                                        <td>6154</td>
                                                        <td>y.berry@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Caesar</td>
                                                        <td>Vance</td>
                                                        <td>Pre-Sales Support</td>
                                                        <td>New York</td>
                                                        <td>21</td>
                                                        <td>2011/12/12</td>
                                                        <td>$106,450</td>
                                                        <td>8330</td>
                                                        <td>c.vance@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Doris</td>
                                                        <td>Wilder</td>
                                                        <td>Sales Assistant</td>
                                                        <td>Sidney</td>
                                                        <td>23</td>
                                                        <td>2010/09/20</td>
                                                        <td>$85,600</td>
                                                        <td>3023</td>
                                                        <td>d.wilder@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Angelica</td>
                                                        <td>Ramos</td>
                                                        <td>Chief Executive Officer (CEO)</td>
                                                        <td>London</td>
                                                        <td>47</td>
                                                        <td>2009/10/09</td>
                                                        <td>$1,200,000</td>
                                                        <td>5797</td>
                                                        <td>a.ramos@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Gavin</td>
                                                        <td>Joyce</td>
                                                        <td>Developer</td>
                                                        <td>Edinburgh</td>
                                                        <td>42</td>
                                                        <td>2010/12/22</td>
                                                        <td>$92,575</td>
                                                        <td>8822</td>
                                                        <td>g.joyce@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jennifer</td>
                                                        <td>Chang</td>
                                                        <td>Regional Director</td>
                                                        <td>Singapore</td>
                                                        <td>28</td>
                                                        <td>2010/11/14</td>
                                                        <td>$357,650</td>
                                                        <td>9239</td>
                                                        <td>j.chang@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Brenden</td>
                                                        <td>Wagner</td>
                                                        <td>Software Engineer</td>
                                                        <td>San Francisco</td>
                                                        <td>28</td>
                                                        <td>2011/06/07</td>
                                                        <td>$206,850</td>
                                                        <td>1314</td>
                                                        <td>b.wagner@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Fiona</td>
                                                        <td>Green</td>
                                                        <td>Chief Operating Officer (COO)</td>
                                                        <td>San Francisco</td>
                                                        <td>48</td>
                                                        <td>2010/03/11</td>
                                                        <td>$850,000</td>
                                                        <td>2947</td>
                                                        <td>f.green@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Shou</td>
                                                        <td>Itou</td>
                                                        <td>Regional Marketing</td>
                                                        <td>Tokyo</td>
                                                        <td>20</td>
                                                        <td>2011/08/14</td>
                                                        <td>$163,000</td>
                                                        <td>8899</td>
                                                        <td>s.itou@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Michelle</td>
                                                        <td>House</td>
                                                        <td>Integration Specialist</td>
                                                        <td>Sidney</td>
                                                        <td>37</td>
                                                        <td>2011/06/02</td>
                                                        <td>$95,400</td>
                                                        <td>2769</td>
                                                        <td>m.house@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Suki</td>
                                                        <td>Burks</td>
                                                        <td>Developer</td>
                                                        <td>London</td>
                                                        <td>53</td>
                                                        <td>2009/10/22</td>
                                                        <td>$114,500</td>
                                                        <td>6832</td>
                                                        <td>s.burks@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Prescott</td>
                                                        <td>Bartlett</td>
                                                        <td>Technical Author</td>
                                                        <td>London</td>
                                                        <td>27</td>
                                                        <td>2011/05/07</td>
                                                        <td>$145,000</td>
                                                        <td>3606</td>
                                                        <td>p.bartlett@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Gavin</td>
                                                        <td>Cortez</td>
                                                        <td>Team Leader</td>
                                                        <td>San Francisco</td>
                                                        <td>22</td>
                                                        <td>2008/10/26</td>
                                                        <td>$235,500</td>
                                                        <td>2860</td>
                                                        <td>g.cortez@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Martena</td>
                                                        <td>Mccray</td>
                                                        <td>Post-Sales support</td>
                                                        <td>Edinburgh</td>
                                                        <td>46</td>
                                                        <td>2011/03/09</td>
                                                        <td>$324,050</td>
                                                        <td>8240</td>
                                                        <td>m.mccray@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Unity</td>
                                                        <td>Butler</td>
                                                        <td>Marketing Designer</td>
                                                        <td>San Francisco</td>
                                                        <td>47</td>
                                                        <td>2009/12/09</td>
                                                        <td>$85,675</td>
                                                        <td>5384</td>
                                                        <td>u.butler@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Howard</td>
                                                        <td>Hatfield</td>
                                                        <td>Office Manager</td>
                                                        <td>San Francisco</td>
                                                        <td>51</td>
                                                        <td>2008/12/16</td>
                                                        <td>$164,500</td>
                                                        <td>7031</td>
                                                        <td>h.hatfield@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Hope</td>
                                                        <td>Fuentes</td>
                                                        <td>Secretary</td>
                                                        <td>San Francisco</td>
                                                        <td>41</td>
                                                        <td>2010/02/12</td>
                                                        <td>$109,850</td>
                                                        <td>6318</td>
                                                        <td>h.fuentes@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Vivian</td>
                                                        <td>Harrell</td>
                                                        <td>Financial Controller</td>
                                                        <td>San Francisco</td>
                                                        <td>62</td>
                                                        <td>2009/02/14</td>
                                                        <td>$452,500</td>
                                                        <td>9422</td>
                                                        <td>v.harrell@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Timothy</td>
                                                        <td>Mooney</td>
                                                        <td>Office Manager</td>
                                                        <td>London</td>
                                                        <td>37</td>
                                                        <td>2008/12/11</td>
                                                        <td>$136,200</td>
                                                        <td>7580</td>
                                                        <td>t.mooney@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jackson</td>
                                                        <td>Bradshaw</td>
                                                        <td>Director</td>
                                                        <td>New York</td>
                                                        <td>65</td>
                                                        <td>2008/09/26</td>
                                                        <td>$645,750</td>
                                                        <td>1042</td>
                                                        <td>j.bradshaw@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Olivia</td>
                                                        <td>Liang</td>
                                                        <td>Support Engineer</td>
                                                        <td>Singapore</td>
                                                        <td>64</td>
                                                        <td>2011/02/03</td>
                                                        <td>$234,500</td>
                                                        <td>2120</td>
                                                        <td>o.liang@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Bruno</td>
                                                        <td>Nash</td>
                                                        <td>Software Engineer</td>
                                                        <td>London</td>
                                                        <td>38</td>
                                                        <td>2011/05/03</td>
                                                        <td>$163,500</td>
                                                        <td>6222</td>
                                                        <td>b.nash@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Sakura</td>
                                                        <td>Yamamoto</td>
                                                        <td>Support Engineer</td>
                                                        <td>Tokyo</td>
                                                        <td>37</td>
                                                        <td>2009/08/19</td>
                                                        <td>$139,575</td>
                                                        <td>9383</td>
                                                        <td>s.yamamoto@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Thor</td>
                                                        <td>Walton</td>
                                                        <td>Developer</td>
                                                        <td>New York</td>
                                                        <td>61</td>
                                                        <td>2013/08/11</td>
                                                        <td>$98,540</td>
                                                        <td>8327</td>
                                                        <td>t.walton@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Finn</td>
                                                        <td>Camacho</td>
                                                        <td>Support Engineer</td>
                                                        <td>San Francisco</td>
                                                        <td>47</td>
                                                        <td>2009/07/07</td>
                                                        <td>$87,500</td>
                                                        <td>2927</td>
                                                        <td>f.camacho@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Serge</td>
                                                        <td>Baldwin</td>
                                                        <td>Data Coordinator</td>
                                                        <td>Singapore</td>
                                                        <td>64</td>
                                                        <td>2012/04/09</td>
                                                        <td>$138,575</td>
                                                        <td>8352</td>
                                                        <td>s.baldwin@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Zenaida</td>
                                                        <td>Frank</td>
                                                        <td>Software Engineer</td>
                                                        <td>New York</td>
                                                        <td>63</td>
                                                        <td>2010/01/04</td>
                                                        <td>$125,250</td>
                                                        <td>7439</td>
                                                        <td>z.frank@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Zorita</td>
                                                        <td>Serrano</td>
                                                        <td>Software Engineer</td>
                                                        <td>San Francisco</td>
                                                        <td>56</td>
                                                        <td>2012/06/01</td>
                                                        <td>$115,000</td>
                                                        <td>4389</td>
                                                        <td>z.serrano@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jennifer</td>
                                                        <td>Acosta</td>
                                                        <td>Junior Javascript Developer</td>
                                                        <td>Edinburgh</td>
                                                        <td>43</td>
                                                        <td>2013/02/01</td>
                                                        <td>$75,650</td>
                                                        <td>3431</td>
                                                        <td>j.acosta@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Cara</td>
                                                        <td>Stevens</td>
                                                        <td>Sales Assistant</td>
                                                        <td>New York</td>
                                                        <td>46</td>
                                                        <td>2011/12/06</td>
                                                        <td>$145,600</td>
                                                        <td>3990</td>
                                                        <td>c.stevens@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Hermione</td>
                                                        <td>Butler</td>
                                                        <td>Regional Director</td>
                                                        <td>London</td>
                                                        <td>47</td>
                                                        <td>2011/03/21</td>
                                                        <td>$356,250</td>
                                                        <td>1016</td>
                                                        <td>h.butler@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Lael</td>
                                                        <td>Greer</td>
                                                        <td>Systems Administrator</td>
                                                        <td>London</td>
                                                        <td>21</td>
                                                        <td>2009/02/27</td>
                                                        <td>$103,500</td>
                                                        <td>6733</td>
                                                        <td>l.greer@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jonas</td>
                                                        <td>Alexander</td>
                                                        <td>Developer</td>
                                                        <td>San Francisco</td>
                                                        <td>30</td>
                                                        <td>2010/07/14</td>
                                                        <td>$86,500</td>
                                                        <td>8196</td>
                                                        <td>j.alexander@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Shad</td>
                                                        <td>Decker</td>
                                                        <td>Regional Director</td>
                                                        <td>Edinburgh</td>
                                                        <td>51</td>
                                                        <td>2008/11/13</td>
                                                        <td>$183,000</td>
                                                        <td>6373</td>
                                                        <td>s.decker@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Michael</td>
                                                        <td>Bruce</td>
                                                        <td>Javascript Developer</td>
                                                        <td>Singapore</td>
                                                        <td>29</td>
                                                        <td>2011/06/27</td>
                                                        <td>$183,000</td>
                                                        <td>5384</td>
                                                        <td>m.bruce@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Donna</td>
                                                        <td>Snider</td>
                                                        <td>Customer Support</td>
                                                        <td>New York</td>
                                                        <td>27</td>
                                                        <td>2011/01/25</td>
                                                        <td>$112,000</td>
                                                        <td>4226</td>
                                                        <td>d.snider@datatables.net</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        {{-- END EXAMPLE TABLE PORTLET--}}
                                        {{-- BEGIN EXAMPLE TABLE PORTLET--}}
                                        <div class="portlet box green">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-globe"></i>Server-side processing(5,000,000 rows)
                                                </div>
                                                <div class="tools"></div>
                                            </div>
                                            <div class="portlet-body">
                                                <table class="table table-striped table-bordered table-hover order-column"
                                                       id="sample_2">
                                                    <thead>
                                                    <tr>
                                                        <th>ID</th>
                                                        <th>First name</th>
                                                        <th>Last name</th>
                                                        <th>ZIP / Post code</th>
                                                        <th>Country</th>
                                                    </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                        {{-- END EXAMPLE TABLE PORTLET--}}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        {{-- BEGIN EXAMPLE TABLE PORTLET--}}
                                        <div class="portlet light ">
                                            <div class="portlet-title">
                                                <div class="caption font-green">
                                                    <i class="icon-settings font-green"></i>
                                                    <span class="caption-subject bold uppercase">Both Scrollbars</span>
                                                </div>
                                                <div class="tools"></div>
                                            </div>
                                            <div class="portlet-body table-both-scroll">
                                                <table class="table table-striped table-bordered table-hover order-column"
                                                       id="sample_3">
                                                    <thead>
                                                    <tr>
                                                        <th>First&nbsp;name</th>
                                                        <th>Lastname</th>
                                                        <th>Position</th>
                                                        <th>Office</th>
                                                        <th>Age</th>
                                                        <th>Start&nbsp;date</th>
                                                        <th>Salary</th>
                                                        <th>Extn.</th>
                                                        <th>E-mail</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>Tiger</td>
                                                        <td>Nixon</td>
                                                        <td>System Architect</td>
                                                        <td>Edinburgh</td>
                                                        <td>61</td>
                                                        <td>2011/04/25</td>
                                                        <td>$320,800</td>
                                                        <td>5421</td>
                                                        <td>t.nixon@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Garrett</td>
                                                        <td>Winters</td>
                                                        <td>Accountant</td>
                                                        <td>Tokyo</td>
                                                        <td>63</td>
                                                        <td>2011/07/25</td>
                                                        <td>$170,750</td>
                                                        <td>8422</td>
                                                        <td>g.winters@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Ashton</td>
                                                        <td>Cox</td>
                                                        <td>Junior Technical Author</td>
                                                        <td>San Francisco</td>
                                                        <td>66</td>
                                                        <td>2009/01/12</td>
                                                        <td>$86,000</td>
                                                        <td>1562</td>
                                                        <td>a.cox@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Cedric</td>
                                                        <td>Kelly</td>
                                                        <td>Senior Javascript Developer</td>
                                                        <td>Edinburgh</td>
                                                        <td>22</td>
                                                        <td>2012/03/29</td>
                                                        <td>$433,060</td>
                                                        <td>6224</td>
                                                        <td>c.kelly@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Airi</td>
                                                        <td>Satou</td>
                                                        <td>Accountant</td>
                                                        <td>Tokyo</td>
                                                        <td>33</td>
                                                        <td>2008/11/28</td>
                                                        <td>$162,700</td>
                                                        <td>5407</td>
                                                        <td>a.satou@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Brielle</td>
                                                        <td>Williamson</td>
                                                        <td>Integration Specialist</td>
                                                        <td>New York</td>
                                                        <td>61</td>
                                                        <td>2012/12/02</td>
                                                        <td>$372,000</td>
                                                        <td>4804</td>
                                                        <td>b.williamson@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Herrod</td>
                                                        <td>Chandler</td>
                                                        <td>Sales Assistant</td>
                                                        <td>San Francisco</td>
                                                        <td>59</td>
                                                        <td>2012/08/06</td>
                                                        <td>$137,500</td>
                                                        <td>9608</td>
                                                        <td>h.chandler@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Rhona</td>
                                                        <td>Davidson</td>
                                                        <td>Integration Specialist</td>
                                                        <td>Tokyo</td>
                                                        <td>55</td>
                                                        <td>2010/10/14</td>
                                                        <td>$327,900</td>
                                                        <td>6200</td>
                                                        <td>r.davidson@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Colleen</td>
                                                        <td>Hurst</td>
                                                        <td>Javascript Developer</td>
                                                        <td>San Francisco</td>
                                                        <td>39</td>
                                                        <td>2009/09/15</td>
                                                        <td>$205,500</td>
                                                        <td>2360</td>
                                                        <td>c.hurst@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Sonya</td>
                                                        <td>Frost</td>
                                                        <td>Software Engineer</td>
                                                        <td>Edinburgh</td>
                                                        <td>23</td>
                                                        <td>2008/12/13</td>
                                                        <td>$103,600</td>
                                                        <td>1667</td>
                                                        <td>s.frost@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jena</td>
                                                        <td>Gaines</td>
                                                        <td>Office Manager</td>
                                                        <td>London</td>
                                                        <td>30</td>
                                                        <td>2008/12/19</td>
                                                        <td>$90,560</td>
                                                        <td>3814</td>
                                                        <td>j.gaines@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Quinn</td>
                                                        <td>Flynn</td>
                                                        <td>Support Lead</td>
                                                        <td>Edinburgh</td>
                                                        <td>22</td>
                                                        <td>2013/03/03</td>
                                                        <td>$342,000</td>
                                                        <td>9497</td>
                                                        <td>q.flynn@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Charde</td>
                                                        <td>Marshall</td>
                                                        <td>Regional Director</td>
                                                        <td>San Francisco</td>
                                                        <td>36</td>
                                                        <td>2008/10/16</td>
                                                        <td>$470,600</td>
                                                        <td>6741</td>
                                                        <td>c.marshall@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Haley</td>
                                                        <td>Kennedy</td>
                                                        <td>Senior Marketing Designer</td>
                                                        <td>London</td>
                                                        <td>43</td>
                                                        <td>2012/12/18</td>
                                                        <td>$313,500</td>
                                                        <td>3597</td>
                                                        <td>h.kennedy@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Tatyana</td>
                                                        <td>Fitzpatrick</td>
                                                        <td>Regional Director</td>
                                                        <td>London</td>
                                                        <td>19</td>
                                                        <td>2010/03/17</td>
                                                        <td>$385,750</td>
                                                        <td>1965</td>
                                                        <td>t.fitzpatrick@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Michael</td>
                                                        <td>Silva</td>
                                                        <td>Marketing Designer</td>
                                                        <td>London</td>
                                                        <td>66</td>
                                                        <td>2012/11/27</td>
                                                        <td>$198,500</td>
                                                        <td>1581</td>
                                                        <td>m.silva@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Paul</td>
                                                        <td>Byrd</td>
                                                        <td>Chief Financial Officer (CFO)</td>
                                                        <td>New York</td>
                                                        <td>64</td>
                                                        <td>2010/06/09</td>
                                                        <td>$725,000</td>
                                                        <td>3059</td>
                                                        <td>p.byrd@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Gloria</td>
                                                        <td>Little</td>
                                                        <td>Systems Administrator</td>
                                                        <td>New York</td>
                                                        <td>59</td>
                                                        <td>2009/04/10</td>
                                                        <td>$237,500</td>
                                                        <td>1721</td>
                                                        <td>g.little@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Bradley</td>
                                                        <td>Greer</td>
                                                        <td>Software Engineer</td>
                                                        <td>London</td>
                                                        <td>41</td>
                                                        <td>2012/10/13</td>
                                                        <td>$132,000</td>
                                                        <td>2558</td>
                                                        <td>b.greer@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Dai</td>
                                                        <td>Rios</td>
                                                        <td>Personnel Lead</td>
                                                        <td>Edinburgh</td>
                                                        <td>35</td>
                                                        <td>2012/09/26</td>
                                                        <td>$217,500</td>
                                                        <td>2290</td>
                                                        <td>d.rios@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jenette</td>
                                                        <td>Caldwell</td>
                                                        <td>Development Lead</td>
                                                        <td>New York</td>
                                                        <td>30</td>
                                                        <td>2011/09/03</td>
                                                        <td>$345,000</td>
                                                        <td>1937</td>
                                                        <td>j.caldwell@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Yuri</td>
                                                        <td>Berry</td>
                                                        <td>Chief Marketing Officer (CMO)</td>
                                                        <td>New York</td>
                                                        <td>40</td>
                                                        <td>2009/06/25</td>
                                                        <td>$675,000</td>
                                                        <td>6154</td>
                                                        <td>y.berry@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Caesar</td>
                                                        <td>Vance</td>
                                                        <td>Pre-Sales Support</td>
                                                        <td>New York</td>
                                                        <td>21</td>
                                                        <td>2011/12/12</td>
                                                        <td>$106,450</td>
                                                        <td>8330</td>
                                                        <td>c.vance@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Doris</td>
                                                        <td>Wilder</td>
                                                        <td>Sales Assistant</td>
                                                        <td>Sidney</td>
                                                        <td>23</td>
                                                        <td>2010/09/20</td>
                                                        <td>$85,600</td>
                                                        <td>3023</td>
                                                        <td>d.wilder@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Angelica</td>
                                                        <td>Ramos</td>
                                                        <td>Chief Executive Officer (CEO)</td>
                                                        <td>London</td>
                                                        <td>47</td>
                                                        <td>2009/10/09</td>
                                                        <td>$1,200,000</td>
                                                        <td>5797</td>
                                                        <td>a.ramos@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Gavin</td>
                                                        <td>Joyce</td>
                                                        <td>Developer</td>
                                                        <td>Edinburgh</td>
                                                        <td>42</td>
                                                        <td>2010/12/22</td>
                                                        <td>$92,575</td>
                                                        <td>8822</td>
                                                        <td>g.joyce@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jennifer</td>
                                                        <td>Chang</td>
                                                        <td>Regional Director</td>
                                                        <td>Singapore</td>
                                                        <td>28</td>
                                                        <td>2010/11/14</td>
                                                        <td>$357,650</td>
                                                        <td>9239</td>
                                                        <td>j.chang@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Brenden</td>
                                                        <td>Wagner</td>
                                                        <td>Software Engineer</td>
                                                        <td>San Francisco</td>
                                                        <td>28</td>
                                                        <td>2011/06/07</td>
                                                        <td>$206,850</td>
                                                        <td>1314</td>
                                                        <td>b.wagner@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Fiona</td>
                                                        <td>Green</td>
                                                        <td>Chief Operating Officer (COO)</td>
                                                        <td>San Francisco</td>
                                                        <td>48</td>
                                                        <td>2010/03/11</td>
                                                        <td>$850,000</td>
                                                        <td>2947</td>
                                                        <td>f.green@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Shou</td>
                                                        <td>Itou</td>
                                                        <td>Regional Marketing</td>
                                                        <td>Tokyo</td>
                                                        <td>20</td>
                                                        <td>2011/08/14</td>
                                                        <td>$163,000</td>
                                                        <td>8899</td>
                                                        <td>s.itou@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Michelle</td>
                                                        <td>House</td>
                                                        <td>Integration Specialist</td>
                                                        <td>Sidney</td>
                                                        <td>37</td>
                                                        <td>2011/06/02</td>
                                                        <td>$95,400</td>
                                                        <td>2769</td>
                                                        <td>m.house@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Suki</td>
                                                        <td>Burks</td>
                                                        <td>Developer</td>
                                                        <td>London</td>
                                                        <td>53</td>
                                                        <td>2009/10/22</td>
                                                        <td>$114,500</td>
                                                        <td>6832</td>
                                                        <td>s.burks@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Prescott</td>
                                                        <td>Bartlett</td>
                                                        <td>Technical Author</td>
                                                        <td>London</td>
                                                        <td>27</td>
                                                        <td>2011/05/07</td>
                                                        <td>$145,000</td>
                                                        <td>3606</td>
                                                        <td>p.bartlett@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Gavin</td>
                                                        <td>Cortez</td>
                                                        <td>Team Leader</td>
                                                        <td>San Francisco</td>
                                                        <td>22</td>
                                                        <td>2008/10/26</td>
                                                        <td>$235,500</td>
                                                        <td>2860</td>
                                                        <td>g.cortez@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Martena</td>
                                                        <td>Mccray</td>
                                                        <td>Post-Sales support</td>
                                                        <td>Edinburgh</td>
                                                        <td>46</td>
                                                        <td>2011/03/09</td>
                                                        <td>$324,050</td>
                                                        <td>8240</td>
                                                        <td>m.mccray@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Unity</td>
                                                        <td>Butler</td>
                                                        <td>Marketing Designer</td>
                                                        <td>San Francisco</td>
                                                        <td>47</td>
                                                        <td>2009/12/09</td>
                                                        <td>$85,675</td>
                                                        <td>5384</td>
                                                        <td>u.butler@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Howard</td>
                                                        <td>Hatfield</td>
                                                        <td>Office Manager</td>
                                                        <td>San Francisco</td>
                                                        <td>51</td>
                                                        <td>2008/12/16</td>
                                                        <td>$164,500</td>
                                                        <td>7031</td>
                                                        <td>h.hatfield@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Hope</td>
                                                        <td>Fuentes</td>
                                                        <td>Secretary</td>
                                                        <td>San Francisco</td>
                                                        <td>41</td>
                                                        <td>2010/02/12</td>
                                                        <td>$109,850</td>
                                                        <td>6318</td>
                                                        <td>h.fuentes@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Vivian</td>
                                                        <td>Harrell</td>
                                                        <td>Financial Controller</td>
                                                        <td>San Francisco</td>
                                                        <td>62</td>
                                                        <td>2009/02/14</td>
                                                        <td>$452,500</td>
                                                        <td>9422</td>
                                                        <td>v.harrell@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Timothy</td>
                                                        <td>Mooney</td>
                                                        <td>Office Manager</td>
                                                        <td>London</td>
                                                        <td>37</td>
                                                        <td>2008/12/11</td>
                                                        <td>$136,200</td>
                                                        <td>7580</td>
                                                        <td>t.mooney@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jackson</td>
                                                        <td>Bradshaw</td>
                                                        <td>Director</td>
                                                        <td>New York</td>
                                                        <td>65</td>
                                                        <td>2008/09/26</td>
                                                        <td>$645,750</td>
                                                        <td>1042</td>
                                                        <td>j.bradshaw@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Olivia</td>
                                                        <td>Liang</td>
                                                        <td>Support Engineer</td>
                                                        <td>Singapore</td>
                                                        <td>64</td>
                                                        <td>2011/02/03</td>
                                                        <td>$234,500</td>
                                                        <td>2120</td>
                                                        <td>o.liang@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Bruno</td>
                                                        <td>Nash</td>
                                                        <td>Software Engineer</td>
                                                        <td>London</td>
                                                        <td>38</td>
                                                        <td>2011/05/03</td>
                                                        <td>$163,500</td>
                                                        <td>6222</td>
                                                        <td>b.nash@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Sakura</td>
                                                        <td>Yamamoto</td>
                                                        <td>Support Engineer</td>
                                                        <td>Tokyo</td>
                                                        <td>37</td>
                                                        <td>2009/08/19</td>
                                                        <td>$139,575</td>
                                                        <td>9383</td>
                                                        <td>s.yamamoto@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Thor</td>
                                                        <td>Walton</td>
                                                        <td>Developer</td>
                                                        <td>New York</td>
                                                        <td>61</td>
                                                        <td>2013/08/11</td>
                                                        <td>$98,540</td>
                                                        <td>8327</td>
                                                        <td>t.walton@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Finn</td>
                                                        <td>Camacho</td>
                                                        <td>Support Engineer</td>
                                                        <td>San Francisco</td>
                                                        <td>47</td>
                                                        <td>2009/07/07</td>
                                                        <td>$87,500</td>
                                                        <td>2927</td>
                                                        <td>f.camacho@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Serge</td>
                                                        <td>Baldwin</td>
                                                        <td>Data Coordinator</td>
                                                        <td>Singapore</td>
                                                        <td>64</td>
                                                        <td>2012/04/09</td>
                                                        <td>$138,575</td>
                                                        <td>8352</td>
                                                        <td>s.baldwin@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Zenaida</td>
                                                        <td>Frank</td>
                                                        <td>Software Engineer</td>
                                                        <td>New York</td>
                                                        <td>63</td>
                                                        <td>2010/01/04</td>
                                                        <td>$125,250</td>
                                                        <td>7439</td>
                                                        <td>z.frank@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Zorita</td>
                                                        <td>Serrano</td>
                                                        <td>Software Engineer</td>
                                                        <td>San Francisco</td>
                                                        <td>56</td>
                                                        <td>2012/06/01</td>
                                                        <td>$115,000</td>
                                                        <td>4389</td>
                                                        <td>z.serrano@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jennifer</td>
                                                        <td>Acosta</td>
                                                        <td>Junior Javascript Developer</td>
                                                        <td>Edinburgh</td>
                                                        <td>43</td>
                                                        <td>2013/02/01</td>
                                                        <td>$75,650</td>
                                                        <td>3431</td>
                                                        <td>j.acosta@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Cara</td>
                                                        <td>Stevens</td>
                                                        <td>Sales Assistant</td>
                                                        <td>New York</td>
                                                        <td>46</td>
                                                        <td>2011/12/06</td>
                                                        <td>$145,600</td>
                                                        <td>3990</td>
                                                        <td>c.stevens@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Hermione</td>
                                                        <td>Butler</td>
                                                        <td>Regional Director</td>
                                                        <td>London</td>
                                                        <td>47</td>
                                                        <td>2011/03/21</td>
                                                        <td>$356,250</td>
                                                        <td>1016</td>
                                                        <td>h.butler@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Lael</td>
                                                        <td>Greer</td>
                                                        <td>Systems Administrator</td>
                                                        <td>London</td>
                                                        <td>21</td>
                                                        <td>2009/02/27</td>
                                                        <td>$103,500</td>
                                                        <td>6733</td>
                                                        <td>l.greer@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jonas</td>
                                                        <td>Alexander</td>
                                                        <td>Developer</td>
                                                        <td>San Francisco</td>
                                                        <td>30</td>
                                                        <td>2010/07/14</td>
                                                        <td>$86,500</td>
                                                        <td>8196</td>
                                                        <td>j.alexander@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Shad</td>
                                                        <td>Decker</td>
                                                        <td>Regional Director</td>
                                                        <td>Edinburgh</td>
                                                        <td>51</td>
                                                        <td>2008/11/13</td>
                                                        <td>$183,000</td>
                                                        <td>6373</td>
                                                        <td>s.decker@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Michael</td>
                                                        <td>Bruce</td>
                                                        <td>Javascript Developer</td>
                                                        <td>Singapore</td>
                                                        <td>29</td>
                                                        <td>2011/06/27</td>
                                                        <td>$183,000</td>
                                                        <td>5384</td>
                                                        <td>m.bruce@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Donna</td>
                                                        <td>Snider</td>
                                                        <td>Customer Support</td>
                                                        <td>New York</td>
                                                        <td>27</td>
                                                        <td>2011/01/25</td>
                                                        <td>$112,000</td>
                                                        <td>4226</td>
                                                        <td>d.snider@datatables.net</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        {{-- BEGIN EXAMPLE TABLE PORTLET--}}
                                        <div class="portlet light ">
                                            <div class="portlet-title">
                                                <div class="caption font-red">
                                                    <i class="icon-settings font-red"></i>
                                                    <span class="caption-subject bold uppercase">Both Scrollbars</span>
                                                </div>
                                                <div class="tools"></div>
                                            </div>
                                            <div class="portlet-body table-both-scroll">
                                                <table class="table table-striped table-bordered table-hover order-column"
                                                       id="sample_4">
                                                    <thead>
                                                    <tr>
                                                        <th>First&nbsp;name</th>
                                                        <th>Lastname</th>
                                                        <th>Position</th>
                                                        <th>Office</th>
                                                        <th>Age</th>
                                                        <th>Start&nbsp;date</th>
                                                        <th>Salary</th>
                                                        <th>Extn.</th>
                                                        <th>E-mail</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>Tiger</td>
                                                        <td>Nixon</td>
                                                        <td>System Architect</td>
                                                        <td>Edinburgh</td>
                                                        <td>61</td>
                                                        <td>2011/04/25</td>
                                                        <td>$320,800</td>
                                                        <td>5421</td>
                                                        <td>t.nixon@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Garrett</td>
                                                        <td>Winters</td>
                                                        <td>Accountant</td>
                                                        <td>Tokyo</td>
                                                        <td>63</td>
                                                        <td>2011/07/25</td>
                                                        <td>$170,750</td>
                                                        <td>8422</td>
                                                        <td>g.winters@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Ashton</td>
                                                        <td>Cox</td>
                                                        <td>Junior Technical Author</td>
                                                        <td>San Francisco</td>
                                                        <td>66</td>
                                                        <td>2009/01/12</td>
                                                        <td>$86,000</td>
                                                        <td>1562</td>
                                                        <td>a.cox@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Cedric</td>
                                                        <td>Kelly</td>
                                                        <td>Senior Javascript Developer</td>
                                                        <td>Edinburgh</td>
                                                        <td>22</td>
                                                        <td>2012/03/29</td>
                                                        <td>$433,060</td>
                                                        <td>6224</td>
                                                        <td>c.kelly@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Airi</td>
                                                        <td>Satou</td>
                                                        <td>Accountant</td>
                                                        <td>Tokyo</td>
                                                        <td>33</td>
                                                        <td>2008/11/28</td>
                                                        <td>$162,700</td>
                                                        <td>5407</td>
                                                        <td>a.satou@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Brielle</td>
                                                        <td>Williamson</td>
                                                        <td>Integration Specialist</td>
                                                        <td>New York</td>
                                                        <td>61</td>
                                                        <td>2012/12/02</td>
                                                        <td>$372,000</td>
                                                        <td>4804</td>
                                                        <td>b.williamson@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Herrod</td>
                                                        <td>Chandler</td>
                                                        <td>Sales Assistant</td>
                                                        <td>San Francisco</td>
                                                        <td>59</td>
                                                        <td>2012/08/06</td>
                                                        <td>$137,500</td>
                                                        <td>9608</td>
                                                        <td>h.chandler@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Rhona</td>
                                                        <td>Davidson</td>
                                                        <td>Integration Specialist</td>
                                                        <td>Tokyo</td>
                                                        <td>55</td>
                                                        <td>2010/10/14</td>
                                                        <td>$327,900</td>
                                                        <td>6200</td>
                                                        <td>r.davidson@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Colleen</td>
                                                        <td>Hurst</td>
                                                        <td>Javascript Developer</td>
                                                        <td>San Francisco</td>
                                                        <td>39</td>
                                                        <td>2009/09/15</td>
                                                        <td>$205,500</td>
                                                        <td>2360</td>
                                                        <td>c.hurst@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Sonya</td>
                                                        <td>Frost</td>
                                                        <td>Software Engineer</td>
                                                        <td>Edinburgh</td>
                                                        <td>23</td>
                                                        <td>2008/12/13</td>
                                                        <td>$103,600</td>
                                                        <td>1667</td>
                                                        <td>s.frost@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jena</td>
                                                        <td>Gaines</td>
                                                        <td>Office Manager</td>
                                                        <td>London</td>
                                                        <td>30</td>
                                                        <td>2008/12/19</td>
                                                        <td>$90,560</td>
                                                        <td>3814</td>
                                                        <td>j.gaines@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Quinn</td>
                                                        <td>Flynn</td>
                                                        <td>Support Lead</td>
                                                        <td>Edinburgh</td>
                                                        <td>22</td>
                                                        <td>2013/03/03</td>
                                                        <td>$342,000</td>
                                                        <td>9497</td>
                                                        <td>q.flynn@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Charde</td>
                                                        <td>Marshall</td>
                                                        <td>Regional Director</td>
                                                        <td>San Francisco</td>
                                                        <td>36</td>
                                                        <td>2008/10/16</td>
                                                        <td>$470,600</td>
                                                        <td>6741</td>
                                                        <td>c.marshall@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Haley</td>
                                                        <td>Kennedy</td>
                                                        <td>Senior Marketing Designer</td>
                                                        <td>London</td>
                                                        <td>43</td>
                                                        <td>2012/12/18</td>
                                                        <td>$313,500</td>
                                                        <td>3597</td>
                                                        <td>h.kennedy@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Tatyana</td>
                                                        <td>Fitzpatrick</td>
                                                        <td>Regional Director</td>
                                                        <td>London</td>
                                                        <td>19</td>
                                                        <td>2010/03/17</td>
                                                        <td>$385,750</td>
                                                        <td>1965</td>
                                                        <td>t.fitzpatrick@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Michael</td>
                                                        <td>Silva</td>
                                                        <td>Marketing Designer</td>
                                                        <td>London</td>
                                                        <td>66</td>
                                                        <td>2012/11/27</td>
                                                        <td>$198,500</td>
                                                        <td>1581</td>
                                                        <td>m.silva@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Paul</td>
                                                        <td>Byrd</td>
                                                        <td>Chief Financial Officer (CFO)</td>
                                                        <td>New York</td>
                                                        <td>64</td>
                                                        <td>2010/06/09</td>
                                                        <td>$725,000</td>
                                                        <td>3059</td>
                                                        <td>p.byrd@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Gloria</td>
                                                        <td>Little</td>
                                                        <td>Systems Administrator</td>
                                                        <td>New York</td>
                                                        <td>59</td>
                                                        <td>2009/04/10</td>
                                                        <td>$237,500</td>
                                                        <td>1721</td>
                                                        <td>g.little@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Bradley</td>
                                                        <td>Greer</td>
                                                        <td>Software Engineer</td>
                                                        <td>London</td>
                                                        <td>41</td>
                                                        <td>2012/10/13</td>
                                                        <td>$132,000</td>
                                                        <td>2558</td>
                                                        <td>b.greer@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Dai</td>
                                                        <td>Rios</td>
                                                        <td>Personnel Lead</td>
                                                        <td>Edinburgh</td>
                                                        <td>35</td>
                                                        <td>2012/09/26</td>
                                                        <td>$217,500</td>
                                                        <td>2290</td>
                                                        <td>d.rios@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jenette</td>
                                                        <td>Caldwell</td>
                                                        <td>Development Lead</td>
                                                        <td>New York</td>
                                                        <td>30</td>
                                                        <td>2011/09/03</td>
                                                        <td>$345,000</td>
                                                        <td>1937</td>
                                                        <td>j.caldwell@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Yuri</td>
                                                        <td>Berry</td>
                                                        <td>Chief Marketing Officer (CMO)</td>
                                                        <td>New York</td>
                                                        <td>40</td>
                                                        <td>2009/06/25</td>
                                                        <td>$675,000</td>
                                                        <td>6154</td>
                                                        <td>y.berry@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Caesar</td>
                                                        <td>Vance</td>
                                                        <td>Pre-Sales Support</td>
                                                        <td>New York</td>
                                                        <td>21</td>
                                                        <td>2011/12/12</td>
                                                        <td>$106,450</td>
                                                        <td>8330</td>
                                                        <td>c.vance@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Doris</td>
                                                        <td>Wilder</td>
                                                        <td>Sales Assistant</td>
                                                        <td>Sidney</td>
                                                        <td>23</td>
                                                        <td>2010/09/20</td>
                                                        <td>$85,600</td>
                                                        <td>3023</td>
                                                        <td>d.wilder@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Angelica</td>
                                                        <td>Ramos</td>
                                                        <td>Chief Executive Officer (CEO)</td>
                                                        <td>London</td>
                                                        <td>47</td>
                                                        <td>2009/10/09</td>
                                                        <td>$1,200,000</td>
                                                        <td>5797</td>
                                                        <td>a.ramos@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Gavin</td>
                                                        <td>Joyce</td>
                                                        <td>Developer</td>
                                                        <td>Edinburgh</td>
                                                        <td>42</td>
                                                        <td>2010/12/22</td>
                                                        <td>$92,575</td>
                                                        <td>8822</td>
                                                        <td>g.joyce@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jennifer</td>
                                                        <td>Chang</td>
                                                        <td>Regional Director</td>
                                                        <td>Singapore</td>
                                                        <td>28</td>
                                                        <td>2010/11/14</td>
                                                        <td>$357,650</td>
                                                        <td>9239</td>
                                                        <td>j.chang@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Brenden</td>
                                                        <td>Wagner</td>
                                                        <td>Software Engineer</td>
                                                        <td>San Francisco</td>
                                                        <td>28</td>
                                                        <td>2011/06/07</td>
                                                        <td>$206,850</td>
                                                        <td>1314</td>
                                                        <td>b.wagner@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Fiona</td>
                                                        <td>Green</td>
                                                        <td>Chief Operating Officer (COO)</td>
                                                        <td>San Francisco</td>
                                                        <td>48</td>
                                                        <td>2010/03/11</td>
                                                        <td>$850,000</td>
                                                        <td>2947</td>
                                                        <td>f.green@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Shou</td>
                                                        <td>Itou</td>
                                                        <td>Regional Marketing</td>
                                                        <td>Tokyo</td>
                                                        <td>20</td>
                                                        <td>2011/08/14</td>
                                                        <td>$163,000</td>
                                                        <td>8899</td>
                                                        <td>s.itou@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Michelle</td>
                                                        <td>House</td>
                                                        <td>Integration Specialist</td>
                                                        <td>Sidney</td>
                                                        <td>37</td>
                                                        <td>2011/06/02</td>
                                                        <td>$95,400</td>
                                                        <td>2769</td>
                                                        <td>m.house@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Suki</td>
                                                        <td>Burks</td>
                                                        <td>Developer</td>
                                                        <td>London</td>
                                                        <td>53</td>
                                                        <td>2009/10/22</td>
                                                        <td>$114,500</td>
                                                        <td>6832</td>
                                                        <td>s.burks@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Prescott</td>
                                                        <td>Bartlett</td>
                                                        <td>Technical Author</td>
                                                        <td>London</td>
                                                        <td>27</td>
                                                        <td>2011/05/07</td>
                                                        <td>$145,000</td>
                                                        <td>3606</td>
                                                        <td>p.bartlett@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Gavin</td>
                                                        <td>Cortez</td>
                                                        <td>Team Leader</td>
                                                        <td>San Francisco</td>
                                                        <td>22</td>
                                                        <td>2008/10/26</td>
                                                        <td>$235,500</td>
                                                        <td>2860</td>
                                                        <td>g.cortez@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Martena</td>
                                                        <td>Mccray</td>
                                                        <td>Post-Sales support</td>
                                                        <td>Edinburgh</td>
                                                        <td>46</td>
                                                        <td>2011/03/09</td>
                                                        <td>$324,050</td>
                                                        <td>8240</td>
                                                        <td>m.mccray@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Unity</td>
                                                        <td>Butler</td>
                                                        <td>Marketing Designer</td>
                                                        <td>San Francisco</td>
                                                        <td>47</td>
                                                        <td>2009/12/09</td>
                                                        <td>$85,675</td>
                                                        <td>5384</td>
                                                        <td>u.butler@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Howard</td>
                                                        <td>Hatfield</td>
                                                        <td>Office Manager</td>
                                                        <td>San Francisco</td>
                                                        <td>51</td>
                                                        <td>2008/12/16</td>
                                                        <td>$164,500</td>
                                                        <td>7031</td>
                                                        <td>h.hatfield@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Hope</td>
                                                        <td>Fuentes</td>
                                                        <td>Secretary</td>
                                                        <td>San Francisco</td>
                                                        <td>41</td>
                                                        <td>2010/02/12</td>
                                                        <td>$109,850</td>
                                                        <td>6318</td>
                                                        <td>h.fuentes@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Vivian</td>
                                                        <td>Harrell</td>
                                                        <td>Financial Controller</td>
                                                        <td>San Francisco</td>
                                                        <td>62</td>
                                                        <td>2009/02/14</td>
                                                        <td>$452,500</td>
                                                        <td>9422</td>
                                                        <td>v.harrell@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Timothy</td>
                                                        <td>Mooney</td>
                                                        <td>Office Manager</td>
                                                        <td>London</td>
                                                        <td>37</td>
                                                        <td>2008/12/11</td>
                                                        <td>$136,200</td>
                                                        <td>7580</td>
                                                        <td>t.mooney@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jackson</td>
                                                        <td>Bradshaw</td>
                                                        <td>Director</td>
                                                        <td>New York</td>
                                                        <td>65</td>
                                                        <td>2008/09/26</td>
                                                        <td>$645,750</td>
                                                        <td>1042</td>
                                                        <td>j.bradshaw@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Olivia</td>
                                                        <td>Liang</td>
                                                        <td>Support Engineer</td>
                                                        <td>Singapore</td>
                                                        <td>64</td>
                                                        <td>2011/02/03</td>
                                                        <td>$234,500</td>
                                                        <td>2120</td>
                                                        <td>o.liang@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Bruno</td>
                                                        <td>Nash</td>
                                                        <td>Software Engineer</td>
                                                        <td>London</td>
                                                        <td>38</td>
                                                        <td>2011/05/03</td>
                                                        <td>$163,500</td>
                                                        <td>6222</td>
                                                        <td>b.nash@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Sakura</td>
                                                        <td>Yamamoto</td>
                                                        <td>Support Engineer</td>
                                                        <td>Tokyo</td>
                                                        <td>37</td>
                                                        <td>2009/08/19</td>
                                                        <td>$139,575</td>
                                                        <td>9383</td>
                                                        <td>s.yamamoto@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Thor</td>
                                                        <td>Walton</td>
                                                        <td>Developer</td>
                                                        <td>New York</td>
                                                        <td>61</td>
                                                        <td>2013/08/11</td>
                                                        <td>$98,540</td>
                                                        <td>8327</td>
                                                        <td>t.walton@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Finn</td>
                                                        <td>Camacho</td>
                                                        <td>Support Engineer</td>
                                                        <td>San Francisco</td>
                                                        <td>47</td>
                                                        <td>2009/07/07</td>
                                                        <td>$87,500</td>
                                                        <td>2927</td>
                                                        <td>f.camacho@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Serge</td>
                                                        <td>Baldwin</td>
                                                        <td>Data Coordinator</td>
                                                        <td>Singapore</td>
                                                        <td>64</td>
                                                        <td>2012/04/09</td>
                                                        <td>$138,575</td>
                                                        <td>8352</td>
                                                        <td>s.baldwin@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Zenaida</td>
                                                        <td>Frank</td>
                                                        <td>Software Engineer</td>
                                                        <td>New York</td>
                                                        <td>63</td>
                                                        <td>2010/01/04</td>
                                                        <td>$125,250</td>
                                                        <td>7439</td>
                                                        <td>z.frank@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Zorita</td>
                                                        <td>Serrano</td>
                                                        <td>Software Engineer</td>
                                                        <td>San Francisco</td>
                                                        <td>56</td>
                                                        <td>2012/06/01</td>
                                                        <td>$115,000</td>
                                                        <td>4389</td>
                                                        <td>z.serrano@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jennifer</td>
                                                        <td>Acosta</td>
                                                        <td>Junior Javascript Developer</td>
                                                        <td>Edinburgh</td>
                                                        <td>43</td>
                                                        <td>2013/02/01</td>
                                                        <td>$75,650</td>
                                                        <td>3431</td>
                                                        <td>j.acosta@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Cara</td>
                                                        <td>Stevens</td>
                                                        <td>Sales Assistant</td>
                                                        <td>New York</td>
                                                        <td>46</td>
                                                        <td>2011/12/06</td>
                                                        <td>$145,600</td>
                                                        <td>3990</td>
                                                        <td>c.stevens@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Hermione</td>
                                                        <td>Butler</td>
                                                        <td>Regional Director</td>
                                                        <td>London</td>
                                                        <td>47</td>
                                                        <td>2011/03/21</td>
                                                        <td>$356,250</td>
                                                        <td>1016</td>
                                                        <td>h.butler@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Lael</td>
                                                        <td>Greer</td>
                                                        <td>Systems Administrator</td>
                                                        <td>London</td>
                                                        <td>21</td>
                                                        <td>2009/02/27</td>
                                                        <td>$103,500</td>
                                                        <td>6733</td>
                                                        <td>l.greer@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Jonas</td>
                                                        <td>Alexander</td>
                                                        <td>Developer</td>
                                                        <td>San Francisco</td>
                                                        <td>30</td>
                                                        <td>2010/07/14</td>
                                                        <td>$86,500</td>
                                                        <td>8196</td>
                                                        <td>j.alexander@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Shad</td>
                                                        <td>Decker</td>
                                                        <td>Regional Director</td>
                                                        <td>Edinburgh</td>
                                                        <td>51</td>
                                                        <td>2008/11/13</td>
                                                        <td>$183,000</td>
                                                        <td>6373</td>
                                                        <td>s.decker@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Michael</td>
                                                        <td>Bruce</td>
                                                        <td>Javascript Developer</td>
                                                        <td>Singapore</td>
                                                        <td>29</td>
                                                        <td>2011/06/27</td>
                                                        <td>$183,000</td>
                                                        <td>5384</td>
                                                        <td>m.bruce@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Donna</td>
                                                        <td>Snider</td>
                                                        <td>Customer Support</td>
                                                        <td>New York</td>
                                                        <td>27</td>
                                                        <td>2011/01/25</td>
                                                        <td>$112,000</td>
                                                        <td>4226</td>
                                                        <td>d.snider@datatables.net</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- END PAGE CONTENT INNER --}}
                        </div>
                    </div>
                    {{-- END PAGE CONTENT BODY --}}
                    {{-- END CONTENT BODY --}}
                </div>
                {{-- END CONTENT --}}
                {{-- BEGIN QUICK SIDEBAR --}}
                <a href="javascript:;" class="page-quick-sidebar-toggler">
                    <i class="icon-login"></i>
                </a>

                <div class="page-quick-sidebar-wrapper" data-close-on-body-click="false">
                    <div class="page-quick-sidebar">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="javascript:;" data-target="#quick_sidebar_tab_1" data-toggle="tab"> Users
                                    <span class="badge badge-danger">2</span>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;" data-target="#quick_sidebar_tab_2" data-toggle="tab"> Alerts
                                    <span class="badge badge-success">7</span>
                                </a>
                            </li>
                            <li class="dropdown">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> More
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a href="javascript:;" data-target="#quick_sidebar_tab_3" data-toggle="tab">
                                            <i class="icon-bell"></i> Alerts </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;" data-target="#quick_sidebar_tab_3" data-toggle="tab">
                                            <i class="icon-info"></i> Notifications </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;" data-target="#quick_sidebar_tab_3" data-toggle="tab">
                                            <i class="icon-speech"></i> Activities </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="javascript:;" data-target="#quick_sidebar_tab_3" data-toggle="tab">
                                            <i class="icon-settings"></i> Settings </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active page-quick-sidebar-chat" id="quick_sidebar_tab_1">
                                <div class="page-quick-sidebar-chat-users" data-rail-color="#ddd"
                                     data-wrapper-class="page-quick-sidebar-list">
                                    <h3 class="list-heading">Staff</h3>
                                    <ul class="media-list list-items">
                                        <li class="media">
                                            <div class="media-status">
                                                <span class="badge badge-success">8</span>
                                            </div>
                                            <img class="media-object" src="{{ asset('img/layout/avatar3.jpg') }}" alt="...">

                                            <div class="media-body">
                                                <h4 class="media-heading">Bob Nilson</h4>

                                                <div class="media-heading-sub"> Project Manager</div>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <img class="media-object" src="{{ asset('img/layout/avatar1.jpg') }}" alt="...">

                                            <div class="media-body">
                                                <h4 class="media-heading">Nick Larson</h4>

                                                <div class="media-heading-sub"> Art Director</div>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <div class="media-status">
                                                <span class="badge badge-danger">3</span>
                                            </div>
                                            <img class="media-object" src="{{ asset('img/layout/avatar4.jpg') }}" alt="...">

                                            <div class="media-body">
                                                <h4 class="media-heading">Deon Hubert</h4>

                                                <div class="media-heading-sub"> CTO</div>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <img class="media-object" src="{{ asset('img/layout/avatar2.jpg') }}" alt="...">

                                            <div class="media-body">
                                                <h4 class="media-heading">Ella Wong</h4>

                                                <div class="media-heading-sub"> CEO</div>
                                            </div>
                                        </li>
                                    </ul>
                                    <h3 class="list-heading">Customers</h3>
                                    <ul class="media-list list-items">
                                        <li class="media">
                                            <div class="media-status">
                                                <span class="badge badge-warning">2</span>
                                            </div>
                                            <img class="media-object" src="{{ asset('img/layout/avatar6.jpg') }}" alt="...">

                                            <div class="media-body">
                                                <h4 class="media-heading">Lara Kunis</h4>

                                                <div class="media-heading-sub"> CEO, Loop Inc</div>
                                                <div class="media-heading-small"> Last seen 03:10 AM</div>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <div class="media-status">
                                                <span class="label label-sm label-success">new</span>
                                            </div>
                                            <img class="media-object" src="{{ asset('img/layout/avatar7.jpg') }}" alt="...">

                                            <div class="media-body">
                                                <h4 class="media-heading">Ernie Kyllonen</h4>

                                                <div class="media-heading-sub"> Project Manager,
                                                    <br> SmartBizz PTL
                                                </div>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <img class="media-object" src="{{ asset('img/layout/avatar8.jpg') }}" alt="...">

                                            <div class="media-body">
                                                <h4 class="media-heading">Lisa Stone</h4>

                                                <div class="media-heading-sub"> CTO, Keort Inc</div>
                                                <div class="media-heading-small"> Last seen 13:10 PM</div>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <div class="media-status">
                                                <span class="badge badge-success">7</span>
                                            </div>
                                            <img class="media-object" src="{{ asset('img/layout/avatar9.jpg') }}" alt="...">

                                            <div class="media-body">
                                                <h4 class="media-heading">Deon Portalatin</h4>

                                                <div class="media-heading-sub"> CFO, H&D LTD</div>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <img class="media-object" src="{{ asset('img/layout/avatar10.jpg') }}" alt="...">

                                            <div class="media-body">
                                                <h4 class="media-heading">Irina Savikova</h4>

                                                <div class="media-heading-sub"> CEO, Tizda Motors Inc</div>
                                            </div>
                                        </li>
                                        <li class="media">
                                            <div class="media-status">
                                                <span class="badge badge-danger">4</span>
                                            </div>
                                            <img class="media-object" src="{{ asset('img/layout/avatar11.jpg') }}" alt="...">

                                            <div class="media-body">
                                                <h4 class="media-heading">Maria Gomez</h4>

                                                <div class="media-heading-sub"> Manager, Infomatic Inc</div>
                                                <div class="media-heading-small"> Last seen 03:10 AM</div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="page-quick-sidebar-item">
                                    <div class="page-quick-sidebar-chat-user">
                                        <div class="page-quick-sidebar-nav">
                                            <a href="javascript:;" class="page-quick-sidebar-back-to-list">
                                                <i class="icon-arrow-left"></i>Back</a>
                                        </div>
                                        <div class="page-quick-sidebar-chat-user-messages">
                                            <div class="post out">
                                                <img class="avatar" alt="" src="{{ asset('img/layout/avatar3.jpg') }}"/>

                                                <div class="message">
                                                    <span class="arrow"></span>
                                                    <a href="javascript:;" class="name">Bob Nilson</a>
                                                    <span class="datetime">20:15</span>
                                                    <span class="body"> When could you send me the report ? </span>
                                                </div>
                                            </div>
                                            <div class="post in">
                                                <img class="avatar" alt="" src="{{ asset('img/layout/avatar2.jpg') }}"/>

                                                <div class="message">
                                                    <span class="arrow"></span>
                                                    <a href="javascript:;" class="name">Ella Wong</a>
                                                    <span class="datetime">20:15</span>
                                                    <span class="body"> Its almost done. I will be sending it shortly </span>
                                                </div>
                                            </div>
                                            <div class="post out">
                                                <img class="avatar" alt="" src="{{ asset('img/layout/avatar3.jpg') }}"/>

                                                <div class="message">
                                                    <span class="arrow"></span>
                                                    <a href="javascript:;" class="name">Bob Nilson</a>
                                                    <span class="datetime">20:15</span>
                                                    <span class="body"> Alright. Thanks! :) </span>
                                                </div>
                                            </div>
                                            <div class="post in">
                                                <img class="avatar" alt="" src="{{ asset('img/layout/avatar2.jpg') }}"/>

                                                <div class="message">
                                                    <span class="arrow"></span>
                                                    <a href="javascript:;" class="name">Ella Wong</a>
                                                    <span class="datetime">20:16</span>
                                                    <span class="body"> You are most welcome. Sorry for the delay. </span>
                                                </div>
                                            </div>
                                            <div class="post out">
                                                <img class="avatar" alt="" src="{{ asset('img/layout/avatar3.jpg') }}"/>

                                                <div class="message">
                                                    <span class="arrow"></span>
                                                    <a href="javascript:;" class="name">Bob Nilson</a>
                                                    <span class="datetime">20:17</span>
                                                    <span class="body"> No probs. Just take your time :) </span>
                                                </div>
                                            </div>
                                            <div class="post in">
                                                <img class="avatar" alt="" src="{{ asset('img/layout/avatar2.jpg') }}"/>

                                                <div class="message">
                                                    <span class="arrow"></span>
                                                    <a href="javascript:;" class="name">Ella Wong</a>
                                                    <span class="datetime">20:40</span>
                                                    <span class="body"> Alright. I just emailed it to you. </span>
                                                </div>
                                            </div>
                                            <div class="post out">
                                                <img class="avatar" alt="" src="{{ asset('img/layout/avatar3.jpg') }}"/>

                                                <div class="message">
                                                    <span class="arrow"></span>
                                                    <a href="javascript:;" class="name">Bob Nilson</a>
                                                    <span class="datetime">20:17</span>
                                                    <span class="body"> Great! Thanks. Will check it right away. </span>
                                                </div>
                                            </div>
                                            <div class="post in">
                                                <img class="avatar" alt="" src="{{ asset('img/layout/avatar2.jpg') }}"/>

                                                <div class="message">
                                                    <span class="arrow"></span>
                                                    <a href="javascript:;" class="name">Ella Wong</a>
                                                    <span class="datetime">20:40</span>
                                                    <span class="body"> Please let me know if you have any comment. </span>
                                                </div>
                                            </div>
                                            <div class="post out">
                                                <img class="avatar" alt="" src="{{ asset('img/layout/avatar3.jpg') }}"/>

                                                <div class="message">
                                                    <span class="arrow"></span>
                                                    <a href="javascript:;" class="name">Bob Nilson</a>
                                                    <span class="datetime">20:17</span>
                                                    <span class="body"> Sure. I will check and buzz you if anything needs to be corrected. </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="page-quick-sidebar-chat-user-form">
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Type a message here...">

                                                <div class="input-group-btn">
                                                    <button type="button" class="btn green">
                                                        <i class="icon-paper-clip"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane page-quick-sidebar-alerts" id="quick_sidebar_tab_2">
                                <div class="page-quick-sidebar-alerts-list">
                                    <h3 class="list-heading">General</h3>
                                    <ul class="feeds list-items">
                                        <li>
                                            <div class="col1">
                                                <div class="cont">
                                                    <div class="cont-col1">
                                                        <div class="label label-sm label-info">
                                                            <i class="fa fa-check"></i>
                                                        </div>
                                                    </div>
                                                    <div class="cont-col2">
                                                        <div class="desc"> You have 4 pending tasks.
                                                                    <span class="label label-sm label-warning "> Take action
                                                                        <i class="fa fa-share"></i>
                                                                    </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col2">
                                                <div class="date"> Just now</div>
                                            </div>
                                        </li>
                                        <li>
                                            <a href="javascript:;">
                                                <div class="col1">
                                                    <div class="cont">
                                                        <div class="cont-col1">
                                                            <div class="label label-sm label-success">
                                                                <i class="fa fa-bar-chart-o"></i>
                                                            </div>
                                                        </div>
                                                        <div class="cont-col2">
                                                            <div class="desc"> Finance Report for year 2013 has been
                                                                released.
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col2">
                                                    <div class="date"> 20 mins</div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <div class="col1">
                                                <div class="cont">
                                                    <div class="cont-col1">
                                                        <div class="label label-sm label-danger">
                                                            <i class="fa fa-user"></i>
                                                        </div>
                                                    </div>
                                                    <div class="cont-col2">
                                                        <div class="desc"> You have 5 pending membership that requires a
                                                            quick review.
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col2">
                                                <div class="date"> 24 mins</div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="col1">
                                                <div class="cont">
                                                    <div class="cont-col1">
                                                        <div class="label label-sm label-info">
                                                            <i class="fa fa-shopping-cart"></i>
                                                        </div>
                                                    </div>
                                                    <div class="cont-col2">
                                                        <div class="desc"> New order received with
                                                            <span class="label label-sm label-success"> Reference Number: DR23923 </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col2">
                                                <div class="date"> 30 mins</div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="col1">
                                                <div class="cont">
                                                    <div class="cont-col1">
                                                        <div class="label label-sm label-success">
                                                            <i class="fa fa-user"></i>
                                                        </div>
                                                    </div>
                                                    <div class="cont-col2">
                                                        <div class="desc"> You have 5 pending membership that requires a
                                                            quick review.
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col2">
                                                <div class="date"> 24 mins</div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="col1">
                                                <div class="cont">
                                                    <div class="cont-col1">
                                                        <div class="label label-sm label-info">
                                                            <i class="fa fa-bell-o"></i>
                                                        </div>
                                                    </div>
                                                    <div class="cont-col2">
                                                        <div class="desc"> Web server hardware needs to be upgraded.
                                                            <span class="label label-sm label-warning"> Overdue </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col2">
                                                <div class="date"> 2 hours</div>
                                            </div>
                                        </li>
                                        <li>
                                            <a href="javascript:;">
                                                <div class="col1">
                                                    <div class="cont">
                                                        <div class="cont-col1">
                                                            <div class="label label-sm label-default">
                                                                <i class="fa fa-briefcase"></i>
                                                            </div>
                                                        </div>
                                                        <div class="cont-col2">
                                                            <div class="desc"> IPO Report for year 2013 has been released.
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col2">
                                                    <div class="date"> 20 mins</div>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                    <h3 class="list-heading">System</h3>
                                    <ul class="feeds list-items">
                                        <li>
                                            <div class="col1">
                                                <div class="cont">
                                                    <div class="cont-col1">
                                                        <div class="label label-sm label-info">
                                                            <i class="fa fa-check"></i>
                                                        </div>
                                                    </div>
                                                    <div class="cont-col2">
                                                        <div class="desc"> You have 4 pending tasks.
                                                                    <span class="label label-sm label-warning "> Take action
                                                                        <i class="fa fa-share"></i>
                                                                    </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col2">
                                                <div class="date"> Just now</div>
                                            </div>
                                        </li>
                                        <li>
                                            <a href="javascript:;">
                                                <div class="col1">
                                                    <div class="cont">
                                                        <div class="cont-col1">
                                                            <div class="label label-sm label-danger">
                                                                <i class="fa fa-bar-chart-o"></i>
                                                            </div>
                                                        </div>
                                                        <div class="cont-col2">
                                                            <div class="desc"> Finance Report for year 2013 has been
                                                                released.
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col2">
                                                    <div class="date"> 20 mins</div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <div class="col1">
                                                <div class="cont">
                                                    <div class="cont-col1">
                                                        <div class="label label-sm label-default">
                                                            <i class="fa fa-user"></i>
                                                        </div>
                                                    </div>
                                                    <div class="cont-col2">
                                                        <div class="desc"> You have 5 pending membership that requires a
                                                            quick review.
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col2">
                                                <div class="date"> 24 mins</div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="col1">
                                                <div class="cont">
                                                    <div class="cont-col1">
                                                        <div class="label label-sm label-info">
                                                            <i class="fa fa-shopping-cart"></i>
                                                        </div>
                                                    </div>
                                                    <div class="cont-col2">
                                                        <div class="desc"> New order received with
                                                            <span class="label label-sm label-success"> Reference Number: DR23923 </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col2">
                                                <div class="date"> 30 mins</div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="col1">
                                                <div class="cont">
                                                    <div class="cont-col1">
                                                        <div class="label label-sm label-success">
                                                            <i class="fa fa-user"></i>
                                                        </div>
                                                    </div>
                                                    <div class="cont-col2">
                                                        <div class="desc"> You have 5 pending membership that requires a
                                                            quick review.
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col2">
                                                <div class="date"> 24 mins</div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="col1">
                                                <div class="cont">
                                                    <div class="cont-col1">
                                                        <div class="label label-sm label-warning">
                                                            <i class="fa fa-bell-o"></i>
                                                        </div>
                                                    </div>
                                                    <div class="cont-col2">
                                                        <div class="desc"> Web server hardware needs to be upgraded.
                                                            <span class="label label-sm label-default "> Overdue </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col2">
                                                <div class="date"> 2 hours</div>
                                            </div>
                                        </li>
                                        <li>
                                            <a href="javascript:;">
                                                <div class="col1">
                                                    <div class="cont">
                                                        <div class="cont-col1">
                                                            <div class="label label-sm label-info">
                                                                <i class="fa fa-briefcase"></i>
                                                            </div>
                                                        </div>
                                                        <div class="cont-col2">
                                                            <div class="desc"> IPO Report for year 2013 has been released.
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col2">
                                                    <div class="date"> 20 mins</div>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="tab-pane page-quick-sidebar-settings" id="quick_sidebar_tab_3">
                                <div class="page-quick-sidebar-settings-list">
                                    <h3 class="list-heading">General Settings</h3>
                                    <ul class="list-items borderless">
                                        <li> Enable Notifications
                                            <input type="checkbox" class="make-switch" checked data-size="small"
                                                   data-on-color="success" data-on-text="ON" data-off-color="default"
                                                   data-off-text="OFF"></li>
                                        <li> Allow Tracking
                                            <input type="checkbox" class="make-switch" data-size="small" data-on-color="info"
                                                   data-on-text="ON" data-off-color="default" data-off-text="OFF"></li>
                                        <li> Log Errors
                                            <input type="checkbox" class="make-switch" checked data-size="small"
                                                   data-on-color="danger" data-on-text="ON" data-off-color="default"
                                                   data-off-text="OFF"></li>
                                        <li> Auto Sumbit Issues
                                            <input type="checkbox" class="make-switch" data-size="small"
                                                   data-on-color="warning" data-on-text="ON" data-off-color="default"
                                                   data-off-text="OFF"></li>
                                        <li> Enable SMS Alerts
                                            <input type="checkbox" class="make-switch" checked data-size="small"
                                                   data-on-color="success" data-on-text="ON" data-off-color="default"
                                                   data-off-text="OFF"></li>
                                    </ul>
                                    <h3 class="list-heading">System Settings</h3>
                                    <ul class="list-items borderless">
                                        <li> Security Level
                                            <select class="form-control input-inline input-sm input-small">
                                                <option value="1">Normal</option>
                                                <option value="2" selected>Medium</option>
                                                <option value="e">High</option>
                                            </select>
                                        </li>
                                        <li> Failed Email Attempts
                                            <input class="form-control input-inline input-sm input-small" value="5"/></li>
                                        <li> Secondary SMTP Port
                                            <input class="form-control input-inline input-sm input-small" value="3560"/></li>
                                        <li> Notify On System Error
                                            <input type="checkbox" class="make-switch" checked data-size="small"
                                                   data-on-color="danger" data-on-text="ON" data-off-color="default"
                                                   data-off-text="OFF"></li>
                                        <li> Notify On SMTP Error
                                            <input type="checkbox" class="make-switch" checked data-size="small"
                                                   data-on-color="warning" data-on-text="ON" data-off-color="default"
                                                   data-off-text="OFF"></li>
                                    </ul>
                                    <div class="inner-content">
                                        <button class="btn btn-success">
                                            <i class="icon-settings"></i> Save Changes
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- END QUICK SIDEBAR --}}
            </div>
            {{-- END CONTAINER --}}
        </div>
    </div>
@endsection