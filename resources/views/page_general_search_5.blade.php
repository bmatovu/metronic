@extends ("layouts.base")

@section('extra-css')
    @parent
        {{-- BEGIN THEME GLOBAL STYLES --}}
<link href="{{ asset('css/plugins-md.min.css') }}" rel="stylesheet" type="text/css" />
{{-- END THEME GLOBAL STYLES --}}
        {{-- BEGIN PAGE LEVEL PLUGINS --}}
<link href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('plugins/fancybox/source/jquery.fancybox.css') }}" rel="stylesheet" type="text/css" />
{{-- END PAGE LEVEL PLUGINS --}}
{{-- BEGIN PAGE LEVEL STYLES --}}
<link href="{{ asset('pages/css/search.min.css') }}" rel="stylesheet" type="text/css" />
{{-- END PAGE LEVEL STYLES --}}
@endsection

@push('extra-js')
        {{-- BEGIN PAGE LEVEL PLUGINS --}}
<script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/fancybox/source/jquery.fancybox.pack.js') }}" type="text/javascript"></script>
{{-- END PAGE LEVEL PLUGINS --}}
{{-- BEGIN PAGE LEVEL SCRIPTS --}}
<script src="{{ asset('pages/js/search.min.js') }}" type="text/javascript"></script>
{{-- END PAGE LEVEL SCRIPTS --}}
@endpush

@section('main-content')
    <div class="page-wrapper-row full-height">
        <div class="page-wrapper-middle">
            {{-- BEGIN CONTAINER --}}
            <div class="page-container">
                {{-- BEGIN CONTENT --}}
                <div class="page-content-wrapper">
                    {{-- BEGIN CONTENT BODY --}}
                    {{-- BEGIN PAGE HEAD--}}
                    <div class="page-head">
                        <div class="container">
                            {{-- BEGIN PAGE TITLE --}}
                            <div class="page-title">
                                <h1>Search Results 5
                                    <small>search results</small>
                                </h1>
                            </div>
                            {{-- END PAGE TITLE --}}
                            {{-- BEGIN PAGE TOOLBAR --}}
                            @include('includes.toolbar')
                            {{-- END PAGE TOOLBAR --}}
                        </div>
                    </div>
                    {{-- END PAGE HEAD--}}
                    {{-- BEGIN PAGE CONTENT BODY --}}
                    <div class="page-content">
                        <div class="container">
                            {{-- BEGIN PAGE BREADCRUMBS --}}
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <a href="{{ url('/') }}">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <a href="#">Pages</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <a href="#">General</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Search</span>
                                </li>
                            </ul>
                            {{-- END PAGE BREADCRUMBS --}}
                            {{-- BEGIN PAGE CONTENT INNER --}}
                            <div class="page-content-inner">
                                <div class="search-page search-content-4">
                                    <div class="search-bar bordered">
                                        <div class="row">
                                            <div class="col-lg-8">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" placeholder="Search for...">
                                                            <span class="input-group-btn">
                                                                <button class="btn green-soft uppercase bold" type="button">Search</button>
                                                            </span>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 extra-buttons">
                                                <button class="btn grey-steel uppercase bold" type="button">Reset Search</button>
                                                <button class="btn grey-cararra font-blue" type="button">Advanced Search</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="search-table table-responsive">
                                        <table class="table table-bordered table-striped table-condensed">
                                            <thead class="bg-blue">
                                            <tr>
                                                <th>
                                                    <a href="javascript:;">Status</a>
                                                </th>
                                                <th>
                                                    <a href="javascript:;">Date</a>
                                                </th>
                                                <th>
                                                    <a href="javascript:;">Title & Author</a>
                                                </th>
                                                <th>
                                                    <a href="javascript:;">Description</a>
                                                </th>
                                                <th>
                                                    <a href="javascript:;">Download</a>
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td class="table-status">
                                                    <a href="javascript:;">
                                                        <i class="icon-arrow-right font-blue"></i>
                                                    </a>
                                                </td>
                                                <td class="table-date font-blue">
                                                    <a href="javascript:;">October 17, 2015</a>
                                                </td>
                                                <td class="table-title">
                                                    <h3>
                                                        <a href="javascript:;">Metronic Admin Reborn</a>
                                                    </h3>
                                                    <p>Last Activity:
                                                        <a href="javascript:;">Bob Robson</a> -
                                                        <span class="font-grey-cascade">25 mins ago</span>
                                                    </p>
                                                </td>
                                                <td class="table-desc"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy sead euismod dolore tincidunt ut laoreet dolore dolor sit amet </td>
                                                <td class="table-download">
                                                    <a href="javascript:;">
                                                        <i class="icon-doc font-green-soft"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="table-status">
                                                    <a href="javascript:;">
                                                        <i class="icon-arrow-right font-blue"></i>
                                                    </a>
                                                </td>
                                                <td class="table-date font-blue">
                                                    <a href="javascript:;">October 17, 2015</a>
                                                </td>
                                                <td class="table-title">
                                                    <h3>
                                                        <a href="javascript:;">Lorem ipsum dolor</a>
                                                    </h3>
                                                    <p>Last Activity:
                                                        <a href="javascript:;">Bob Robson</a> -
                                                        <span class="font-grey-cascade">25 mins ago</span>
                                                    </p>
                                                </td>
                                                <td class="table-desc"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy sead euismod dolore tincidunt ut laoreet dolore dolor sit amet </td>
                                                <td class="table-download">
                                                    <a href="javascript:;">
                                                        <i class="icon-doc font-green-soft"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="table-status">
                                                    <a href="javascript:;">
                                                        <i class="icon-check font-grey"></i>
                                                    </a>
                                                </td>
                                                <td class="table-date font-blue">
                                                    <a href="javascript:;">October 15, 2015</a>
                                                </td>
                                                <td class="table-title">
                                                    <h3>
                                                        <a href="javascript:;">Typi non habent</a>
                                                    </h3>
                                                    <p>Last Activity:
                                                        <a href="javascript:;">Bob Robson</a> -
                                                        <span class="font-grey-cascade">25 mins ago</span>
                                                    </p>
                                                </td>
                                                <td class="table-desc"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy sead euismod dolore tincidunt ut laoreet dolore dolor sit amet </td>
                                                <td class="table-download">
                                                    <a href="javascript:;">
                                                        <i class="icon-doc font-green-soft"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="table-status">
                                                    <a href="javascript:;">
                                                        <i class="icon-arrow-right font-blue"></i>
                                                    </a>
                                                </td>
                                                <td class="table-date font-blue">
                                                    <a href="javascript:;">October 12, 2015</a>
                                                </td>
                                                <td class="table-title">
                                                    <h3>
                                                        <a href="javascript:;">Metronic Admin Search Result</a>
                                                    </h3>
                                                    <p>Last Activity:
                                                        <a href="javascript:;">Bob Robson</a> -
                                                        <span class="font-grey-cascade">25 mins ago</span>
                                                    </p>
                                                </td>
                                                <td class="table-desc"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy sead euismod dolore tincidunt ut laoreet dolore dolor sit amet </td>
                                                <td class="table-download">
                                                    <a href="javascript:;">
                                                        <i class="icon-doc font-green-soft"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="table-status">
                                                    <a href="javascript:;">
                                                        <i class="icon-arrow-right font-blue"></i>
                                                    </a>
                                                </td>
                                                <td class="table-date font-blue">
                                                    <a href="javascript:;">October 11, 2015</a>
                                                </td>
                                                <td class="table-title">
                                                    <h3>
                                                        <a href="javascript:;">Mirum est notare</a>
                                                    </h3>
                                                    <p>Last Activity:
                                                        <a href="javascript:;">Bob Robson</a> -
                                                        <span class="font-grey-cascade">25 mins ago</span>
                                                    </p>
                                                </td>
                                                <td class="table-desc"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy sead euismod dolore tincidunt ut laoreet dolore dolor sit amet </td>
                                                <td class="table-download">
                                                    <a href="javascript:;">
                                                        <i class="icon-doc font-green-soft"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="table-status">
                                                    <a href="javascript:;">
                                                        <i class="icon-check font-grey"></i>
                                                    </a>
                                                </td>
                                                <td class="table-date font-blue">
                                                    <a href="javascript:;">October 9, 2015</a>
                                                </td>
                                                <td class="table-title">
                                                    <h3>
                                                        <a href="javascript:;">Metronic Admin Reborn</a>
                                                    </h3>
                                                    <p>Last Activity:
                                                        <a href="javascript:;">Bob Robson</a> -
                                                        <span class="font-grey-cascade">25 mins ago</span>
                                                    </p>
                                                </td>
                                                <td class="table-desc"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy sead euismod dolore tincidunt ut laoreet dolore dolor sit amet </td>
                                                <td class="table-download">
                                                    <a href="javascript:;">
                                                        <i class="icon-doc font-green-soft"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="table-status">
                                                    <a href="javascript:;">
                                                        <i class="icon-check font-grey"></i>
                                                    </a>
                                                </td>
                                                <td class="table-date font-blue">
                                                    <a href="javascript:;">October 9, 2015</a>
                                                </td>
                                                <td class="table-title">
                                                    <h3>
                                                        <a href="javascript:;">Metronic Admin Reborn</a>
                                                    </h3>
                                                    <p>Last Activity:
                                                        <a href="javascript:;">Bob Robson</a> -
                                                        <span class="font-grey-cascade">25 mins ago</span>
                                                    </p>
                                                </td>
                                                <td class="table-desc"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy sead euismod dolore tincidunt ut laoreet dolore dolor sit amet </td>
                                                <td class="table-download">
                                                    <a href="javascript:;">
                                                        <i class="icon-doc font-green-soft"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="table-status">
                                                    <a href="javascript:;">
                                                        <i class="icon-arrow-right font-blue"></i>
                                                    </a>
                                                </td>
                                                <td class="table-date font-blue">
                                                    <a href="javascript:;">October 6, 2015</a>
                                                </td>
                                                <td class="table-title">
                                                    <h3>
                                                        <a href="javascript:;">Metronic Admin Reborn Progress</a>
                                                    </h3>
                                                    <p>Last Activity:
                                                        <a href="javascript:;">Bob Robson</a> -
                                                        <span class="font-grey-cascade">25 mins ago</span>
                                                    </p>
                                                </td>
                                                <td class="table-desc"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy sead euismod dolore tincidunt ut laoreet dolore dolor sit amet </td>
                                                <td class="table-download">
                                                    <a href="javascript:;">
                                                        <i class="icon-doc font-green-soft"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="table-status">
                                                    <a href="javascript:;">
                                                        <i class="icon-arrow-right font-blue"></i>
                                                    </a>
                                                </td>
                                                <td class="table-date font-blue">
                                                    <a href="javascript:;">October 3, 2015</a>
                                                </td>
                                                <td class="table-title">
                                                    <h3>
                                                        <a href="javascript:;">Metronic Search Page 5</a>
                                                    </h3>
                                                    <p>Last Activity:
                                                        <a href="javascript:;">Bob Robson</a> -
                                                        <span class="font-grey-cascade">25 mins ago</span>
                                                    </p>
                                                </td>
                                                <td class="table-desc"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy sead euismod dolore tincidunt ut laoreet dolore dolor sit amet </td>
                                                <td class="table-download">
                                                    <a href="javascript:;">
                                                        <i class="icon-doc font-green-soft"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="search-pagination pagination-rounded">
                                        <ul class="pagination">
                                            <li class="page-active">
                                                <a href="javascript:;"> 1 </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;"> 2 </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;"> 3 </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;"> 4 </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            {{-- END PAGE CONTENT INNER --}}
                        </div>
                    </div>
                    {{-- END PAGE CONTENT BODY --}}
                    {{-- END CONTENT BODY --}}
                </div>
                {{-- END CONTENT --}}
                {{-- BEGIN QUICK SIDEBAR --}}
                @include('includes.sidebar')
                {{-- END QUICK SIDEBAR --}}
            </div>
            {{-- END CONTAINER --}}
        </div>
    </div>
@endsection