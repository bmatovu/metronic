@extends ("layouts.base-min")

@section('extra-css')
    @parent
    {{-- BEGIN PAGE LEVEL STYLES --}}
    <link href="{{ asset('pages/css/lock.min.css') }}" rel="stylesheet" type="text/css" />
    {{-- END PAGE LEVEL STYLES --}}
@endsection

@push('extra-js')
    {{-- BEGIN PAGE LEVEL SCRIPTS --}}
    <script src="{{ asset('pages/js/lock.min.js') }}" type="text/javascript"></script>
    {{-- END PAGE LEVEL SCRIPTS --}}
@endpush

@section('main-content')

    <div class="page-lock">
        <div class="page-logo">
            <a class="brand" href="{{ url('') }}">
                <img src="{{ asset('pages/img/logo-big.png') }}" alt="logo"/> </a>
        </div>
        <div class="page-body">
            <div class="lock-head"> Locked</div>
            <div class="lock-body">
                <div class="pull-left lock-avatar-block">
                    <img src="{{ asset('pages/media/profile/photo3.jpg') }}" class="lock-avatar"></div>
                <form class="lock-form pull-left" action="{{ url('') }}" method="post">
                    <h4>Amanda Smith</h4>

                    <div class="form-group">
                        <input class="form-control placeholder-no-fix" type="password" autocomplete="off"
                               placeholder="Password" name="password"/></div>
                    <div class="form-actions">
                        <button type="submit" class="btn red uppercase">Login</button>
                    </div>
                </form>
            </div>
            <div class="lock-bottom">
                <a href="">Not Amanda Smith?</a>
            </div>
        </div>
        <div class="page-footer-custom"> 2014 &copy; Metronic. Admin Dashboard Template.</div>
    </div>

@endsection