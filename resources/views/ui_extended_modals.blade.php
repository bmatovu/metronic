@extends ("layouts.base")

@section('extra-css')
    @parent
    {{-- BEGIN PAGE LEVEL STYLES --}}
    <link href="{{ asset('plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('plugins/bootstrap-modal/css/bootstrap-modal.css') }}" rel="stylesheet" type="text/css"/>
    {{-- END PAGE LEVEL STYLES --}}
    {{-- BEGIN THEME GLOBAL STYLES --}}
    <link href="{{ asset('css/plugins-md.min.css') }}" rel="stylesheet" type="text/css"/>
    {{-- END THEME GLOBAL STYLES --}}
@endsection

@push('extra-js')
{{-- BEGIN PAGE LEVEL PLUGINS --}}
<script src="{{ asset('plugins/bootstrap-modal/js/bootstrap-modalmanager.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/bootstrap-modal/js/bootstrap-modal.js') }}" type="text/javascript"></script>
{{-- END PAGE LEVEL PLUGINS --}}
{{-- BEGIN PAGE LEVEL SCRIPTS --}}
<script src="{{ asset('pages/js/ui-extended-modals.min.js') }}" type="text/javascript"></script>
{{-- END PAGE LEVEL SCRIPTS --}}
@endpush

@section('main-content')
    <div class="page-wrapper-row full-height">
        <div class="page-wrapper-middle">
            {{-- BEGIN CONTAINER --}}
            <div class="page-container">
                {{-- BEGIN CONTENT --}}
                <div class="page-content-wrapper">
                    {{-- BEGIN CONTENT BODY --}}
                    {{-- BEGIN PAGE HEAD--}}
                    <div class="page-head">
                        <div class="container">
                            {{-- BEGIN PAGE TITLE --}}
                            <div class="page-title">
                                <h1>Extended Modals
                                    <small>extended bootstrap modals</small>
                                </h1>
                            </div>
                            {{-- END PAGE TITLE --}}
                            {{-- BEGIN PAGE TOOLBAR --}}
                            @include('includes.toolbar')
                            {{-- END PAGE TOOLBAR --}}
                        </div>
                    </div>
                    {{-- END PAGE HEAD--}}
                    {{-- BEGIN PAGE CONTENT BODY --}}
                    <div class="page-content">
                        <div class="container">
                            {{-- BEGIN PAGE BREADCRUMBS --}}
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <a href="{{ url('/') }}">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <a href="#">UI Features</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Extended Modals</span>
                                </li>
                            </ul>
                            {{-- END PAGE BREADCRUMBS --}}
                            {{-- BEGIN PAGE CONTENT INNER --}}
                            <div class="page-content-inner">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="note note-success">
                                            <h4 class="block">Extended Modals</h4>

                                            <p> Extends Bootstrap's native modals to provide additional functionality.
                                                Introduces a ModalManager class that operates behind the scenes to handle
                                                multiple modals by listening on their events. Go ahead
                                                to check out
                                                <a href="https://github.com/jschr/bootstrap-modal"
                                                   class="btn btn-outline red" target="_blank"> the official home </a> of the
                                                Extended Modals Plugin. </p>
                                        </div>
                                        {{-- BEGIN PORTLET--}}
                                        <div class="portlet light ">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="icon-bubble font-green-sharp"></i>
                                                    <span class="caption-subject font-green-sharp sbold">Extended Modals Example</span>
                                                </div>
                                                <div class="actions">
                                                    <div class="btn-group">
                                                        <a class="btn green-haze btn-outline btn-circle btn-sm"
                                                           href="javascript:;" data-toggle="dropdown" data-hover="dropdown"
                                                           data-close-others="true"> Actions
                                                            <i class="fa fa-angle-down"></i>
                                                        </a>
                                                        <ul class="dropdown-menu pull-right">
                                                            <li>
                                                                <a href="javascript:;"> Option 1</a>
                                                            </li>
                                                            <li class="divider"></li>
                                                            <li>
                                                                <a href="javascript:;">Option 2</a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;">Option 3</a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;">Option 4</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <table class="table table-hover table-striped table-bordered">
                                                    <tr>
                                                        <td> Responsive</td>
                                                        <td>
                                                            <a class="btn btn-outline dark" data-toggle="modal"
                                                               href="#responsive"> View Demo </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td> Stackable</td>
                                                        <td>
                                                            <a class="btn btn-outline dark" data-target="#stack1"
                                                               data-toggle="modal"> View Demo </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td> Ajax</td>
                                                        <td>
                                                            <a class="btn btn-outline dark" id="ajax-demo"
                                                               data-url="{{ url('ui_extended_modals_ajax_sample') }}"
                                                               data-toggle="modal"> View Demo </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td> Static Background</td>
                                                        <td>
                                                            <a class="btn btn-outline dark" data-target="#static2"
                                                               data-toggle="modal"> View Demo </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td> Static Background with Animation</td>
                                                        <td>
                                                            <a class="btn btn-outline dark" data-target="#static"
                                                               data-toggle="modal"> View Demo </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td> Full Width</td>
                                                        <td>
                                                            <a class="btn btn-outline dark" data-target="#full-width"
                                                               data-toggle="modal"> View Demo </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td> Long Modals</td>
                                                        <td>
                                                            <a class="btn btn-outline dark" data-target="#long"
                                                               data-toggle="modal"> View Demo </a>
                                                        </td>
                                                    </tr>
                                                </table>
                                                {{-- responsive --}}
                                                <div id="responsive" class="modal fade" tabindex="-1" data-width="760">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-hidden="true"></button>
                                                        <h4 class="modal-title">Responsive</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <h4>Some Input</h4>

                                                                <p>
                                                                    <input class="form-control" type="text"></p>

                                                                <p>
                                                                    <input class="form-control" type="text"></p>

                                                                <p>
                                                                    <input class="form-control" type="text"></p>

                                                                <p>
                                                                    <input class="form-control" type="text"></p>

                                                                <p>
                                                                    <input class="form-control" type="text"></p>

                                                                <p>
                                                                    <input class="form-control" type="text"></p>

                                                                <p>
                                                                    <input class="form-control" type="text"></p>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <h4>Some More Input</h4>

                                                                <p>
                                                                    <input class="form-control" type="text"></p>

                                                                <p>
                                                                    <input class="form-control" type="text"></p>

                                                                <p>
                                                                    <input class="form-control" type="text"></p>

                                                                <p>
                                                                    <input class="form-control" type="text"></p>

                                                                <p>
                                                                    <input class="form-control" type="text"></p>

                                                                <p>
                                                                    <input class="form-control" type="text"></p>

                                                                <p>
                                                                    <input class="form-control" type="text"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" data-dismiss="modal"
                                                                class="btn btn-outline dark">Close
                                                        </button>
                                                        <button type="button" class="btn green">Save changes</button>
                                                    </div>
                                                </div>
                                                {{-- stackable --}}
                                                <div id="stack1" class="modal fade" tabindex="-1"
                                                     data-focus-on="input:first">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-hidden="true"></button>
                                                        <h4 class="modal-title">Stack One</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p> One fine body... </p>

                                                        <p> One fine body... </p>

                                                        <p> One fine body... </p>

                                                        <div class="form-group">
                                                            <input class="form-control" type="text" data-tabindex="1"></div>
                                                        <div class="form-group">
                                                            <input class="form-control" type="text" data-tabindex="2"></div>
                                                        <button class="btn blue" data-toggle="modal" href="#stack2">Launch
                                                            modal
                                                        </button>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" data-dismiss="modal"
                                                                class="btn btn-outline dark">Close
                                                        </button>
                                                        <button type="button" class="btn green">Ok</button>
                                                    </div>
                                                </div>
                                                <div id="stack2" class="modal fade" tabindex="-1"
                                                     data-focus-on="input:first">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-hidden="true"></button>
                                                        <h4 class="modal-title">Stack Two</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p> One fine body... </p>

                                                        <p> One fine body... </p>

                                                        <div class="form-group">
                                                            <input class="form-control" type="text" data-tabindex="1"></div>
                                                        <div class="form-group">
                                                            <input class="form-control" type="text" data-tabindex="2"></div>
                                                        <button class="btn blue" data-toggle="modal" href="#stack3">Launch
                                                            modal
                                                        </button>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" data-dismiss="modal"
                                                                class="btn btn-outline dark">Close
                                                        </button>
                                                        <button type="button" class="btn red">Ok</button>
                                                    </div>
                                                </div>
                                                <div id="stack3" class="modal fade" tabindex="-1"
                                                     data-focus-on="input:first">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-hidden="true"></button>
                                                        <h4 class="modal-title">Stack Three</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p> One fine body... </p>
                                                        <input class="form-control" type="text" data-tabindex="1">
                                                        <input class="form-control" type="text" data-tabindex="2"></div>
                                                    <div class="modal-footer">
                                                        <button type="button" data-dismiss="modal"
                                                                class="btn btn-outline dark">Close
                                                        </button>
                                                        <button type="button" class="btn green">Ok</button>
                                                    </div>
                                                </div>
                                                {{-- ajax --}}
                                                <div id="ajax-modal" class="modal fade" tabindex="-1"></div>
                                                {{-- static --}}
                                                <div id="static" class="modal fade" tabindex="-1" data-backdrop="static"
                                                     data-keyboard="false">
                                                    <div class="modal-body">
                                                        <p> Would you like to continue with some arbitrary task? </p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" data-dismiss="modal"
                                                                class="btn btn-outline dark">Cancel
                                                        </button>
                                                        <button type="button" data-dismiss="modal" class="btn green">Continue
                                                            Task
                                                        </button>
                                                    </div>
                                                </div>
                                                <div id="static2" class="modal fade" tabindex="-1" data-backdrop="static"
                                                     data-keyboard="false" data-attention-animation="false">
                                                    <div class="modal-body">
                                                        <p> Would you like to continue with some arbitrary task? </p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" data-dismiss="modal"
                                                                class="btn btn-outline dark">Cancel
                                                        </button>
                                                        <button type="button" data-dismiss="modal" class="btn green">Continue
                                                            Task
                                                        </button>
                                                    </div>
                                                </div>
                                                {{-- full width --}}
                                                <div id="full-width" class="modal container fade" tabindex="-1">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-hidden="true"></button>
                                                        <h4 class="modal-title">Full Width</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p> This modal will resize itself to the same dimensions as the
                                                            container class. </p>

                                                        <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis
                                                            sollicitudin ipsum ac ante fermentum suscipit. In ac augue non
                                                            purus accumsan lobortis id sed nibh. Nunc egestas hendrerit
                                                            ipsum,
                                                            et porttitor augue volutpat non. Aliquam erat volutpat.
                                                            Vestibulum scelerisque lobortis pulvinar. Aenean hendrerit risus
                                                            neque, eget tincidunt leo. Vestibulum est tortor, commodo nec
                                                            cursus nec, vestibulum vel nibh. Morbi elit magna, ornare
                                                            placerat euismod semper, dignissim vel odio. Phasellus elementum
                                                            quam eu ipsum euismod pretium. </p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" data-dismiss="modal"
                                                                class="btn btn-outline dark">Close
                                                        </button>
                                                        <button type="button" class="btn green">Save changes</button>
                                                    </div>
                                                </div>
                                                {{-- long modals --}}
                                                <div id="long" class="modal fade modal-scroll" tabindex="-1"
                                                     data-replace="true">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-hidden="true"></button>
                                                        <h4 class="modal-title">A Fairly Long Modal</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <img style="height: 800px" src="http://i.imgur.com/KwPYo.jpg') }}">
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" data-dismiss="modal"
                                                                class="btn btn-outline dark">Close
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- END PORTLET--}}
                                    </div>
                                </div>
                            </div>
                            {{-- END PAGE CONTENT INNER --}}
                        </div>
                    </div>
                    {{-- END PAGE CONTENT BODY --}}
                    {{-- END CONTENT BODY --}}
                </div>
                {{-- END CONTENT --}}
                {{-- BEGIN QUICK SIDEBAR --}}
                @include('includes.sidebar')
                {{-- END QUICK SIDEBAR --}}
            </div>
            {{-- END CONTAINER --}}
        </div>
    </div>
@endsection