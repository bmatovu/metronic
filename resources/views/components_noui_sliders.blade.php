@extends ("layouts.base")

@section('extra-css')
    @parent
    {{-- BEGIN PAGE LEVEL PLUGINS --}}
    <link href="{{ asset('plugins/nouislider/nouislider.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('plugins/nouislider/nouislider.pips.css') }}" rel="stylesheet" type="text/css"/>
    {{-- END PAGE LEVEL PLUGINS --}}
    {{-- BEGIN THEME GLOBAL STYLES --}}
    <link href="{{ asset('css/plugins-md.min.css') }}" rel="stylesheet" type="text/css"/>
    {{-- END THEME GLOBAL STYLES --}}
@endsection

@push('extra-js')
{{-- BEGIN PAGE LEVEL PLUGINS --}}
<script src="{{ asset('plugins/nouislider/wNumb.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/nouislider/nouislider.min.js') }}" type="text/javascript"></script>
{{-- END PAGE LEVEL PLUGINS --}}
{{-- BEGIN PAGE LEVEL SCRIPTS --}}
<script src="{{ asset('pages/js/components-nouisliders.min.js') }}" type="text/javascript"></script>
{{-- END PAGE LEVEL SCRIPTS --}}
@endpush

@section('main-content')
    <div class="page-wrapper-row full-height">
        <div class="page-wrapper-middle">
            {{-- BEGIN CONTAINER --}}
            <div class="page-container">
                {{-- BEGIN CONTENT --}}
                <div class="page-content-wrapper">
                    {{-- BEGIN CONTENT BODY --}}
                    {{-- BEGIN PAGE HEAD--}}
                    <div class="page-head">
                        <div class="container">
                            {{-- BEGIN PAGE TITLE --}}
                            <div class="page-title">
                                <h1>NoUI Range Sliders
                                    <small>super tiny jQuery plugin that allows you to create range sliders</small>
                                </h1>
                            </div>
                            {{-- END PAGE TITLE --}}
                            {{-- BEGIN PAGE TOOLBAR --}}
                            @include('includes.toolbar')
                            {{-- END PAGE TOOLBAR --}}
                        </div>
                    </div>
                    {{-- END PAGE HEAD--}}
                    {{-- BEGIN PAGE CONTENT BODY --}}
                    <div class="page-content">
                        <div class="container">
                            {{-- BEGIN PAGE BREADCRUMBS --}}
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <a href="{{ url('/') }}">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <a href="#">Components</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>NoUI Range Sliders</span>
                                </li>
                            </ul>
                            {{-- END PAGE BREADCRUMBS --}}
                            {{-- BEGIN PAGE CONTENT INNER --}}
                            <div class="page-content-inner">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="m-heading-1 border-green m-bordered">
                                            <h3>Lightweight JavaScript Range Slider</h3>

                                            <p> noUiSlider is a range slider without bloat. It offers a ton off features, and
                                                it is as small, lightweight and minimal as possible, which is great for
                                                mobile use on the many supported devices, including
                                                iPhone, iPad, Android devices & Windows (Phone) 8 desktops, tablets and
                                                all-in-ones. It works on desktops too, of course. </p>

                                            <p> For more info please check out
                                                <a class="btn red btn-outline" href="http://refreshless.com/nouislider/"
                                                   target="_blank">the official documentation</a>
                                            </p>
                                        </div>
                                        {{-- BEGIN PORTLET--}}
                                        <div class="portlet light form-fit ">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="icon-settings font-red"></i>
                                                    <span class="caption-subject font-red sbold uppercase">Ion Range Sliders</span>
                                                </div>
                                                <div class="actions">
                                                    <div class="btn-group btn-group-devided" data-toggle="buttons">
                                                        <label class="btn btn-transparent red btn-outline btn-circle btn-sm active">
                                                            <input type="radio" name="options" class="toggle" id="option1">Actions</label>
                                                        <label class="btn btn-transparent red btn-outline btn-circle btn-sm">
                                                            <input type="radio" name="options" class="toggle" id="option2">Settings</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="portlet-body form">
                                                <form role="form" class="form-horizontal form-bordered">
                                                    <div class="form-body">
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Basic</label>

                                                            <div class="col-md-4">
                                                                <div id="demo2" class="noUi-success"></div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Custom Connect</label>

                                                            <div class="col-md-4">
                                                                <div id="demo3" class="noui-connect"></div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">HTML5 Input
                                                                Elements</label>

                                                            <div class="col-md-4">
                                                                <div id="demo4" class="noUi-success"></div>
                                                                <div class="well margin-top-30">
                                                                    <select id="demo4_select"
                                                                            class="form-control input-small input-inline"> </select>
                                                                    <input id="demo4_input"
                                                                           class="form-control input-small input-inline">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Linear Slider</label>

                                                            <div class="col-md-4">
                                                                <div id="demo5" class="noUi-danger"></div>
                                                                <div class="well margin-top-30">
                                                                    <strong>Value:</strong>
                                                                    <span id="demo5_lower-value"></span>
                                                                    <br>
                                                                    <strong>Value:</strong>
                                                                    <span id="demo5_upper-value"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">With Tooltips</label>

                                                            <div class="col-md-4">
                                                                <div id="demo8" class="noUi-danger"></div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Soft Limits</label>

                                                            <div class="col-md-4">
                                                                <div id="demo7" class="noUi-primary margin-bottom-40"></div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group last">
                                                            <label class="col-md-3 control-label">Locking Sliders
                                                                Together</label>

                                                            <div class="col-md-4">
                                                                <div id="demo6_slider1" class="noUi-danger"></div>
                                                                <strong>Value:</strong>
                                                                <span id="demo6_slider1-span"></span>

                                                                <div id="demo6_slider2" class="noUi-success"></div>
                                                                <strong>Value:</strong>
                                                                <span id="demo6_slider2-span"></span>

                                                                <p>
                                                                    <a href="javascript:;"
                                                                       class="btn red btn-outline sbold uppercase"
                                                                       id="demo6_lockbutton">Lock</a>
                                                                </p>
                                                                <span class="help-block"> Two cross-updating sliders can be created using a combination of the change and slide events. </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-offset-3 col-md-9">
                                                                <button type="submit" class="btn green">Submit</button>
                                                                <button type="button" class="btn default">Cancel</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        {{-- END PORTLET--}}
                                    </div>
                                </div>
                            </div>
                            {{-- END PAGE CONTENT INNER --}}
                        </div>
                    </div>
                    {{-- END PAGE CONTENT BODY --}}
                    {{-- END CONTENT BODY --}}
                </div>
                {{-- END CONTENT --}}
                {{-- BEGIN QUICK SIDEBAR --}}
                @include('includes.sidebar')
                {{-- END QUICK SIDEBAR --}}
            </div>
            {{-- END CONTAINER --}}
        </div>
    </div>
@endsection