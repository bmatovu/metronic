@extends ("layouts.base")

@section('extra-css')
    @parent
    {{-- BEGIN PAGE LEVEL PLUGINS --}}
    <link href="{{ asset('plugins/bootstrap-touchspin/bootstrap.touchspin.css') }}" rel="stylesheet" type="text/css"/>
    {{-- END PAGE LEVEL PLUGINS --}}
    {{-- BEGIN THEME GLOBAL STYLES --}}
    <link href="{{ asset('css/plugins-md.min.css') }}" rel="stylesheet" type="text/css"/>
    {{-- END THEME GLOBAL STYLES --}}
@endsection

@push('extra-js')
{{-- BEGIN PAGE LEVEL PLUGINS --}}
<script src="{{ asset('plugins/fuelux/js/spinner.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/bootstrap-touchspin/bootstrap.touchspin.js') }}" type="text/javascript"></script>
{{-- END PAGE LEVEL PLUGINS --}}
{{-- BEGIN PAGE LEVEL SCRIPTS --}}
<script src="{{ asset('pages/js/components-bootstrap-touchspin.min.js') }}" type="text/javascript"></script>
{{-- END PAGE LEVEL SCRIPTS --}}
@endpush

@section('main-content')
    <div class="page-wrapper-row full-height">
        <div class="page-wrapper-middle">
            {{-- BEGIN CONTAINER --}}
            <div class="page-container">
                {{-- BEGIN CONTENT --}}
                <div class="page-content-wrapper">
                    {{-- BEGIN CONTENT BODY --}}
                    {{-- BEGIN PAGE HEAD--}}
                    <div class="page-head">
                        <div class="container">
                            {{-- BEGIN PAGE TITLE --}}
                            <div class="page-title">
                                <h1>Bootstrap Touchspin
                                    <small>advanced bootstrap touch input spinners</small>
                                </h1>
                            </div>
                            {{-- END PAGE TITLE --}}
                            {{-- BEGIN PAGE TOOLBAR --}}
                            @include('includes.toolbar')
                            {{-- END PAGE TOOLBAR --}}
                        </div>
                    </div>
                    {{-- END PAGE HEAD--}}
                    {{-- BEGIN PAGE CONTENT BODY --}}
                    <div class="page-content">
                        <div class="container">
                            {{-- BEGIN PAGE BREADCRUMBS --}}
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <a href="{{ url('/') }}">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <a href="#">Components</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Bootstrap Touchspin</span>
                                </li>
                            </ul>
                            {{-- END PAGE BREADCRUMBS --}}
                            {{-- BEGIN PAGE CONTENT INNER --}}
                            <div class="page-content-inner">
                                <div class="note note-success">
                                    <h3>Bootstrap TouchSpin</h3>

                                    <p> A mobile and touch friendly input spinner component for Bootstrap 3. It supports the
                                        mousewheel and the up/down keys.. For more info please check out
                                        <a href="http://www.virtuosoft.eu/code/bootstrap-touchspin/"
                                           target="_blank">the official documentation</a>. </p>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        {{-- BEGIN PORTLET--}}
                                        <div class="portlet light form-fit ">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="icon-settings font-dark"></i>
                                                    <span class="caption-subject font-dark sbold uppercase">Bootstrap TouchSpin</span>
                                                </div>
                                                <div class="actions">
                                                    <div class="btn-group btn-group-devided" data-toggle="buttons">
                                                        <label class="btn btn-transparent dark btn-outline btn-circle btn-sm active">
                                                            <input type="radio" name="options" class="toggle" id="option1">New</label>
                                                        <label class="btn btn-transparent dark btn-outline btn-circle btn-sm">
                                                            <input type="radio" name="options" class="toggle" id="option2">Returning</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="portlet-body form">
                                                {{-- BEGIN FORM--}}
                                                <form action="#" class="form-horizontal form-bordered">
                                                    <div class="form-body">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Example with
                                                                postfix</label>

                                                            <div class="col-md-4">
                                                                <input id="touchspin_1" type="text" value="55" name="demo1"
                                                                       class="form-control"></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">With prefix</label>

                                                            <div class="col-md-4">
                                                                <input id="touchspin_2" type="text" value="0" name="demo2"
                                                                       class="form-control"></div>
                                                        </div>
                                                        <div class="form-group last">
                                                            <label class="control-label col-md-3">Vertical button
                                                                alignment</label>

                                                            <div class="col-md-4">
                                                                <input id="touchspin_3" type="text" value=""
                                                                       name="demo_vertical"></div>
                                                        </div>
                                                        <div class="form-group last">
                                                            <label class="control-label col-md-3">Vertical buttons with
                                                                custom icons</label>

                                                            <div class="col-md-4">
                                                                <input id="touchspin_4" type="text" value=""
                                                                       name="demo_vertical"></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Init with empty
                                                                value</label>

                                                            <div class="col-md-4">
                                                                <input id="touchspin_5" type="text" value="" name="demo3_21">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Value attribute is not
                                                                set</label>

                                                            <div class="col-md-4">
                                                                <input id="touchspin_6" type="text" value=""
                                                                       name="demo_vertical"></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Value is set explicitly to
                                                                33</label>

                                                            <div class="col-md-4">
                                                                <input id="touchspin_7" type="text" value="33"
                                                                       name="demo_vertical"></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Button
                                                                postfix(small)</label>

                                                            <div class="col-md-4">
                                                                <input id="touchspin_8" type="text" value="" name="demo4"
                                                                       class="input-sm"></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Button
                                                                postfix(large)</label>

                                                            <div class="col-md-4">
                                                                <div class="input-group input-group-lg">
                                                                    <input id="touchspin_9" type="text" value=""
                                                                           name="demo4_2" class="form-control input-lg">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Button group</label>

                                                            <div class="col-md-4">
                                                                <div class="input-group">
                                                                    <input id="touchspin_10" type="text" class="form-control"
                                                                           name="demo5" value="50">

                                                                    <div class="input-group-btn">
                                                                        <button type="button" class="btn btn-default">
                                                                            Action
                                                                        </button>
                                                                        <button type="button"
                                                                                class="btn btn-default dropdown-toggle"
                                                                                data-toggle="dropdown">
                                                                            <span class="caret"></span>
                                                                            <span class="sr-only">Toggle Dropdown</span>
                                                                        </button>
                                                                        <ul class="dropdown-menu pull-right">
                                                                            <li>
                                                                                <a href="#">Action</a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#">Another action</a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#">Something else here</a>
                                                                            </li>
                                                                            <li class="divider"></li>
                                                                            <li>
                                                                                <a href="#">Separated link</a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Change button class</label>

                                                            <div class="col-md-4">
                                                                <input id="touchspin_11" type="text" value="" name="demo4_2">
                                                            </div>
                                                        </div>
                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-md-offset-3 col-md-9">
                                                                    <a href="javascript:;" class="btn dark">
                                                                        <i class="fa fa-check"></i> Submit</a>
                                                                    <a href="javascript:;"
                                                                       class="btn btn-outline grey-salsa">Cancel</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- END PAGE CONTENT INNER --}}
                        </div>
                    </div>
                    {{-- END PAGE CONTENT BODY --}}
                    {{-- END CONTENT BODY --}}
                </div>
                {{-- END CONTENT --}}
                {{-- BEGIN QUICK SIDEBAR --}}
                @include('includes.sidebar')
                {{-- END QUICK SIDEBAR --}}
            </div>
            {{-- END CONTAINER --}}
        </div>
    </div>
@endsection