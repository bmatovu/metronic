@extends ("layouts.base")

@section('extra-css')
    @parent
    {{-- BEGIN PAGE LEVEL PLUGINS --}}
    <link href="{{ asset('plugins/ion.rangeslider/css/ion.rangeSlider.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('plugins/ion.rangeslider/css/ion.rangeSlider.skinFlat.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('plugins/bootstrap-markdown/css/bootstrap-markdown.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('plugins/bootstrap-summernote/summernote.css') }}" rel="stylesheet" type="text/css"/>
    {{-- END PAGE LEVEL PLUGINS --}}
    {{-- BEGIN THEME GLOBAL STYLES --}}
    <link href="{{ asset('css/plugins-md.min.css') }}" rel="stylesheet" type="text/css"/>
    {{-- END THEME GLOBAL STYLES --}}
@endsection

@push('extra-js')
{{-- BEGIN PAGE LEVEL PLUGINS --}}
<script src="{{ asset('plugins/ion.rangeslider/js/ion.rangeSlider.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/bootstrap-markdown/lib/markdown.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/bootstrap-markdown/js/bootstrap-markdown.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/bootstrap-summernote/summernote.min.js') }}" type="text/javascript"></script>
{{-- END PAGE LEVEL PLUGINS --}}
{{-- BEGIN PAGE LEVEL SCRIPTS --}}
<script src="{{ asset('pages/js/components-ion-sliders.min.js') }}" type="text/javascript"></script>
{{-- END PAGE LEVEL SCRIPTS --}}
@endpush

@section('main-content')
    <div class="page-wrapper-row full-height">
        <div class="page-wrapper-middle">
            {{-- BEGIN CONTAINER --}}
            <div class="page-container">
                {{-- BEGIN CONTENT --}}
                <div class="page-content-wrapper">
                    {{-- BEGIN CONTENT BODY --}}
                    {{-- BEGIN PAGE HEAD--}}
                    <div class="page-head">
                        <div class="container">
                            {{-- BEGIN PAGE TITLE --}}
                            <div class="page-title">
                                <h1>Ion Range Sliders
                                    <small>cool, comfortable and easily customizable range slider with skins support</small>
                                </h1>
                            </div>
                            {{-- END PAGE TITLE --}}
                            {{-- BEGIN PAGE TOOLBAR --}}
                            @include('includes.toolbar')
                            {{-- END PAGE TOOLBAR --}}
                        </div>
                    </div>
                    {{-- END PAGE HEAD--}}
                    {{-- BEGIN PAGE CONTENT BODY --}}
                    <div class="page-content">
                        <div class="container">
                            {{-- BEGIN PAGE BREADCRUMBS --}}
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <a href="{{ url('/') }}">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <a href="#">Components</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Ion Range Sliders</span>
                                </li>
                            </ul>
                            {{-- END PAGE BREADCRUMBS --}}
                            {{-- BEGIN PAGE CONTENT INNER --}}
                            <div class="page-content-inner">
                                <div class="m-heading-1 border-green m-bordered">
                                    <h3>Ion Range Slider</h3>

                                    <p> Ion.RangeSlider. Is an easy, flexible and responsive range slider with tons of
                                        options. For more info please check out
                                        <a class="btn red btn-outline" href="https://github.com/IonDen/ion.rangeSlider"
                                           target="_blank">the official documentation</a>
                                    </p>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        {{-- BEGIN PORTLET--}}
                                        <div class="portlet light form-fit ">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="icon-settings font-dark"></i>
                                                    <span class="caption-subject font-dark sbold uppercase">Basic Demos</span>
                                                </div>
                                                <div class="actions">
                                                    <div class="btn-group btn-group-devided" data-toggle="buttons">
                                                        <label class="btn btn-transparent dark btn-outline btn-circle btn-sm active">
                                                            <input type="radio" name="options" class="toggle" id="option1">Actions</label>
                                                        <label class="btn btn-transparent dark btn-outline btn-circle btn-sm">
                                                            <input type="radio" name="options" class="toggle" id="option2">Settings</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="portlet-body form">
                                                <form role="form" class="form-horizontal form-bordered">
                                                    <div class="form-body">
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Basic</label>

                                                            <div class="col-md-9">
                                                                <input id="range_1" type="text"/></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Min & Max Values</label>

                                                            <div class="col-md-9">
                                                                <input id="range_2" type="text" value=""/>
                                                                <span class="help-block"> Set min value, max value and start point </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Range Type</label>

                                                            <div class="col-md-9">
                                                                <input id="range_3" type="text" value=""/>
                                                                <span class="help-block"> Set type to double and specify range, also showing grid and adding prefix "$" </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Negative Range</label>

                                                            <div class="col-md-9">
                                                                <input id="range_4" type="text" value=""/>
                                                                <span class="help-block"> Set up range with negative values </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Custom Steps</label>

                                                            <div class="col-md-9">
                                                                <input id="range_5" type="text" value=""/>
                                                                <span class="help-block"> Set up you own numbers </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">String Values</label>

                                                            <div class="col-md-9">
                                                                <input id="range_6" type="text" value=""/>
                                                                <span class="help-block"> Using any strings as your values </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Month Values</label>

                                                            <div class="col-md-9">
                                                                <input id="range_7" type="text" value=""/>
                                                                <span class="help-block"> One more example with strings </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Values Connect</label>

                                                            <div class="col-md-9">
                                                                <input id="range_8" type="text" value=""/>
                                                                <span class="help-block"> Taking care about how from and to values connect? Use decorate_both option. </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group last">
                                                            <label class="col-md-3 control-label">Values Connect
                                                                Separator</label>

                                                            <div class="col-md-9">
                                                                <input id="range_9" type="text" value=""/>
                                                                <span class="help-block"> Use your own separator symbol with values_separator option. Like ? </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-offset-3 col-md-9">
                                                                <button type="submit" class="btn green">Submit</button>
                                                                <button type="button" class="btn default">Cancel</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        {{-- END PORTLET--}}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        {{-- BEGIN PORTLET--}}
                                        <div class="portlet light form-fit ">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="icon-settings font-green"></i>
                                                    <span class="caption-subject font-green sbold uppercase">Advanced Demos</span>
                                                </div>
                                                <div class="actions">
                                                    <div class="btn-group btn-group-devided" data-toggle="buttons">
                                                        <label class="btn btn-transparent green btn-outline btn-circle btn-sm active">
                                                            <input type="radio" name="options" class="toggle" id="option1">Actions</label>
                                                        <label class="btn btn-transparent green btn-outline btn-circle btn-sm">
                                                            <input type="radio" name="options" class="toggle" id="option2">Settings</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="portlet-body form">
                                                <form role="form" class="form-horizontal form-bordered">
                                                    <div class="form-body">
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Lock handles</label>

                                                            <div class="col-md-9">
                                                                <input id="range_10" type="text" value=""/>
                                                                <span class="help-block"> Sometimes you want to forbid dragging one or both handles. Try to move left handle, you can't. </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-body">
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Movement Limits 1</label>

                                                            <div class="col-md-9">
                                                                <input id="range_11" type="text" value=""/>
                                                                        <span class="help-block"> Sometimes you may want to show user full slider, but restict him from using it for 100%. The limits is the tool for you. Try to drag slider, you will see, it has invisible borders
                                                                            on 10 and 50. </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-body">
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Movement Limits 2</label>

                                                            <div class="col-md-9">
                                                                <input id="range_12" type="text" value=""/>
                                                                <span class="help-block"> One more example with limits. </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-body">
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Disable Slider</label>

                                                            <div class="col-md-9">
                                                                <input id="range_13" type="text" value=""/>
                                                                <span class="help-block"> You can lock your slider, by using disable option. </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-offset-3 col-md-9">
                                                                <button type="submit" class="btn dark">Submit</button>
                                                                <button type="button" class="btn grey-salsa btn-outline">
                                                                    Cancel
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        {{-- END PORTLET--}}
                                    </div>
                                </div>
                            </div>
                            {{-- END PAGE CONTENT INNER --}}
                        </div>
                    </div>
                    {{-- END PAGE CONTENT BODY --}}
                    {{-- END CONTENT BODY --}}
                </div>
                {{-- END CONTENT --}}
                {{-- BEGIN QUICK SIDEBAR --}}
                @include('includes.sidebar')
                {{-- END QUICK SIDEBAR --}}
            </div>
            {{-- END CONTAINER --}}
        </div>
    </div>
@endsection