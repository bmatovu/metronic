@extends ("layouts.base")

@section('extra-css')
    @parent
    {{-- BEGIN PAGE LEVEL PLUGINS --}}
    <link href="{{ asset('plugins/cubeportfolio/css/cubeportfolio.css') }}" rel="stylesheet" type="text/css"/>
    {{-- END PAGE LEVEL PLUGINS --}}
    {{-- BEGIN THEME GLOBAL STYLES --}}
    <link href="{{ asset('css/plugins-md.min.css') }}" rel="stylesheet" type="text/css"/>
    {{-- END THEME GLOBAL STYLES --}}
    {{-- BEGIN PAGE LEVEL STYLES --}}
    <link href="{{ asset('pages/css/portfolio.min.css') }}" rel="stylesheet" type="text/css"/>
    {{-- END PAGE LEVEL STYLES --}}
@endsection

@push('extra-js')
{{-- BEGIN PAGE LEVEL PLUGINS --}}
<script src="{{ asset('plugins/cubeportfolio/js/jquery.cubeportfolio.min.js') }}" type="text/javascript"></script>
{{-- END PAGE LEVEL PLUGINS --}}
{{-- BEGIN PAGE LEVEL SCRIPTS --}}
<script src="{{ asset('pages/js/portfolio-3.min.js') }}" type="text/javascript"></script>
{{-- END PAGE LEVEL SCRIPTS --}}
@endpush

@section('main-content')
    <div class="page-wrapper-row full-height">
        <div class="page-wrapper-middle">
            {{-- BEGIN CONTAINER --}}
            <div class="page-container">
                {{-- BEGIN CONTENT --}}
                <div class="page-content-wrapper">
                    {{-- BEGIN CONTENT BODY --}}
                    {{-- BEGIN PAGE HEAD--}}
                    <div class="page-head">
                        <div class="container">
                            {{-- BEGIN PAGE TITLE --}}
                            <div class="page-title">
                                <h1>Portfolio 3
                                    <small>Portfolio 3 - Lightbox Grid</small>
                                </h1>
                            </div>
                            {{-- END PAGE TITLE --}}
                            {{-- BEGIN PAGE TOOLBAR --}}
                            @include('includes.toolbar')
                            {{-- END PAGE TOOLBAR --}}
                        </div>
                    </div>
                    {{-- END PAGE HEAD--}}
                    {{-- BEGIN PAGE CONTENT BODY --}}
                    <div class="page-content">
                        <div class="container">
                            {{-- BEGIN PAGE BREADCRUMBS --}}
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <a href="{{ url('/') }}">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <a href="#">Pages</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <a href="#">General</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Portfolio</span>
                                </li>
                            </ul>
                            {{-- END PAGE BREADCRUMBS --}}
                            {{-- BEGIN PAGE CONTENT INNER --}}
                            <div class="page-content-inner">
                                <div class="portfolio-content portfolio-3">
                                    <div class="clearfix">
                                        <div id="js-filters-lightbox-gallery1"
                                             class="cbp-l-filters-dropdown cbp-l-filters-dropdown-floated">
                                            <div class="cbp-l-filters-dropdownWrap border-grey-salsa">
                                                <div class="cbp-l-filters-dropdownHeader uppercase">Sort Gallery</div>
                                                <div class="cbp-l-filters-dropdownList">
                                                    <div data-filter="*"
                                                         class="cbp-filter-item-active cbp-filter-item uppercase"> All (
                                                        <div class="cbp-filter-counter"></div>
                                                        items)
                                                    </div>
                                                    <div data-filter=".identity" class="cbp-filter-item uppercase"> Identity
                                                        (
                                                        <div class="cbp-filter-counter"></div>
                                                        items)
                                                    </div>
                                                    <div data-filter=".web-design" class="cbp-filter-item uppercase"> Web
                                                        Design (
                                                        <div class="cbp-filter-counter"></div>
                                                        items)
                                                    </div>
                                                    <div data-filter=".print" class="cbp-filter-item uppercase"> Print (
                                                        <div class="cbp-filter-counter"></div>
                                                        items)
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="js-filters-lightbox-gallery2"
                                             class="cbp-l-filters-button cbp-l-filters-left">
                                            <div data-filter="*"
                                                 class="cbp-filter-item-active cbp-filter-item btn blue btn-outline uppercase">
                                                All
                                            </div>
                                            <div data-filter=".graphic"
                                                 class="cbp-filter-item btn blue btn-outline uppercase">Graphic
                                            </div>
                                            <div data-filter=".logos" class="cbp-filter-item btn blue btn-outline uppercase">
                                                Logo
                                            </div>
                                            <div data-filter=".motion"
                                                 class="cbp-filter-item btn blue btn-outline uppercase">Motion
                                            </div>
                                        </div>
                                    </div>
                                    <div id="js-grid-lightbox-gallery" class="cbp">
                                        <div class="cbp-item web-design graphic print motion">
                                            <a href="plugins/cubeportfolio/ajax/project3.html"
                                               class="cbp-caption cbp-singlePageInline"
                                               data-title="World Clock Widget<br>by Paul Flavius Nechita" rel="nofollow">
                                                <div class="cbp-caption-defaultWrap">
                                                    <img src="{{ asset('img/portfolio/600x600/01.jpg') }}" alt=""></div>
                                                <div class="cbp-caption-activeWrap">
                                                    <div class="cbp-l-caption-alignLeft">
                                                        <div class="cbp-l-caption-body">
                                                            <div class="cbp-l-caption-title">World Clock Widget</div>
                                                            <div class="cbp-l-caption-desc">by Paul Flavius Nechita</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="cbp-item web-design logos identity graphic">
                                            <a href="plugins/cubeportfolio/ajax/project4.html"
                                               class="cbp-caption cbp-singlePageInline"
                                               data-title="Bolt UI<br>by Tiberiu Neamu" rel="nofollow">
                                                <div class="cbp-caption-defaultWrap">
                                                    <img src="{{ asset('img/portfolio/600x600/1.jpg') }}" alt=""></div>
                                                <div class="cbp-caption-activeWrap">
                                                    <div class="cbp-l-caption-alignLeft">
                                                        <div class="cbp-l-caption-body">
                                                            <div class="cbp-l-caption-title">Bolt UI</div>
                                                            <div class="cbp-l-caption-desc">by Tiberiu Neamu</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="cbp-item graphic print identity">
                                            <a href="plugins/cubeportfolio/ajax/project3.html"
                                               class="cbp-caption cbp-singlePageInline"
                                               data-title="WhereTO App<br>by Tiberiu Neamu" rel="nofollow">
                                                <div class="cbp-caption-defaultWrap">
                                                    <img src="{{ asset('img/portfolio/600x600/02.jpg') }}" alt=""></div>
                                                <div class="cbp-caption-activeWrap">
                                                    <div class="cbp-l-caption-alignLeft">
                                                        <div class="cbp-l-caption-body">
                                                            <div class="cbp-l-caption-title">WhereTO App</div>
                                                            <div class="cbp-l-caption-desc">by Tiberiu Neamu</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="cbp-item web-design motion logos">
                                            <a href="plugins/cubeportfolio/ajax/project4.html"
                                               class="cbp-caption cbp-singlePageInline"
                                               data-title="iDevices<br>by Tiberiu Neamu" rel="nofollow">
                                                <div class="cbp-caption-defaultWrap">
                                                    <img src="{{ asset('img/portfolio/600x600/2.jpg') }}" alt=""></div>
                                                <div class="cbp-caption-activeWrap">
                                                    <div class="cbp-l-caption-alignLeft">
                                                        <div class="cbp-l-caption-body">
                                                            <div class="cbp-l-caption-title">iDevices</div>
                                                            <div class="cbp-l-caption-desc">by Tiberiu Neamu</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="cbp-item identity graphic print">
                                            <a href="plugins/cubeportfolio/ajax/project3.html"
                                               class="cbp-caption cbp-singlePageInline"
                                               data-title="Seemple* Music for iPad<br>by Tiberiu Neamu" rel="nofollow">
                                                <div class="cbp-caption-defaultWrap">
                                                    <img src="{{ asset('img/portfolio/600x600/03.jpg') }}" alt=""></div>
                                                <div class="cbp-caption-activeWrap">
                                                    <div class="cbp-l-caption-alignLeft">
                                                        <div class="cbp-l-caption-body">
                                                            <div class="cbp-l-caption-title">Seemple* Music for iPad</div>
                                                            <div class="cbp-l-caption-desc">by Tiberiu Neamu</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="cbp-item motion print logos web-design">
                                            <a href="plugins/cubeportfolio/ajax/project4.html"
                                               class="cbp-caption cbp-singlePageInline"
                                               data-title="Remind~Me Widget<br>by Tiberiu Neamu" rel="nofollow">
                                                <div class="cbp-caption-defaultWrap">
                                                    <img src="{{ asset('img/portfolio/600x600/3.jpg') }}" alt=""></div>
                                                <div class="cbp-caption-activeWrap">
                                                    <div class="cbp-l-caption-alignLeft">
                                                        <div class="cbp-l-caption-body">
                                                            <div class="cbp-l-caption-title">Remind~Me Widget</div>
                                                            <div class="cbp-l-caption-desc">by Tiberiu Neamu</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="cbp-item graphic logos">
                                            <a href="plugins/cubeportfolio/ajax/project3.html"
                                               class="cbp-caption cbp-singlePageInline"
                                               data-title="Workout Buddy<br>by Tiberiu Neamu" rel="nofollow">
                                                <div class="cbp-caption-defaultWrap">
                                                    <img src="{{ asset('img/portfolio/600x600/04.jpg') }}" alt=""></div>
                                                <div class="cbp-caption-activeWrap">
                                                    <div class="cbp-l-caption-alignLeft">
                                                        <div class="cbp-l-caption-body">
                                                            <div class="cbp-l-caption-title">Workout Buddy</div>
                                                            <div class="cbp-l-caption-desc">by Tiberiu Neamu</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="cbp-item identity print logos motion">
                                            <a href="plugins/cubeportfolio/ajax/project4.html"
                                               class="cbp-caption cbp-singlePageInline"
                                               data-title="Digital Menu<br>by Cosmin Capitanu" rel="nofollow">
                                                <div class="cbp-caption-defaultWrap">
                                                    <img src="{{ asset('img/portfolio/600x600/4.jpg') }}" alt=""></div>
                                                <div class="cbp-caption-activeWrap">
                                                    <div class="cbp-l-caption-alignLeft">
                                                        <div class="cbp-l-caption-body">
                                                            <div class="cbp-l-caption-title">Digital Menu</div>
                                                            <div class="cbp-l-caption-desc">by Cosmin Capitanu</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="cbp-item identity motion web-design">
                                            <a href="plugins/cubeportfolio/ajax/project3.html"
                                               class="cbp-caption cbp-singlePageInline"
                                               data-title="Holiday Selector<br>by Cosmin Capitanu" rel="nofollow">
                                                <div class="cbp-caption-defaultWrap">
                                                    <img src="{{ asset('img/portfolio/600x600/05.jpg') }}" alt=""></div>
                                                <div class="cbp-caption-activeWrap">
                                                    <div class="cbp-l-caption-alignLeft">
                                                        <div class="cbp-l-caption-body">
                                                            <div class="cbp-l-caption-title">Holiday Selector</div>
                                                            <div class="cbp-l-caption-desc">by Cosmin Capitanu</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div id="js-loadMore-lightbox-gallery" class="cbp-l-loadMore-button">
                                        <a href="plugins/cubeportfolio/ajax/loadMore3.html"
                                           class="cbp-l-loadMore-link btn grey-mint btn-outline" rel="nofollow">
                                            <span class="cbp-l-loadMore-defaultText">LOAD MORE</span>
                                            <span class="cbp-l-loadMore-loadingText">LOADING...</span>
                                            <span class="cbp-l-loadMore-noMoreLoading">NO MORE WORKS</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            {{-- END PAGE CONTENT INNER --}}
                        </div>
                    </div>
                    {{-- END PAGE CONTENT BODY --}}
                    {{-- END CONTENT BODY --}}
                </div>
                {{-- END CONTENT --}}
                {{-- BEGIN QUICK SIDEBAR --}}
                @include('includes.sidebar')
                {{-- END QUICK SIDEBAR --}}
            </div>
            {{-- END CONTAINER --}}
        </div>
    </div>
@endsection