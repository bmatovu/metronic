<div class="page-wrapper-row">

    <div class="page-wrapper-top">

        {{-- BEGIN HEADER --}}
        <div class="page-header">

            {{-- BEGIN HEADER TOP --}}
            <div class="page-header-top">
                <div class="@if(!empty(\Request::is('layout_fluid_page'))) container-fluid @else container @endif">

                    {{-- BEGIN LOGO --}}
                    <div class="page-logo">
                        <a href="{{ url('/') }}">
                            <img src="{{ asset('img/layout/logo-default.png') }}" alt="logo" class="logo-default">
                        </a>
                    </div>
                    {{-- END LOGO --}}

                    {{-- BEGIN RESPONSIVE MENU TOGGLER --}}
                    <a href="javascript:;" class="menu-toggler"></a>
                    {{-- END RESPONSIVE MENU TOGGLER --}}

                    {{-- BEGIN TOP NAVIGATION MENU --}}
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            {{-- BEGIN NOTIFICATION DROPDOWN --}}
                            <li class="dropdown dropdown-extended dropdown-notification @if(!\Request::is('layout_top_bar_light')) dropdown-dark @endif"
                                id="header_notification_bar">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                                   data-close-others="true">
                                    <i class="icon-bell"></i>
                                    <span class="badge badge-default">7</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="external">
                                        <h3>You have
                                            <strong>12 pending</strong> tasks</h3>
                                        <a href="{{ url('app_todo') }}">view all</a>
                                    </li>
                                    <li>
                                        <ul class="dropdown-menu-list scroller" style="height: 250px;"
                                            data-handle-color="#637283">
                                            <li>
                                                <a href="javascript:;">
                                                    <span class="time">just now</span>
                                                    <span class="details">
                                                        <span class="label label-sm label-icon label-success">
                                                            <i class="fa fa-plus"></i>
                                                        </span>
                                                        New user registered.
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <span class="time">3 mins</span>
                                                    <span class="details">
                                                        <span class="label label-sm label-icon label-danger">
                                                            <i class="fa fa-bolt"></i>
                                                        </span>
                                                        Server #12 overloaded.
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <span class="time">10 mins</span>
                                                    <span class="details">
                                                        <span class="label label-sm label-icon label-warning">
                                                            <i class="fa fa-bell-o"></i>
                                                        </span>
                                                        Server #2 not responding.
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <span class="time">14 hrs</span>
                                                    <span class="details">
                                                        <span class="label label-sm label-icon label-info">
                                                            <i class="fa fa-bullhorn"></i>
                                                        </span>
                                                        Application error.
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <span class="time">2 days</span>
                                                    <span class="details">
                                                        <span class="label label-sm label-icon label-danger">
                                                            <i class="fa fa-bolt"></i>
                                                        </span>
                                                        Database overloaded 68%.
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <span class="time">3 days</span>
                                                    <span class="details">
                                                        <span class="label label-sm label-icon label-danger">
                                                            <i class="fa fa-bolt"></i>
                                                        </span>
                                                        A user IP blocked.
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <span class="time">4 days</span>
                                                    <span class="details">
                                                        <span class="label label-sm label-icon label-warning">
                                                            <i class="fa fa-bell-o"></i>
                                                        </span>
                                                        Storage Server #4 not responding.
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <span class="time">5 days</span>
                                                    <span class="details">
                                                        <span class="label label-sm label-icon label-info">
                                                            <i class="fa fa-bullhorn"></i>
                                                        </span>
                                                        System Error.
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <span class="time">9 days</span>
                                                    <span class="details">
                                                        <span class="label label-sm label-icon label-danger">
                                                            <i class="fa fa-bolt"></i>
                                                        </span>
                                                        Storage server failed.
                                                    </span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            {{-- END NOTIFICATION DROPDOWN --}}
                            {{-- BEGIN TODO DROPDOWN --}}
                            <li class="dropdown dropdown-extended dropdown-tasks @if(!\Request::is('layout_top_bar_light')) dropdown-dark @endif"
                                id="header_task_bar">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                                   data-close-others="true">
                                    <i class="icon-calendar"></i>
                                    <span class="badge badge-default">3</span>
                                </a>
                                <ul class="dropdown-menu extended tasks">
                                    <li class="external">
                                        <h3>You have
                                            <strong>12 pending</strong> tasks</h3>
                                        <a href="{{ url('app_todo_2') }}">view all</a>
                                    </li>
                                    <li>
                                        <ul class="dropdown-menu-list scroller" style="height: 275px;"
                                            data-handle-color="#637283">
                                            <li>
                                                <a href="javascript:;">
                                                    <span class="task">
                                                        <span class="desc">New release v1.2 </span>
                                                        <span class="percent">30%</span>
                                                    </span>
                                                    <span class="progress">
                                                        <span style="width: 40%;" class="progress-bar progress-bar-success"
                                                              aria-valuenow="40" aria-valuemin="0" aria-valuemax="100">
                                                            <span class="sr-only">40% Complete</span>
                                                        </span>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <span class="task">
                                                        <span class="desc">Application deployment</span>
                                                        <span class="percent">65%</span>
                                                    </span>
                                                    <span class="progress">
                                                        <span style="width: 65%;" class="progress-bar progress-bar-danger"
                                                              aria-valuenow="65" aria-valuemin="0" aria-valuemax="100">
                                                            <span class="sr-only">65% Complete</span>
                                                        </span>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <span class="task">
                                                        <span class="desc">Mobile app release</span>
                                                        <span class="percent">98%</span>
                                                    </span>
                                                    <span class="progress">
                                                        <span style="width: 98%;" class="progress-bar progress-bar-success"
                                                              aria-valuenow="98" aria-valuemin="0" aria-valuemax="100">
                                                            <span class="sr-only">98% Complete</span>
                                                        </span>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <span class="task">
                                                        <span class="desc">Database migration</span>
                                                        <span class="percent">10%</span>
                                                    </span>
                                                    <span class="progress">
                                                        <span style="width: 10%;" class="progress-bar progress-bar-warning"
                                                              aria-valuenow="10" aria-valuemin="0" aria-valuemax="100">
                                                            <span class="sr-only">10% Complete</span>
                                                        </span>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <span class="task">
                                                        <span class="desc">Web server upgrade</span>
                                                        <span class="percent">58%</span>
                                                    </span>
                                                    <span class="progress">
                                                        <span style="width: 58%;" class="progress-bar progress-bar-info"
                                                              aria-valuenow="58" aria-valuemin="0" aria-valuemax="100">
                                                            <span class="sr-only">58% Complete</span>
                                                        </span>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <span class="task">
                                                        <span class="desc">Mobile development</span>
                                                        <span class="percent">85%</span>
                                                    </span>
                                                    <span class="progress">
                                                        <span style="width: 85%;" class="progress-bar progress-bar-success"
                                                              aria-valuenow="85" aria-valuemin="0" aria-valuemax="100">
                                                            <span class="sr-only">85% Complete</span>
                                                        </span>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <span class="task">
                                                        <span class="desc">New UI release</span>
                                                        <span class="percent">38%</span>
                                                    </span>
                                                    <span class="progress progress-striped">
                                                        <span style="width: 38%;" class="progress-bar progress-bar-important"
                                                              aria-valuenow="18" aria-valuemin="0" aria-valuemax="100">
                                                            <span class="sr-only">38% Complete</span>
                                                        </span>
                                                    </span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            {{-- END TODO DROPDOWN --}}
                            <li class="droddown dropdown-separator">
                                <span class="separator"></span>
                            </li>
                            {{-- BEGIN INBOX DROPDOWN --}}
                            <li class="dropdown dropdown-extended dropdown-inbox @if(!\Request::is('layout_top_bar_light')) dropdown-dark @endif"
                                id="header_inbox_bar">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                                   data-close-others="true">
                                    <span class="circle">3</span>
                                    <span class="corner"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="external">
                                        <h3>You have
                                            <strong>7 New</strong> Messages</h3>
                                        <a href="{{ url('app_inbox') }}">view all</a>
                                    </li>
                                    <li>
                                        <ul class="dropdown-menu-list scroller" style="height: 275px;"
                                            data-handle-color="#637283">
                                            <li>
                                                <a href="#">
                                                    <span class="photo">
                                                        <img src="{{ asset('img/layout/avatar2.jpg') }}" class="img-circle"
                                                             alt=""> </span>
                                                    <span class="subject">
                                                        <span class="from"> Lisa Wong </span>
                                                        <span class="time">Just Now </span>
                                                    </span>
                                                    <span class="message"> Vivamus sed auctor nibh congue nibh. auctor nibh auctor nibh... </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <span class="photo">
                                                        <img src="{{ asset('img/layout/avatar3.jpg') }}" class="img-circle"
                                                             alt=""> </span>
                                                    <span class="subject">
                                                        <span class="from"> Richard Doe </span>
                                                        <span class="time">16 mins </span>
                                                    </span>
                                                    <span class="message"> Vivamus sed congue nibh auctor nibh congue nibh. auctor nibh auctor nibh... </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <span class="photo">
                                                        <img src="{{ asset('img/layout/avatar1.jpg') }}" class="img-circle"
                                                             alt=""> </span>
                                                    <span class="subject">
                                                        <span class="from"> Bob Nilson </span>
                                                        <span class="time">2 hrs </span>
                                                    </span>
                                                    <span class="message"> Vivamus sed nibh auctor nibh congue nibh. auctor nibh auctor nibh... </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <span class="photo">
                                                        <img src="{{ asset('img/layout/avatar2.jpg') }}" class="img-circle"
                                                             alt=""> </span>
                                                    <span class="subject">
                                                        <span class="from"> Lisa Wong </span>
                                                        <span class="time">40 mins </span>
                                                    </span>
                                                    <span class="message"> Vivamus sed auctor 40% nibh congue nibh... </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                <span class="photo">
                                                    <img src="{{ asset('img/layout/avatar3.jpg') }}" class="img-circle"
                                                         alt=""> </span>
                                                <span class="subject">
                                                    <span class="from"> Richard Doe </span>
                                                    <span class="time">46 mins </span>
                                                </span>
                                                    <span class="message"> Vivamus sed congue nibh auctor nibh congue nibh. auctor nibh auctor nibh... </span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            {{-- END INBOX DROPDOWN --}}
                            {{-- BEGIN USER LOGIN DROPDOWN --}}
                            <li class="dropdown dropdown-user @if(!\Request::is('layout_top_bar_light')) dropdown-dark @endif">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                                   data-close-others="true">
                                    <img alt="" class="img-circle" src="{{ asset('img/layout/avatar9.jpg') }}">
                                    <span class="username username-hide-mobile">Nick</span>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <a href="{{ url('page_user_profile_1') }}">
                                            <i class="icon-user"></i> My Profile </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('app_calendar') }}">
                                            <i class="icon-calendar"></i> My Calendar </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('app_inbox') }}">
                                            <i class="icon-envelope-open"></i> My Inbox
                                            <span class="badge badge-danger"> 3 </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('app_todo_2') }}">
                                            <i class="icon-rocket"></i> My Tasks
                                            <span class="badge badge-success"> 7 </span>
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="{{ url('page_user_lock_1') }}">
                                            <i class="icon-lock"></i> Lock Screen </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('page_user_login_1') }}">
                                            <i class="icon-key"></i> Log Out </a>
                                    </li>
                                </ul>
                            </li>
                            {{-- END USER LOGIN DROPDOWN --}}
                            {{-- BEGIN QUICK SIDEBAR TOGGLER --}}
                            <li class="dropdown dropdown-extended quick-sidebar-toggler">
                                <span class="sr-only">Toggle Quick Sidebar</span>
                                <i class="icon-logout"></i>
                            </li>
                            {{-- END QUICK SIDEBAR TOGGLER --}}
                        </ul>
                    </div>
                    {{-- END TOP NAVIGATION MENU --}}
                </div>
            </div>
            {{-- END HEADER TOP --}}
            {{-- BEGIN HEADER MENU --}}
            <div class="page-header-menu">
                <div class="@if(!empty(\Request::is('layout_fluid_page'))) container-fluid @else container @endif">
                    {{-- BEGIN HEADER SEARCH BOX --}}
                    <form class="search-form" action="{{ url('page_general_search') }}" method="GET">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search" name="query">
                            <span class="input-group-btn">
                                <a href="javascript:;" class="btn submit">
                                    <i class="icon-magnifier"></i>
                                </a>
                            </span>
                        </div>
                    </form>
                    {{-- END HEADER SEARCH BOX --}}
                    {{-- BEGIN MEGA MENU --}}
                    {{-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background --}}
                    {{-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover --}}
                    <div class="hor-menu @if(\Request::is('layout_mega_menu_light')) hor-menu-light @endif">
                        <ul class="nav navbar-nav">
                            <li class="menu-dropdown classic-menu-dropdown ">
                                <a href="javascript:;"> Dashboard
                                    <span class="arrow"></span>
                                </a>
                                <ul class="dropdown-menu pull-left">
                                    <li class="{{ Request::is('/') ? 'active' : '' }}">
                                        <a href="{{ url('/') }}" class="nav-link  ">
                                            <i class="icon-bar-chart"></i> Default Dashboard
                                            <span class="badge badge-success">1</span>
                                        </a>
                                    </li>
                                    <li class="{{ Request::is('dashboard_2') ? 'active' : '' }}">
                                        <a href="{{ url('dashboard_2') }}" class="nav-link  ">
                                            <i class="icon-bulb"></i> Dashboard 2 </a>
                                    </li>
                                    <li class="{{ Request::is('dashboard_3') ? 'active' : '' }}">
                                        <a href="{{ url('dashboard_3') }}" class="nav-link  ">
                                            <i class="icon-graph"></i> Dashboard 3
                                            <span class="badge badge-danger">3</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-dropdown mega-menu-dropdown {{ Request::is('ui/*') ? 'active' : '' }}">
                                <a href="javascript:;"> UI Features
                                    <span class="arrow"></span>
                                </a>
                                <ul class="dropdown-menu" style="min-width: 710px">
                                    <li>
                                        <div class="mega-menu-content">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <ul class="mega-menu-submenu">
                                                        <li class="{{ Request::is('ui/ui_colors') ? 'active' : '' }}">
                                                            <a href="{{ url('ui_colors') }}"> Color Library </a>
                                                        </li>
                                                        <li class="{{ Request::is('ui/ui_general') ? 'active' : '' }}">
                                                            <a href="{{ url('ui_general') }}"> General Components </a>
                                                        </li>
                                                        <li class="{{ Request::is('ui/buttons') ? 'active' : '' }}">
                                                            <a href="{{ url('ui_buttons') }}"> Buttons </a>
                                                        </li>
                                                        <li class="{{ Request::is('ui/ui_buttons_spinner') ? 'active' : '' }}">
                                                            <a href="{{ url('ui_buttons_spinner') }}"> Spinner Buttons </a>
                                                        </li>
                                                        <li class="{{ Request::is('ui/ui_confirmations') ? 'active' : '' }}">
                                                            <a href="{{ url('ui_confirmations') }}"> Popover
                                                                Confirmations </a>
                                                        </li>
                                                        <li class="{{ Request::is('ui/ui_icons') ? 'active' : '' }}">
                                                            <a href="{{ url('ui_icons') }}"> Font Icons </a>
                                                        </li>
                                                        <li class="{{ Request::is('ui/ui_socicons') ? 'active' : '' }}">
                                                            <a href="{{ url('ui_socicons') }}"> Social Icons </a>
                                                        </li>
                                                        <li class="{{ Request::is('ui/ui_typography') ? 'active' : '' }}">
                                                            <a href="{{ url('ui_typography') }}"> Typography </a>
                                                        </li>
                                                        <li class="{{ Request::is('ui/ui_tabs_accordions_navs') ? 'active' : '' }}">
                                                            <a href="{{ url('ui_tabs_accordions_navs') }}"> Tabs, Accordions
                                                                & Navs </a>
                                                        </li>
                                                        <li class="{{ Request::is('ui/ui_tree') ? 'active' : '' }}">
                                                            <a href="{{ url('ui_tree') }}"> Tree View </a>
                                                        </li>
                                                        <li class="{{ Request::is('ui/maps_google') ? 'active' : '' }}">
                                                            <a href="{{ url('maps_google') }}"> Google Maps </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4">
                                                    <ul class="mega-menu-submenu">
                                                        <li class="{{ Request::is('ui/maps_vector') ? 'active' : '' }}">
                                                            <a href="{{ url('maps_vector') }}"> Vector Maps </a>
                                                        </li>
                                                        <li class="{{ Request::is('ui/ui_timeline') ? 'active' : '' }}">
                                                            <a href="{{ url('ui_timeline') }}"> Timeline 1 </a>
                                                        </li>
                                                        <li class="{{ Request::is('ui/ui_timeline_2') ? 'active' : '' }}">
                                                            <a href="{{ url('ui_timeline_2') }}"> Timeline 2 </a>
                                                        </li>
                                                        <li class="{{ Request::is('ui/ui_timeline_horizontal') ? 'active' : '' }}">
                                                            <a href="{{ url('ui_timeline_horizontal') }}"> Horizontal
                                                                Timeline </a>
                                                        </li>
                                                        <li class="{{ Request::is('ui/ui_page_progress_style_1') ? 'active' : '' }}">
                                                            <a href="{{ url('ui_page_progress_style_1') }}"> Page Progress
                                                                Bar - Flash </a>
                                                        </li>
                                                        <li class="{{ Request::is('ui/ui_page_progress_style_2') ? 'active' : '' }}">
                                                            <a href="{{ url('ui_page_progress_style_2') }}"> Page Progress
                                                                Bar - Big Counter </a>
                                                        </li>
                                                        <li class="{{ Request::is('ui/ui_blockui') ? 'active' : '' }}">
                                                            <a href="{{ url('ui_blockui') }}"> Block UI </a>
                                                        </li>
                                                        <li class="{{ Request::is('ui/ui_bootstrap_growl') ? 'active' : '' }}">
                                                            <a href="{{ url('ui_bootstrap_growl') }}"> Bootstrap Growl
                                                                Notifications </a>
                                                        </li>
                                                        <li class="{{ Request::is('ui/ui_notific8') ? 'active' : '' }}">
                                                            <a href="{{ url('ui_notific8') }}"> Notific8 Notifications </a>
                                                        </li>
                                                        <li class="{{ Request::is('ui/ui_toastr') ? 'active' : '' }}">
                                                            <a href="{{ url('ui_toastr') }}"> Toastr Notifications </a>
                                                        </li>
                                                        <li class="{{ Request::is('ui/ui_bootbox') ? 'active' : '' }}">
                                                            <a href="{{ url('ui_bootbox') }}"> Bootbox Dialogs </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4">
                                                    <ul class="mega-menu-submenu">
                                                        <li class="{{ Request::is('ui/ui_alerts_api') ? 'active' : '' }}">
                                                            <a href="{{ url('ui_alerts_api') }}"> Metronic Alerts API </a>
                                                        </li>
                                                        <li class="{{ Request::is('ui/ui_session_timeout') ? 'active' : '' }}">
                                                            <a href="{{ url('ui_session_timeout') }}"> Session Timeout </a>
                                                        </li>
                                                        <li class="{{ Request::is('ui/ui_idle_timeout') ? 'active' : '' }}">
                                                            <a href="{{ url('ui_idle_timeout') }}"> User Idle Timeout </a>
                                                        </li>
                                                        <li class="{{ Request::is('ui/ui_modals') ? 'active' : '' }}">
                                                            <a href="{{ url('ui_modals') }}"> Modals </a>
                                                        </li>
                                                        <li class="{{ Request::is('ui/ui_extended_modals') ? 'active' : '' }}">
                                                            <a href="{{ url('ui_extended_modals') }}"> Extended Modals </a>
                                                        </li>
                                                        <li class="{{ Request::is('ui/ui_tiles') ? 'active' : '' }}">
                                                            <a href="{{ url('ui_tiles') }}"> Tiles </a>
                                                        </li>
                                                        <li class="{{ Request::is('ui/ui_datepaginator') ? 'active' : '' }}">
                                                            <a href="{{ url('ui_datepaginator') }}"> Date Paginator </a>
                                                        </li>
                                                        <li class="{{ Request::is('ui/ui_nestable') ? 'active' : '' }}">
                                                            <a href="{{ url('ui_nestable') }}"> Nestable List </a>
                                                        </li>
                                                        <li class="{{ Request::is('ui/ui_confirmations') ? 'active' : '' }}">
                                                            <a href="{{ url('ui_confirmations') }}"> Confirmation Dialogs
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-dropdown classic-menu-dropdown {{ Request::is('layout/*') ? 'active' : '' }}">
                                <a href="javascript:;"> Layouts
                                    <span class="arrow"></span>
                                </a>
                                <ul class="dropdown-menu pull-left">
                                    <li class="{{ Request::is('layout/layout_mega_menu_light') ? 'active' : '' }}">
                                        <a href="{{ url('layout_mega_menu_light') }}" class="nav-link  "> Light Mega
                                            Menu </a>
                                    </li>
                                    <li class="{{ Request::is('layout/layout_top_bar_light') ? 'active' : '' }}">
                                        <a href="{{ url('layout_top_bar_light') }}" class="nav-link  "> Light Top Bar
                                            Dropdowns </a>
                                    </li>
                                    <li class="{{ Request::is('layout/layout_fluid_page') ? 'active' : '' }}">
                                        <a href="{{ url('layout_fluid_page') }}" class="nav-link  "> Fluid Page </a>
                                    </li>
                                    <li class="{{ Request::is('layout/layout_top_bar_fixed') ? 'active' : '' }}">
                                        <a href="{{ url('layout_top_bar_fixed') }}" class="nav-link  "> Fixed Top Bar </a>
                                    </li>
                                    <li class="{{ Request::is('layout/layout_mega_menu_fixed') ? 'active' : '' }}">
                                        <a href="{{ url('layout_mega_menu_fixed') }}" class="nav-link  "> Fixed Mega
                                            Menu </a>
                                    </li>
                                    <li class="{{ Request::is('layout/layout_disabled_menu') ? 'active' : '' }}">
                                        <a href="{{ url('layout_disabled_menu') }}" class="nav-link  "> Disabled Menu
                                            Links </a>
                                    </li>
                                    <li class="{{ Request::is('layout/layout_blank_page') ? 'active' : '' }}">
                                        <a href="{{ url('layout_blank_page') }}" class="nav-link  active"> Blank Page </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-dropdown mega-menu-dropdown mega-menu-full {{ Request::is('componets/*') ? 'active'
                            : '' }}">
                                <a href="javascript:;"> Components
                                    <span class="arrow"></span>
                                </a>
                                <ul class="dropdown-menu" style="">
                                    <li>
                                        <div class="mega-menu-content">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <ul class="mega-menu-submenu">
                                                        <li class="">
                                                            <h3>Components 1</h3>
                                                        </li>
                                                        <li class="{{ Request::is('componets/components_date_time_pickers') ? 'active' : '' }}">
                                                            <a href="{{ url('components_date_time_pickers') }}"> Date & Time
                                                                Pickers </a>
                                                        </li>
                                                        <li class="{{ Request::is('componets/components_color_pickers') ? 'active' : '' }}">
                                                            <a href="{{ url('components_color_pickers') }}"> Color
                                                                Pickers </a>
                                                        </li>
                                                        <li class="{{ Request::is('componets/components_select2') ? 'active' : '' }}">
                                                            <a href="{{ url('components_select2') }}"> Select2 Dropdowns </a>
                                                        </li>
                                                        <li class="{{ Request::is('componets/components_bootstrap_select') ? 'active' : '' }}">
                                                            <a href="{{ url('components_bootstrap_select') }}"> Bootstrap
                                                                Select </a>
                                                        </li>
                                                        <li class="{{ Request::is('componets/components_multi_select') ? 'active' : '' }}">
                                                            <a href="{{ url('components_multi_select') }}"> Multi Select </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-3">
                                                    <ul class="mega-menu-submenu">
                                                        <li class="">
                                                            <h3>Components 2</h3>
                                                        </li>
                                                        <li class="{{ Request::is('components/components_bootstrap_select_splitter') ? 'active' : '' }}">
                                                            <a href="{{ url('components_bootstrap_select_splitter') }}">
                                                                Select Splitter </a>
                                                        </li>
                                                        <li class="{{ Request::is('components/components_typeahead') ? 'active' : '' }}">
                                                            <a href="{{ url('components_typeahead') }}"> Typeahead
                                                                Autocomplete </a>
                                                        </li>
                                                        <li class="{{ Request::is('components/components_bootstrap_tagsinput') ? 'active' : '' }}">
                                                            <a href="{{ url('components_bootstrap_tagsinput') }}"> Bootstrap
                                                                Tagsinput </a>
                                                        </li>
                                                        <li class="{{ Request::is('components/components_bootstrap_switch') ? 'active' : '' }}">
                                                            <a href="{{ url('components_bootstrap_switch') }}"> Bootstrap
                                                                Switch </a>
                                                        </li>
                                                        <li class="{{ Request::is('components/components_bootstrap_maxlength') ? 'active' : '' }}">
                                                            <a href="{{ url('components_bootstrap_maxlength') }}"> Bootstrap
                                                                Maxlength </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-3">
                                                    <ul class="mega-menu-submenu">
                                                        <li class="">
                                                            <h3>Components 3</h3>
                                                        </li>
                                                        <li class="{{ Request::is('components/components_bootstrap_fileinput') ? 'active' : '' }}">
                                                            <a href="{{ url('components_bootstrap_fileinput') }}"> Bootstrap
                                                                File Input </a>
                                                        </li>
                                                        <li class="{{ Request::is('components/components_bootstrap_touchspin') ? 'active' : '' }}">
                                                            <a href="{{ url('components_bootstrap_touchspin') }}"> Bootstrap
                                                                Touchspin </a>
                                                        </li>
                                                        <li class="{{ Request::is('components/components_form_tools') ? 'active' : '' }}">
                                                            <a href="{{ url('components_form_tools') }}"> Form Widgets &
                                                                Tools </a>
                                                        </li>
                                                        <li class="{{ Request::is('components/components_context_menu') ? 'active' : '' }}">
                                                            <a href="{{ url('components_context_menu') }}"> Context Menu </a>
                                                        </li>
                                                        <li class="{{ Request::is('components/components_editors') ? 'active' : '' }}">
                                                            <a href="{{ url('components_editors') }}"> Markdown & WYSIWYG
                                                                Editors </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-3">
                                                    <ul class="mega-menu-submenu">
                                                        <li class="">
                                                            <h3>Components 4</h3>
                                                        </li>
                                                        <li class="{{ Request::is('components/components_code_editors') ? 'active' : '' }}">
                                                            <a href="{{ url('components_code_editors') }}"> Code Editors </a>
                                                        </li>
                                                        <li class="{{ Request::is('components/components_ion_sliders') ? 'active' : '' }}">
                                                            <a href="{{ url('components_ion_sliders') }}"> Ion Range
                                                                Sliders </a>
                                                        </li>
                                                        <li class="{{ Request::is('components/components_noui_sliders') ? 'active' : '' }}">
                                                            <a href="{{ url('components_noui_sliders') }}"> NoUI Range
                                                                Sliders </a>
                                                        </li>
                                                        <li class="{{ Request::is('components/components_knob_dials') ? 'active' : '' }}">
                                                            <a href="{{ url('components_knob_dials') }}"> Knob Circle
                                                                Dials </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-dropdown classic-menu-dropdown ">
                                <a href="javascript:;"> More
                                    <span class="arrow"></span>
                                </a>
                                <ul class="dropdown-menu pull-left">
                                    <li class="dropdown-submenu ">
                                        <a href="javascript:;" class="nav-link nav-toggle ">
                                            <i class="icon-settings"></i> Form Stuff
                                            <span class="arrow"></span>
                                        </a>
                                        <ul class="dropdown-menu {{ Request::is('forms/*') ? 'active' : '' }}">
                                            <li class="{{ Request::is('forms/form_controls') ? 'active' : '' }}">
                                                <a href="{{ url('form_controls') }}" class="nav-link "> Bootstrap Form
                                                    <br>Controls </a>
                                            </li>
                                            <li class="{{ Request::is('forms/form_controls_md') ? 'active' : '' }}">
                                                <a href="{{ url('form_controls_md') }}" class="nav-link "> Material Design
                                                    <br>Form Controls </a>
                                            </li>
                                            <li class="{{ Request::is('forms/form_validation') ? 'active' : '' }}">
                                                <a href="{{ url('form_validation') }}" class="nav-link "> Form
                                                    Validation </a>
                                            </li>
                                            <li class="{{ Request::is('forms/form_validation_states_md') ? 'active' : '' }}">
                                                <a href="{{ url('form_validation_states_md') }}" class="nav-link "> Material
                                                    Design
                                                    <br>Form Validation States </a>
                                            </li>
                                            <li class="{{ Request::is('forms/form_validation_md') ? 'active' : '' }}">
                                                <a href="{{ url('form_validation_md') }}" class="nav-link "> Material Design
                                                    <br>Form Validation </a>
                                            </li>
                                            <li class="{{ Request::is('forms/form_layouts') ? 'active' : '' }}">
                                                <a href="{{ url('form_layouts') }}" class="nav-link "> Form Layouts </a>
                                            </li>
                                            <li class="{{ Request::is('forms/form_input_mask') ? 'active' : '' }}">
                                                <a href="{{ url('form_input_mask') }}" class="nav-link "> Form Input
                                                    Mask </a>
                                            </li>
                                            <li class="{{ Request::is('forms/form_editable') ? 'active' : '' }}">
                                                <a href="{{ url('form_editable') }}" class="nav-link "> Form X-editable </a>
                                            </li>
                                            <li class="{{ Request::is('forms/form_wizard') ? 'active' : '' }}">
                                                <a href="{{ url('form_wizard') }}" class="nav-link "> Form Wizard </a>
                                            </li>
                                            <li class="{{ Request::is('forms/form_icheck') ? 'active' : '' }}">
                                                <a href="{{ url('form_icheck') }}" class="nav-link "> iCheck Controls </a>
                                            </li>
                                            <li class="{{ Request::is('forms/form_image_crop') ? 'active' : '' }}">
                                                <a href="{{ url('form_image_crop') }}" class="nav-link "> Image Cropping </a>
                                            </li>
                                            <li class="{{ Request::is('forms/form_fileupload') ? 'active' : '' }}">
                                                <a href="{{ url('form_fileupload') }}" class="nav-link "> Multiple File
                                                    Upload </a>
                                            </li>
                                            <li class="{{ Request::is('forms/form_dropzone') ? 'active' : '' }}">
                                                <a href="{{ url('form_dropzone') }}" class="nav-link "> Dropzone File
                                                    Upload </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="dropdown-submenu {{ Request::is('tables/*') ? 'active' : '' }}">
                                        <a href="javascript:;" class="nav-link nav-toggle ">
                                            <i class="icon-briefcase"></i> Tables
                                            <span class="arrow"></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li class="{{ Request::is('tables/table_static_basic') ? 'active' : '' }}">
                                                <a href="{{ url('table_static_basic') }}" class="nav-link "> Basic
                                                    Tables </a>
                                            </li>
                                            <li class="{{ Request::is('tables/table_static_responsive') ? 'active' : '' }}">
                                                <a href="{{ url('table_static_responsive') }}" class="nav-link "> Responsive
                                                    Tables </a>
                                            </li>
                                            <li class="{{ Request::is('tables/table_bootstrap') ? 'active' : '' }}">
                                                <a href="{{ url('table_bootstrap') }}" class="nav-link "> Bootstrap
                                                    Tables </a>
                                            </li>
                                            <li class="dropdown-submenu {{ Request::is('tables/datatables/*') ? 'active' : '' }}">
                                                <a href="javascript:;" class="nav-link nav-toggle"> Datatables
                                                    <span class="arrow"></span>
                                                </a>
                                                <ul class="dropdown-menu">
                                                    <li class="{{ Request::is('tables/datatables/table_datatables_managed') ? 'active' : '' }}">
                                                        <a href="{{ url('table_datatables_managed') }}" class="nav-link ">
                                                            Managed Datatables </a>
                                                    </li>
                                                    <li class="{{ Request::is('tables/datatables/table_datatables_buttons') ? 'active' : '' }}">
                                                        <a href="{{ url('table_datatables_buttons') }}" class="nav-link ">
                                                            Buttons Extension </a>
                                                    </li>
                                                    <li class="{{ Request::is('tables/datatables/table_datatables_colreorder') ? 'active' : '' }}">
                                                        <a href="{{ url('table_datatables_colreorder') }}" class="nav-link ">
                                                            Colreorder Extension </a>
                                                    </li>
                                                    <li class="{{ Request::is('tables/datatables/table_datatables_rowreorder') ? 'active' : '' }}">
                                                        <a href="{{ url('table_datatables_rowreorder') }}" class="nav-link ">
                                                            Rowreorder Extension </a>
                                                    </li>
                                                    <li class="{{ Request::is('tables/datatables/table_datatables_scroller') ? 'active' : '' }}">
                                                        <a href="{{ url('table_datatables_scroller') }}" class="nav-link ">
                                                            Scroller Extension </a>
                                                    </li>
                                                    <li class="{{ Request::is('tables/datatables/table_datatables_fixedheader') ? 'active' : '' }}">
                                                        <a href="{{ url('table_datatables_fixedheader') }}"
                                                           class="nav-link "> FixedHeader Extension </a>
                                                    </li>
                                                    <li class="{{ Request::is('tables/datatables/table_datatables_responsive') ? 'active' : '' }}">
                                                        <a href="{{ url('table_datatables_responsive') }}" class="nav-link ">
                                                            Responsive Extension </a>
                                                    </li>
                                                    <li class="{{ Request::is('tables/datatables/table_datatables_editable') ? 'active' : '' }}">
                                                        <a href="{{ url('table_datatables_editable') }}" class="nav-link ">
                                                            Editable Datatables </a>
                                                    </li>
                                                    <li class="{{ Request::is('tables/datatables/table_datatables_ajax') ? 'active' : '' }}">
                                                        <a href="{{ url('table_datatables_ajax') }}" class="nav-link "> Ajax
                                                            Datatables </a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="dropdown-submenu {{ Request::is('portlet/*') ? 'active' : '' }}">
                                        <a href="?p=" class="nav-link nav-toggle ">
                                            <i class="icon-wallet"></i> Portlets
                                            <span class="arrow"></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li class="{{ Request::is('portlet/portlet_boxed') ? 'active' : '' }}">
                                                <a href="{{ url('portlet_boxed') }}" class="nav-link "> Boxed Portlets </a>
                                            </li>
                                            <li class="{{ Request::is('portlet/portlet_light') ? 'active' : '' }}">
                                                <a href="{{ url('portlet_light') }}" class="nav-link "> Light Portlets </a>
                                            </li>
                                            <li class="{{ Request::is('portlet/portlet_solid') ? 'active' : '' }}">
                                                <a href="{{ url('portlet_solid') }}" class="nav-link "> Solid Portlets </a>
                                            </li>
                                            <li class="{{ Request::is('portlet/portlet_ajax') ? 'active' : '' }}">
                                                <a href="{{ url('portlet_ajax') }}" class="nav-link "> Ajax Portlets </a>
                                            </li>
                                            <li class="{{ Request::is('portlet/portlet_draggable') ? 'active' : '' }}">
                                                <a href="{{ url('portlet_draggable') }}" class="nav-link "> Draggable
                                                    Portlets </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="dropdown-submenu ">
                                        <a href="?p=" class="nav-link nav-toggle {{ Request::is('elements/*') ? 'active' :
                                         ''
                                        }}">
                                            <i class="icon-settings"></i> Elements
                                            <span class="arrow"></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li class="{{ Request::is('elements/elements_steps') ? 'active' : '' }}">
                                                <a href="{{ url('elements_steps') }}" class="nav-link "> Steps </a>
                                            </li>
                                            <li class="{{ Request::is('elements/elements_lists') ? 'active' : '' }}">
                                                <a href="{{ url('elements_lists') }}" class="nav-link "> Lists </a>
                                            </li>
                                            <li class="{{ Request::is('elements/elements_ribbons') ? 'active' : '' }}">
                                                <a href="{{ url('elements_ribbons') }}" class="nav-link "> Ribbons </a>
                                            </li>
                                            <li class="{{ Request::is('elements/elements_overlay') ? 'active' : '' }}">
                                                <a href="{{ url('elements_overlay') }}" class="nav-link "> Overlays </a>
                                            </li>
                                            <li class="{{ Request::is('elements/elements_cards') ? 'active' : '' }}">
                                                <a href="{{ url('elements_cards') }}" class="nav-link "> User Cards </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="dropdown-submenu {{ Request::is('charts/*') ? 'active' : '' }}">
                                        <a href="javascript:;" class="nav-link nav-toggle ">
                                            <i class="icon-bar-chart"></i> Charts
                                            <span class="arrow"></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li class="{{ Request::is('charts/charts_amcharts') ? 'active' : '' }}">
                                                <a href="{{ url('charts_amcharts') }}" class="nav-link "> amChart </a>
                                            </li>
                                            <li class="{{ Request::is('charts/charts_flotcharts') ? 'active' : '' }}">
                                                <a href="{{ url('charts_flotcharts') }}" class="nav-link "> Flot Charts </a>
                                            </li>
                                            <li class="{{ Request::is('charts/charts_flowchart') ? 'active' : '' }}">
                                                <a href="{{ url('charts_flowchart') }}" class="nav-link "> Flow Charts </a>
                                            </li>
                                            <li class="{{ Request::is('charts/charts_google') ? 'active' : '' }}">
                                                <a href="{{ url('charts_google') }}" class="nav-link "> Google Charts </a>
                                            </li>
                                            <li class="{{ Request::is('charts/charts_echarts') ? 'active' : '' }}">
                                                <a href="{{ url('charts_echarts') }}" class="nav-link "> eCharts </a>
                                            </li>
                                            <li class="{{ Request::is('charts/charts_morris') ? 'active' : '' }}">
                                                <a href="{{ url('charts_morris') }}" class="nav-link "> Morris Charts </a>
                                            </li>
                                            <li class="dropdown-submenu {{ Request::is('charts/highcharts/*') ? 'active' : '' }}">
                                                <a href="javascript:;" class="nav-link nav-toggle"> HighCharts
                                                    <span class="arrow"></span>
                                                </a>
                                                <ul class="dropdown-menu">
                                                    <li class="{{ Request::is('charts/highcharts') ? 'active' : '' }}">
                                                        <a href="{{ url('charts_highcharts') }}" class="nav-link "
                                                           target="_blank"> HighCharts </a>
                                                    </li>
                                                    <li class="{{ Request::is('charts/highcharts/charts_highstock') ? 'active' : '' }}">
                                                        <a href="{{ url('charts_highstock') }}" class="nav-link "
                                                           target="_blank"> HighStock </a>
                                                    </li>
                                                    <li class="{{ Request::is('charts/highcharts/charts_highmaps') ? 'active' : '' }}">
                                                        <a href="{{ url('charts_highmaps') }}" class="nav-link "
                                                           target="_blank"> HighMaps </a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class="menu-dropdown classic-menu-dropdown ">
                                <a href="javascript:;">
                                    <i class="icon-briefcase"></i> Pages
                                    <span class="arrow"></span>
                                </a>
                                <ul class="dropdown-menu pull-left">
                                    <li class="dropdown-submenu ">
                                        <a href="javascript:;" class="nav-link nav-toggle ">
                                            <i class="icon-basket"></i> eCommerce
                                            <span class="arrow"></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li class="{{ Request::is('') ? 'active' : '' }}">
                                                <a href="{{ url('ecommerce_index') }}" class="nav-link ">
                                                    <i class="icon-home"></i> Dashboard </a>
                                            </li>
                                            <li class="{{ Request::is('') ? 'active' : '' }}">
                                                <a href="{{ url('ecommerce_orders') }}" class="nav-link ">
                                                    <i class="icon-basket"></i> Orders </a>
                                            </li>
                                            <li class="{{ Request::is('') ? 'active' : '' }}">
                                                <a href="{{ url('ecommerce_orders_view') }}" class="nav-link ">
                                                    <i class="icon-tag"></i> Order View </a>
                                            </li>
                                            <li class="{{ Request::is('') ? 'active' : '' }}">
                                                <a href="{{ url('ecommerce_products') }}" class="nav-link ">
                                                    <i class="icon-graph"></i> Products </a>
                                            </li>
                                            <li class="{{ Request::is('') ? 'active' : '' }}">
                                                <a href="{{ url('ecommerce_products_edit') }}" class="nav-link ">
                                                    <i class="icon-graph"></i> Product Edit </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="dropdown-submenu ">
                                        <a href="javascript:;" class="nav-link nav-toggle ">
                                            <i class="icon-docs"></i> Apps
                                            <span class="arrow"></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li class="{{ Request::is('') ? 'active' : '' }}">
                                                <a href="{{ url('app_todo') }}" class="nav-link ">
                                                    <i class="icon-clock"></i> Todo 1 </a>
                                            </li>
                                            <li class="{{ Request::is('') ? 'active' : '' }}">
                                                <a href="{{ url('app_todo_2') }}" class="nav-link ">
                                                    <i class="icon-check"></i> Todo 2 </a>
                                            </li>
                                            <li class="{{ Request::is('') ? 'active' : '' }}">
                                                <a href="{{ url('app_inbox') }}" class="nav-link ">
                                                    <i class="icon-envelope"></i> Inbox </a>
                                            </li>
                                            <li class="{{ Request::is('') ? 'active' : '' }}">
                                                <a href="{{ url('app_ticket') }}" class="nav-link ">
                                                    <i class="icon-envelope"></i> Ticket </a>
                                            </li>
                                            <li class="{{ Request::is('') ? 'active' : '' }}">
                                                <a href="{{ url('app_calendar') }}" class="nav-link ">
                                                    <i class="icon-calendar"></i> Calendar </a>
                                            </li>
                                            <li class="{{ Request::is('') ? 'active' : '' }}">
                                                <a href="{{ url('app_ticket') }}" class="nav-link ">
                                                    <i class="icon-notebook"></i> Support </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="dropdown-submenu ">
                                        <a href="javascript:;" class="nav-link nav-toggle ">
                                            <i class="icon-user"></i> User
                                            <span class="arrow"></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li class="{{ Request::is('') ? 'active' : '' }}">
                                                <a href="{{ url('page_user_profile_1') }}" class="nav-link ">
                                                    <i class="icon-user"></i> Profile 1 </a>
                                            </li>
                                            <li class="{{ Request::is('') ? 'active' : '' }}">
                                                <a href="{{ url('page_user_profile_1_account') }}" class="nav-link ">
                                                    <i class="icon-user-female"></i> Profile 1 Account </a>
                                            </li>
                                            <li class="{{ Request::is('') ? 'active' : '' }}">
                                                <a href="{{ url('page_user_profile_1_help') }}" class="nav-link ">
                                                    <i class="icon-user-following"></i> Profile 1 Help </a>
                                            </li>
                                            <li class="{{ Request::is('') ? 'active' : '' }}">
                                                <a href="{{ url('page_user_profile_2') }}" class="nav-link ">
                                                    <i class="icon-users"></i> Profile 2 </a>
                                            </li>
                                            <li class="dropdown-submenu ">
                                                <a href="javascript:;" class="nav-link nav-toggle">
                                                    <i class="icon-notebook"></i> Login
                                                    <span class="arrow"></span>
                                                </a>
                                                <ul class="dropdown-menu">
                                                    <li class="{{ Request::is('') ? 'active' : '' }}">
                                                        <a href="{{ url('page_user_login_1') }}" class="nav-link "
                                                           target="_blank"> Login Page 1 </a>
                                                    </li>
                                                    <li class="{{ Request::is('') ? 'active' : '' }}">
                                                        <a href="{{ url('page_user_login_2') }}" class="nav-link "
                                                           target="_blank"> Login Page 2 </a>
                                                    </li>
                                                    <li class="{{ Request::is('') ? 'active' : '' }}">
                                                        <a href="{{ url('page_user_login_3') }}" class="nav-link "
                                                           target="_blank"> Login Page 3 </a>
                                                    </li>
                                                    <li class="{{ Request::is('') ? 'active' : '' }}">
                                                        <a href="{{ url('page_user_login_4') }}" class="nav-link "
                                                           target="_blank"> Login Page 4 </a>
                                                    </li>
                                                    <li class="{{ Request::is('') ? 'active' : '' }}">
                                                        <a href="{{ url('page_user_login_5') }}" class="nav-link "
                                                           target="_blank"> Login Page 5 </a>
                                                    </li>
                                                    <li class="{{ Request::is('') ? 'active' : '' }}">
                                                        <a href="{{ url('page_user_login_6') }}" class="nav-link "
                                                           target="_blank"> Login Page 6 </a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="{{ Request::is('') ? 'active' : '' }}">
                                                <a href="{{ url('page_user_lock_1') }}" class="nav-link " target="_blank">
                                                    <i class="icon-lock"></i> Lock Screen 1 </a>
                                            </li>
                                            <li class="{{ Request::is('') ? 'active' : '' }}">
                                                <a href="{{ url('page_user_lock_2') }}" class="nav-link " target="_blank">
                                                    <i class="icon-lock-open"></i> Lock Screen 2 </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="dropdown-submenu ">
                                        <a href="javascript:;" class="nav-link nav-toggle ">
                                            <i class="icon-social-dribbble"></i> General
                                            <span class="arrow"></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li class="{{ Request::is('') ? 'active' : '' }}">
                                                <a href="{{ url('page_general_about') }}" class="nav-link ">
                                                    <i class="icon-info"></i> About </a>
                                            </li>
                                            <li class="{{ Request::is('') ? 'active' : '' }}">
                                                <a href="{{ url('page_general_contact') }}" class="nav-link ">
                                                    <i class="icon-call-end"></i> Contact </a>
                                            </li>
                                            <li class="dropdown-submenu ">
                                                <a href="javascript:;" class="nav-link nav-toggle">
                                                    <i class="icon-notebook"></i> Portfolio
                                                    <span class="arrow"></span>
                                                </a>
                                                <ul class="dropdown-menu">
                                                    <li class="{{ Request::is('') ? 'active' : '' }}">
                                                        <a href="{{ url('page_general_portfolio_1') }}" class="nav-link ">
                                                            Portfolio 1 </a>
                                                    </li>
                                                    <li class="{{ Request::is('') ? 'active' : '' }}">
                                                        <a href="{{ url('page_general_portfolio_2') }}" class="nav-link ">
                                                            Portfolio 2 </a>
                                                    </li>
                                                    <li class="{{ Request::is('') ? 'active' : '' }}">
                                                        <a href="{{ url('page_general_portfolio_3') }}" class="nav-link ">
                                                            Portfolio 3 </a>
                                                    </li>
                                                    <li class="{{ Request::is('') ? 'active' : '' }}">
                                                        <a href="{{ url('page_general_portfolio_4') }}" class="nav-link ">
                                                            Portfolio 4 </a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="dropdown-submenu ">
                                                <a href="javascript:;" class="nav-link nav-toggle">
                                                    <i class="icon-magnifier"></i> Search
                                                    <span class="arrow"></span>
                                                </a>
                                                <ul class="dropdown-menu">
                                                    <li class="{{ Request::is('') ? 'active' : '' }}">
                                                        <a href="{{ url('page_general_search') }}" class="nav-link "> Search
                                                            1 </a>
                                                    </li>
                                                    <li class="{{ Request::is('') ? 'active' : '' }}">
                                                        <a href="{{ url('page_general_search_2') }}" class="nav-link ">
                                                            Search 2 </a>
                                                    </li>
                                                    <li class="{{ Request::is('') ? 'active' : '' }}">
                                                        <a href="{{ url('page_general_search_3') }}" class="nav-link ">
                                                            Search 3 </a>
                                                    </li>
                                                    <li class="{{ Request::is('') ? 'active' : '' }}">
                                                        <a href="{{ url('page_general_search_4') }}" class="nav-link ">
                                                            Search 4 </a>
                                                    </li>
                                                    <li class="{{ Request::is('') ? 'active' : '' }}">
                                                        <a href="{{ url('page_general_search_5') }}" class="nav-link ">
                                                            Search 5 </a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="{{ Request::is('') ? 'active' : '' }}">
                                                <a href="{{ url('page_general_pricing') }}" class="nav-link ">
                                                    <i class="icon-tag"></i> Pricing </a>
                                            </li>
                                            <li class="{{ Request::is('') ? 'active' : '' }}">
                                                <a href="{{ url('page_general_faq') }}" class="nav-link ">
                                                    <i class="icon-wrench"></i> FAQ </a>
                                            </li>
                                            <li class="{{ Request::is('') ? 'active' : '' }}">
                                                <a href="{{ url('page_general_blog') }}" class="nav-link ">
                                                    <i class="icon-pencil"></i> Blog </a>
                                            </li>
                                            <li class="{{ Request::is('') ? 'active' : '' }}">
                                                <a href="{{ url('page_general_blog_post') }}" class="nav-link ">
                                                    <i class="icon-note"></i> Blog Post </a>
                                            </li>
                                            <li class="{{ Request::is('') ? 'active' : '' }}">
                                                <a href="{{ url('page_general_invoice') }}" class="nav-link ">
                                                    <i class="icon-envelope"></i> Invoice </a>
                                            </li>
                                            <li class="{{ Request::is('') ? 'active' : '' }}">
                                                <a href="{{ url('page_general_invoice_2') }}" class="nav-link ">
                                                    <i class="icon-envelope"></i> Invoice 2 </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="dropdown-submenu ">
                                        <a href="javascript:;" class="nav-link nav-toggle ">
                                            <i class="icon-settings"></i> System
                                            <span class="arrow"></span>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li class="{{ Request::is('') ? 'active' : '' }}">
                                                <a href="{{ url('page_system_404_1') }}" class="nav-link "> 404 Page 1 </a>
                                            </li>
                                            <li class="{{ Request::is('') ? 'active' : '' }}">
                                                <a href="{{ url('page_system_404_2') }}" class="nav-link " target="_blank">
                                                    404 Page
                                                    2 </a>
                                            </li>
                                            <li class="{{ Request::is('') ? 'active' : '' }}">
                                                <a href="{{ url('page_system_404_3') }}" class="nav-link " target="_blank">
                                                    404 Page
                                                    3 </a>
                                            </li>
                                            <li class="{{ Request::is('') ? 'active' : '' }}">
                                                <a href="{{ url('page_system_500_1') }}" class="nav-link "> 500 Page 1 </a>
                                            </li>
                                            <li class="{{ Request::is('') ? 'active' : '' }}">
                                                <a href="{{ url('page_system_500_2') }}" class="nav-link " target="_blank">
                                                    500 Page
                                                    2 </a>
                                            </li>
                                            <li class="{{ Request::is('') ? 'active' : '' }}">
                                                <a href="{{ url('page_system_coming_soon') }}" class="nav-link "
                                                   target="_blank">
                                                    Coming Soon </a>
                                            </li>
                                            <li class="{{ Request::is('') ? 'active' : '' }}">
                                                <a href="{{ url('page_cookie_consent_1') }}" class="nav-link ">
                                                    Cookie Consent #1 </a>
                                            </li>
                                            <li class="{{ Request::is('') ? 'active' : '' }}">
                                                <a href="{{ url('page_cookie_consent_1') }}" class="nav-link ">
                                                    Cookie Consent #2 </a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            @if(\Request::is('layout_disabled_menu'))
                                <li class="menu-dropdown classic-menu-dropdown ">
                                    <a href="javascript:;" class="disabled-link disable-target">
                                        <i class="icon-briefcase "></i> Disabled
                                        <span class="arrow"></span>
                                    </a>
                                    <ul class="dropdown-menu pull-left">
                                        <li class="dropdown-submenu ">
                                            <a href="javascript:;" class="nav-link nav-toggle disabled-link disable-target">
                                                <i class="icon-basket"></i> Sample Link
                                                <span class="arrow"></span>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li class="{{ Request::is('') ? 'active' : '' }}">
                                                    <a href="{{ asset('ecommerce_index') }}" class="nav-link ">
                                                        <i class="icon-home"></i> Sample Link </a>
                                                </li>
                                                <li class="{{ Request::is('') ? 'active' : '' }}">
                                                    <a href="{{ asset('ecommerce_orders') }}"
                                                       class="nav-link disabled-link disable-target">
                                                        <i class="icon-basket"></i> Sample Link </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="dropdown-submenu ">
                                            <a href="javascript:;" class="nav-link nav-toggle ">
                                                <i class="icon-docs"></i> Sample Link
                                                <span class="arrow"></span>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li class="">
                                                    <a href="{{ url('app_todo') }}" class="nav-link ">
                                                        <i class="icon-clock"></i> Sample Link </a>
                                                </li>
                                                <li class="">
                                                    <a href="{{ url('app_todo_2') }}" class="nav-link disabled-link
                                                    disable-target">
                                                        <i class="icon-check"></i> Sample Link </a>
                                                </li>
                                                <li class="">
                                                    <a href="{{ url('app_inbox') }}" class="nav-link ">
                                                        <i class="icon-envelope"></i> Sample Link </a>
                                                </li>
                                                <li class="">
                                                    <a href="{{ url('app_calendar') }}" class="nav-link disabled-link
                                                    disable-target">
                                                        <i class="icon-calendar"></i> Sample Link </a>
                                                </li>
                                                <li class="">
                                                    <a href="?p=app_timeline" class="nav-link ">
                                                        <i class="icon-paper-plane"></i> Sample Link </a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            @endif
                        </ul>
                    </div>
                    {{-- END MEGA MENU --}}
                </div>
            </div>
            {{-- END HEADER MENU --}}
        </div>
        {{-- END HEADER --}}
    </div>
</div>