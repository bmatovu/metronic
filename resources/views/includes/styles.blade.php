<link rel="shortcut icon" href="{{ asset('favicon.ico') }}" />

{{-- BEGIN GLOBAL MANDATORY STYLES --}}
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
<link href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
{{-- <link href="{{ asset('plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" /> --}}
{{-- END GLOBAL MANDATORY STYLES --}}

{{-- BEGIN PAGE LEVEL PLUGINS --}}

{{-- END PAGE LEVEL PLUGINS --}}

{{-- BEGIN THEME GLOBAL STYLES --}}
<link href="{{ asset('css/components-md.min.css') }}" rel="stylesheet" id="style_components" type="text/css" />
{{-- END THEME GLOBAL STYLES --}}

{{-- BEGIN THEME LAYOUT STYLES --}}
<link href="{{ asset('css/layout/layout.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('css/layout/themes/default.min.css') }}" rel="stylesheet" type="text/css" id="style_color" />
{{-- <link href="{{ asset('css/layout/custom.min.css') }}" rel="stylesheet" type="text/css" /> --}}
{{-- END THEME LAYOUT STYLES --}}

{{-- BEGIN DYNAMIC CSS --}}
@yield('extra-css')
{{-- END DYNAMIC CSS --}}