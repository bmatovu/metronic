<meta charset="utf-8" />
<title>
    @if(!empty($title))
        {{ $title }}
    @else
        {{ config('app.name') }} | Metronic Template
    @endif
</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport" />
<meta content="Laravel: Metronic Theme #3" name="description" />
<meta content="Brian Matovu" name="author" />
{{--CSRF Token--}}
<meta name="csrf-token" content="{{ csrf_token() }}">

@include('includes.styles')