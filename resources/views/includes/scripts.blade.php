<!--[if lt IE 9]>
<script src="{{ asset('plugins/respond.min.js') }}"></script>
<script src="{{ asset('plugins/excanvas.min.js') }}"></script>
<![endif]-->

{{-- BEGIN CORE PLUGINS --}}
<script src="{{ asset('plugins/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
{{-- <script src="{{ asset('plugins/js.cookie.min.js') }}" type="text/javascript"></script> --}}
{{-- <script src="{{ asset('plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script> --}}
{{-- <script src="{{ asset('plugins/jquery.blockui.min.js') }}" type="text/javascript"></script> --}}
{{-- <script src="{{ asset('plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script> --}}
{{-- END CORE PLUGINS --}}

{{-- BEGIN THEME GLOBAL SCRIPTS --}}
<script src="{{ asset('js/app.min.js') }}" type="text/javascript"></script>
{{-- END THEME GLOBAL SCRIPTS --}}

{{-- BEGIN THEME LAYOUT SCRIPTS --}}
{{-- Show menu on minimized/phone display --}}
<script src="{{ asset('js/layout/layout.min.js') }}" type="text/javascript"></script>
{{-- Change Stylesheet Themes --}}
<script src="{{ asset('js/layout/demo.min.js') }}" type="text/javascript"></script>
{{-- Enable Quick Sidebar --}}
<script src="{{ asset('js/layout/quick-sidebar.min.js') }}" type="text/javascript"></script>
{{-- END THEME LAYOUT SCRIPTS --}}

{{-- BEGIN DYNAMIC SCRIPTS --}}
@stack('extra-js')
{{-- END DYNAMIC SCRIPTS --}}