@extends ("layouts.base")

@section('extra-css')
    @parent
    {{-- BEGIN GLOBAL MANDATORY STYLES --}}
    <link href="{{ asset('plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css"/>
    {{-- END GLOBAL MANDATORY STYLES --}}
    {{-- BEGIN PAGE LEVEL PLUGINS --}}
    <link href="{{ asset('plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css"/>
    {{-- END PAGE LEVEL PLUGINS --}}
    {{-- BEGIN THEME GLOBAL STYLES --}}
    <link href="{{ asset('css/plugins-md.min.css') }}" rel="stylesheet" type="text/css"/>
    {{-- END THEME GLOBAL STYLES --}}
@endsection

@push('extra-js')
{{-- BEGIN CORE PLUGINS --}}
<script src="{{ asset('plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
{{-- END CORE PLUGINS --}}
{{-- BEGIN PAGE LEVEL SCRIPTS --}}
<script src="{{ asset('plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
{{-- END PAGE LEVEL SCRIPTS --}}
@endpush

@section('main-content')
    <div class="page-wrapper-row full-height">
        <div class="page-wrapper-middle">
            {{-- BEGIN CONTAINER --}}
            <div class="page-container">
                {{-- BEGIN CONTENT --}}
                <div class="page-content-wrapper">
                    {{-- BEGIN CONTENT BODY --}}
                    {{-- BEGIN PAGE HEAD--}}
                    <div class="page-head">
                        <div class="container">
                            {{-- BEGIN PAGE TITLE --}}
                            <div class="page-title">
                                <h1>Bootstrap File Input
                                    <small>advanced bootstrap file input examples</small>
                                </h1>
                            </div>
                            {{-- END PAGE TITLE --}}
                            {{-- BEGIN PAGE TOOLBAR --}}
                            @include('includes.toolbar')
                            {{-- END PAGE TOOLBAR --}}
                        </div>
                    </div>
                    {{-- END PAGE HEAD--}}
                    {{-- BEGIN PAGE CONTENT BODY --}}
                    <div class="page-content">
                        <div class="container">
                            {{-- BEGIN PAGE BREADCRUMBS --}}
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <a href="{{ url('/') }}">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <a href="#">Components</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Bootstrap File Input</span>
                                </li>
                            </ul>
                            {{-- END PAGE BREADCRUMBS --}}
                            {{-- BEGIN PAGE CONTENT INNER --}}
                            <div class="page-content-inner">
                                <div class="note note-success">
                                    <h3>Bootstrap File Input</h3>

                                    <p> The file input plugin allows you to create a visually appealing file or image input
                                        widgets. For more info please check out
                                        <a href="http://www.jasny.net/bootstrap/javascript/#fileinput" target="_blank">the
                                            official documentation</a>. </p>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        {{-- BEGIN PORTLET--}}
                                        <div class="portlet light form-fit ">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="icon-settings font-green"></i>
                                                    <span class="caption-subject font-green sbold uppercase">Advanced File Input</span>
                                                </div>
                                                <div class="actions">
                                                    <input type="checkbox" class="make-switch" checked data-on="success"
                                                           data-on-color="success" data-off-color="warning"
                                                           data-size="small"></div>
                                            </div>
                                            <div class="portlet-body form">
                                                {{-- BEGIN FORM--}}
                                                <form action="#" class="form-horizontal form-bordered">
                                                    <div class="form-body">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Default1</label>

                                                            <div class="col-md-3">
                                                                <div class="fileinput fileinput-new"
                                                                     data-provides="fileinput">
                                                                    <div class="input-group input-large">
                                                                        <div class="form-control uneditable-input input-fixed input-medium"
                                                                             data-trigger="fileinput">
                                                                            <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                                                            <span class="fileinput-filename"> </span>
                                                                        </div>
                                                                                <span class="input-group-addon btn default btn-file">
                                                                                    <span class="fileinput-new"> Select file </span>
                                                                                    <span class="fileinput-exists"> Change </span>
                                                                                    <input type="file" name="..."> </span>
                                                                        <a href="javascript:;"
                                                                           class="input-group-addon btn red fileinput-exists"
                                                                           data-dismiss="fileinput"> Remove </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Without input</label>

                                                            <div class="col-md-9">
                                                                <div class="fileinput fileinput-new"
                                                                     data-provides="fileinput">
                                                                            <span class="btn green btn-file">
                                                                                <span class="fileinput-new"> Select file </span>
                                                                                <span class="fileinput-exists"> Change </span>
                                                                                <input type="file" name="..."> </span>
                                                                    <span class="fileinput-filename"> </span> &nbsp;
                                                                    <a href="javascript:;" class="close fileinput-exists"
                                                                       data-dismiss="fileinput"> </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group ">
                                                            <label class="control-label col-md-3">Image Upload #1</label>

                                                            <div class="col-md-9">
                                                                <div class="fileinput fileinput-new"
                                                                     data-provides="fileinput">
                                                                    <div class="fileinput-preview thumbnail"
                                                                         data-trigger="fileinput"
                                                                         style="width: 200px; height: 150px;"></div>
                                                                    <div>
                                                                                <span class="btn red btn-outline btn-file">
                                                                                    <span class="fileinput-new"> Select image </span>
                                                                                    <span class="fileinput-exists"> Change </span>
                                                                                    <input type="file" name="..."> </span>
                                                                        <a href="javascript:;"
                                                                           class="btn red fileinput-exists"
                                                                           data-dismiss="fileinput"> Remove </a>
                                                                    </div>
                                                                </div>
                                                                <div class="clearfix margin-top-10">
                                                                    <span class="label label-success">NOTE!</span> Image
                                                                    preview only works in IE10+, FF3.6+, Safari6.0+,
                                                                    Chrome6.0+ and Opera11.1+. In older browsers the filename
                                                                    is shown instead.
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group last">
                                                            <label class="control-label col-md-3">Image Upload #2</label>

                                                            <div class="col-md-9">
                                                                <div class="fileinput fileinput-new"
                                                                     data-provides="fileinput">
                                                                    <div class="fileinput-new thumbnail"
                                                                         style="width: 200px; height: 150px;">
                                                                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image"
                                                                             alt=""/></div>
                                                                    <div class="fileinput-preview fileinput-exists thumbnail"
                                                                         style="max-width: 200px; max-height: 150px;"></div>
                                                                    <div>
                                                                                <span class="btn default btn-file">
                                                                                    <span class="fileinput-new"> Select image </span>
                                                                                    <span class="fileinput-exists"> Change </span>
                                                                                    <input type="file" name="..."> </span>
                                                                        <a href="javascript:;"
                                                                           class="btn red fileinput-exists"
                                                                           data-dismiss="fileinput"> Remove </a>
                                                                    </div>
                                                                </div>
                                                                <div class="clearfix margin-top-10">
                                                                    <span class="label label-danger">NOTE!</span> Image
                                                                    preview only works in IE10+, FF3.6+, Safari6.0+,
                                                                    Chrome6.0+ and Opera11.1+. In older browsers the filename
                                                                    is shown instead.
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-offset-3 col-md-9">
                                                                <a href="javascript:;" class="btn green">
                                                                    <i class="fa fa-check"></i> Submit</a>
                                                                <a href="javascript:;" class="btn btn-outline grey-salsa">Cancel</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                                {{-- END FORM--}}
                                            </div>
                                        </div>
                                        {{-- END PORTLET--}}
                                    </div>
                                </div>
                            </div>
                            {{-- END PAGE CONTENT INNER --}}
                        </div>
                    </div>
                    {{-- END PAGE CONTENT BODY --}}
                    {{-- END CONTENT BODY --}}
                </div>
                {{-- END CONTENT --}}
                {{-- BEGIN QUICK SIDEBAR --}}
                @include('includes.sidebar')
                {{-- END QUICK SIDEBAR --}}
            </div>
            {{-- END CONTAINER --}}
        </div>
    </div>
@endsection