@extends ("layouts.base")

@section('extra-css')
    @parent
    {{-- BEGIN PAGE LEVEL PLUGINS --}}
    <link href="{{ asset('plugins/typeahead/typeahead.css') }}" rel="stylesheet" type="text/css"/>
    {{-- END PAGE LEVEL PLUGINS --}}
    {{-- BEGIN THEME GLOBAL STYLES --}}
    <link href="{{ asset('css/plugins-md.min.css') }}" rel="stylesheet" type="text/css"/>
    {{-- END THEME GLOBAL STYLES --}}
@endsection

@push('extra-js')
{{-- BEGIN PAGE LEVEL PLUGINS --}}
<script src="{{ asset('plugins/typeahead/handlebars.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/typeahead/typeahead.bundle.min.js') }}" type="text/javascript"></script>
{{-- END PAGE LEVEL PLUGINS --}}
{{-- BEGIN PAGE LEVEL SCRIPTS --}}
<script src="{{ asset('pages/js/components-typeahead.min.js') }}" type="text/javascript"></script>
{{-- END PAGE LEVEL SCRIPTS --}}
@endpush

@section('main-content')
    <div class="page-wrapper-row full-height">
        <div class="page-wrapper-middle">
            {{-- BEGIN CONTAINER --}}
            <div class="page-container">
                {{-- BEGIN CONTENT --}}
                <div class="page-content-wrapper">
                    {{-- BEGIN CONTENT BODY --}}
                    {{-- BEGIN PAGE HEAD--}}
                    <div class="page-head">
                        <div class="container">
                            {{-- BEGIN PAGE TITLE --}}
                            <div class="page-title">
                                <h1>Typeahead Input Autocomplete
                                    <small>twitter input auto complete</small>
                                </h1>
                            </div>
                            {{-- END PAGE TITLE --}}
                            {{-- BEGIN PAGE TOOLBAR --}}
                            @include('includes.toolbar')
                            {{-- END PAGE TOOLBAR --}}
                        </div>
                    </div>
                    {{-- END PAGE HEAD--}}
                    {{-- BEGIN PAGE CONTENT BODY --}}
                    <div class="page-content">
                        <div class="container">
                            {{-- BEGIN PAGE BREADCRUMBS --}}
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <a href="{{ url('/') }}">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <a href="#">Components</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Typeahead Autocomplete</span>
                                </li>
                            </ul>
                            {{-- END PAGE BREADCRUMBS --}}
                            {{-- BEGIN PAGE CONTENT INNER --}}
                            <div class="page-content-inner">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="note note-success">
                                            <h3>typeahead.js</h3>

                                            <p> A flexible JavaScript library that provides a strong foundation for building
                                                robust typeaheads. For more info check
                                                <a href="https://twitter.github.io/typeahead.js/" target="_blank">the
                                                    official documentation</a>. </p>
                                        </div>
                                        {{-- BEGIN PORTLET--}}
                                        <div class="portlet light form-fit ">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="icon-social-dribbble font-dark"></i>
                                                    <span class="caption-subject font-dark bold uppercase">Twitter Auto Complete(typeahead.js)</span>
                                                </div>
                                                <div class="actions">
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-cloud-upload"></i>
                                                    </a>
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-wrench"></i>
                                                    </a>
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-trash"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body form">
                                                <form action="#" id="form-username" class="form-horizontal form-bordered">
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Basic Auto Complete</label>

                                                        <div class="col-sm-4">
                                                            <div class="input-group">
                                                                        <span class="input-group-addon">
                                                                            <i class="fa fa-user"></i>
                                                                        </span>
                                                                <input type="text" id="typeahead_example_1"
                                                                       name="typeahead_example_1" class="form-control"/>
                                                            </div>
                                                            <p class="help-block"> E.g: metronic, keenthemes </p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Country Auto Complete</label>

                                                        <div class="col-sm-4">
                                                            <div class="input-group">
                                                                        <span class="input-group-addon">
                                                                            <i class="fa fa-search"></i>
                                                                        </span>
                                                                <input type="text" id="typeahead_example_2"
                                                                       name="typeahead_example_2" class="form-control"/>
                                                            </div>
                                                            <p class="help-block"> E.g: USA, Malaysia. Prefetch from JSON
                                                                source</code>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Custom Template</label>

                                                        <div class="col-sm-4">
                                                            <div class="input-group">
                                                                        <span class="input-group-addon">
                                                                            <i class="fa fa-cogs"></i>
                                                                        </span>
                                                                <input type="text" id="typeahead_example_3"
                                                                       name="typeahead_example_3" class="form-control"/>
                                                            </div>
                                                            <p class="help-block"> Uses a precompiled template to customize
                                                                look of suggestion.</code>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group ">
                                                        <label class="col-sm-3 control-label">Multiple Sections with
                                                            Headers</label>

                                                        <div class="col-sm-4">
                                                            <div class="input-group">
                                                                        <span class="input-group-addon">
                                                                            <i class="fa fa-check"></i>
                                                                        </span>
                                                                <input type="text" id="typeahead_example_4"
                                                                       name="typeahead_example_4" class="form-control"/>
                                                            </div>
                                                            <p class="help-block"> Two datasets that are prefetched, stored,
                                                                and searched on the client. Highlighting is enabled. </p>
                                                        </div>
                                                    </div>
                                                    <div class="form-group last">
                                                        <label class="control-label col-md-3">Modal</label>

                                                        <div class="col-md-4">
                                                            <a href="#myModal_autocomplete" role="button"
                                                               class="btn red btn-outline" data-toggle="modal"> View in
                                                                Modal </a>
                                                        </div>
                                                    </div>
                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-offset-3 col-md-9">
                                                                <button type="submit" class="btn green">
                                                                    <i class="fa fa-check"></i> Submit
                                                                </button>
                                                                <button type="button" class="btn btn-outline grey-salsa">
                                                                    Cancel
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                                <div id="myModal_autocomplete" class="modal fade" role="dialog"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                        aria-hidden="true"></button>
                                                                <h4 class="modal-title">Radio Switch in Modal</h4>
                                                            </div>
                                                            <div class="modal-body form">
                                                                <form action="#" class="form-horizontal form-row-seperated">
                                                                    <div class="form-group">
                                                                        <label class="col-sm-4 control-label">Basic Auto
                                                                            Complete</label>

                                                                        <div class="col-sm-8">
                                                                            <div class="input-group">
                                                                                        <span class="input-group-addon">
                                                                                            <i class="fa fa-user"></i>
                                                                                        </span>
                                                                                <input type="text"
                                                                                       id="typeahead_example_modal_1"
                                                                                       name="typeahead_example_modal_1"
                                                                                       class="form-control"/></div>
                                                                            <p class="help-block"> E.g: metronic,
                                                                                keenthemes. </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="col-sm-4 control-label">Country Auto
                                                                            Complete</label>

                                                                        <div class="col-sm-8">
                                                                            <div class="input-group">
                                                                                        <span class="input-group-addon">
                                                                                            <i class="fa fa-search"></i>
                                                                                        </span>
                                                                                <input type="text"
                                                                                       id="typeahead_example_modal_2"
                                                                                       name="typeahead_example_modal_2"
                                                                                       class="form-control"/></div>
                                                                            <p class="help-block"> E.g: USA, Malaysia.
                                                                                Prefetch from JSON source</code>
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="col-sm-4 control-label">Custom
                                                                            Template</label>

                                                                        <div class="col-sm-8">
                                                                            <div class="input-group">
                                                                                        <span class="input-group-addon">
                                                                                            <i class="fa fa-cogs"></i>
                                                                                        </span>
                                                                                <input type="text"
                                                                                       id="typeahead_example_modal_3"
                                                                                       name="typeahead_example_modal_3"
                                                                                       class="form-control"/></div>
                                                                            <p class="help-block"> Uses a precompiled
                                                                                template to customize look of
                                                                                suggestion.</code>
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group last">
                                                                        <label class="col-sm-4 control-label">Multiple
                                                                            Sections with Headers</label>

                                                                        <div class="col-sm-8">
                                                                            <div class="input-group">
                                                                                        <span class="input-group-addon">
                                                                                            <i class="fa fa-check"></i>
                                                                                        </span>
                                                                                <input type="text"
                                                                                       id="typeahead_example_modal_4"
                                                                                       name="typeahead_example_modal_4"
                                                                                       class="form-control"/></div>
                                                                            <p class="help-block"> Two datasets that are
                                                                                prefetched, stored, and searched on the
                                                                                client. Highlighting is enabled. </p>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn grey-salsa btn-outline"
                                                                        data-dismiss="modal">Close
                                                                </button>
                                                                <button type="button" class="btn green">
                                                                    <i class="fa fa-check"></i> Save changes
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- END PORTLET--}}
                                    </div>
                                </div>
                            </div>
                            {{-- END PAGE CONTENT INNER --}}
                        </div>
                    </div>
                    {{-- END PAGE CONTENT BODY --}}
                    {{-- END CONTENT BODY --}}
                </div>
                {{-- END CONTENT --}}
                {{-- BEGIN QUICK SIDEBAR --}}
                @include('includes.sidebar')
                {{-- END QUICK SIDEBAR --}}
            </div>
            {{-- END CONTAINER --}}
        </div>
    </div>
@endsection