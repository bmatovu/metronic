@extends ("layouts.base")

@section('extra-css')
    @parent
    {{-- BEGIN THEME GLOBAL STYLES --}}
    <link href="{{ asset('css/plugins-md.min.css') }}" rel="stylesheet" type="text/css"/>
    {{-- END THEME GLOBAL STYLES --}}
@endsection

@push('extra-js')
{{-- BEGIN PAGE LEVEL SCRIPTS --}}
<script src="{{ asset('pages/js/ui-blockui.min.js') }}" type="text/javascript"></script>
{{-- END PAGE LEVEL SCRIPTS --}}
@endpush

@section('main-content')
    <div class="page-wrapper-row full-height">
        <div class="page-wrapper-middle">
            {{-- BEGIN CONTAINER --}}
            <div class="page-container">
                {{-- BEGIN CONTENT --}}
                <div class="page-content-wrapper">
                    {{-- BEGIN CONTENT BODY --}}
                    {{-- BEGIN PAGE HEAD--}}
                    <div class="page-head">
                        <div class="container">
                            {{-- BEGIN PAGE TITLE --}}
                            <div class="page-title">
                                <h1>Block UI
                                    <small>block ui samples</small>
                                </h1>
                            </div>
                            {{-- END PAGE TITLE --}}
                            {{-- BEGIN PAGE TOOLBAR --}}
                            @include('includes.toolbar')
                            {{-- END PAGE TOOLBAR --}}
                        </div>
                    </div>
                    {{-- END PAGE HEAD--}}
                    {{-- BEGIN PAGE CONTENT BODY --}}
                    <div class="page-content">
                        <div class="container">
                            {{-- BEGIN PAGE BREADCRUMBS --}}
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <a href="{{ url('/') }}">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <a href="#">UI Features</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Block UI</span>
                                </li>
                            </ul>
                            {{-- END PAGE BREADCRUMBS --}}
                            {{-- BEGIN PAGE CONTENT INNER --}}
                            <div class="page-content-inner">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="portlet light " id="blockui_sample_1_portlet_body">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="icon-bubble font-green-sharp"></i>
                                                    <span class="caption-subject font-green-sharp sbold">Portlet Blocking</span>
                                                </div>
                                                <div class="actions">
                                                    <div class="btn-group">
                                                        <a class="btn green-haze btn-outline btn-circle btn-sm"
                                                           href="javascript:;" data-toggle="dropdown" data-hover="dropdown"
                                                           data-close-others="true"> Actions
                                                            <i class="fa fa-angle-down"></i>
                                                        </a>
                                                        <ul class="dropdown-menu pull-right">
                                                            <li>
                                                                <a href="javascript:;"> Option 1</a>
                                                            </li>
                                                            <li class="divider"></li>
                                                            <li>
                                                                <a href="javascript:;">Option 2</a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;">Option 3</a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;">Option 4</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <p> At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis
                                                    praesentium voluptatum deleniti atque corrupti quos dolores et quas
                                                    molestias excepturi sint occaecati cupiditate non provident,
                                                    similique. </p>

                                                <p> Aet accusamus et iusto odio dignissimos ducimus qui blanditiis
                                                    praesentium voluptatum. At vero eos et accusamus et iusto odio
                                                    dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque
                                                    corrupti quos dolores et. </p>

                                                <p>
                                                    <a href="javascript:;" class="btn btn-outline sbold blue-madison"
                                                       id="blockui_sample_1_1"> Block Portlet With Default Message </a>
                                                </p>

                                                <p>
                                                    <a href="javascript:;" class="btn btn-outline sbold red-intense"
                                                       id="blockui_sample_1_2"> Block Portlet With Boxed Message </a>
                                                </p>

                                                <p>
                                                    <a href="javascript:;" class="btn btn-outline sbold green-haze"
                                                       id="blockui_sample_1_3"> Block Portlet With CSS3 Animation</a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="portlet light ">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="icon-bubble font-red-mint"></i>
                                                    <span class="caption-subject font-red-mint sbold">Page Blocking</span>
                                                </div>
                                                <div class="actions">
                                                    <div class="btn-group">
                                                        <a class="btn dark btn-outline btn-circle btn-sm" href="javascript:;"
                                                           data-toggle="dropdown" data-hover="dropdown"
                                                           data-close-others="true"> Actions
                                                            <i class="fa fa-angle-down"></i>
                                                        </a>
                                                        <ul class="dropdown-menu pull-right">
                                                            <li>
                                                                <a href="javascript:;"> Option 1</a>
                                                            </li>
                                                            <li class="divider"></li>
                                                            <li>
                                                                <a href="javascript:;">Option 2</a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;">Option 3</a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;">Option 4</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <p>
                                                    <a href="javascript:;" class="btn btn-outline sbold green"
                                                       id="blockui_sample_2_1"> Block Page With Default Message </a>
                                                </p>

                                                <p>
                                                    <a href="javascript:;" class="btn btn-outline sbold yellow"
                                                       id="blockui_sample_2_2"> Block Page With Boxed Message </a>
                                                </p>

                                                <p>
                                                    <a href="javascript:;" class="btn btn-outline sbold red"
                                                       id="blockui_sample_2_3"> Block Page Without Background Overlay </a>
                                                </p>

                                                <p>
                                                    <a href="javascript:;" class="btn btn-outline sbold purple"
                                                       id="blockui_sample_2_4"> Block Page with CSS3 Animation </a>
                                                </p>

                                                <p> At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis
                                                    praesentium voluptatum deleniti atque corrupti quos dolores et quas
                                                    molestias excepturi sint occaecati cupiditate non provident,
                                                    similique. </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="portlet green-sharp box">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-cogs"></i>Element Blocking
                                                </div>
                                                <div class="tools">
                                                    <a href="javascript:;" class="collapse"> </a>
                                                    <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                    <a href="javascript:;" class="reload"> </a>
                                                    <a href="javascript:;" class="remove"> </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <p>
                                                    <a class="btn red" data-toggle="modal" href="#basic"> Block Modal
                                                        Content </a>
                                                </p>

                                                <div class="modal fade" id="basic" tabindex="-1" role="basic"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                        aria-hidden="true"></button>
                                                                <h4 class="modal-title">Modal Title</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>
                                                                    <a href="javascript:;" class="btn btn-outline sbold blue"
                                                                       id="blockui_sample_3_1_0"> Block Whole Modal </a>
                                                                </p>

                                                                <p>
                                                                    <a href="javascript:;"
                                                                       class="btn btn-outline sbold green"
                                                                       id="blockui_sample_3_1"> Block Below Element </a>
                                                                    <a href="javascript:;"
                                                                       class="btn btn-outline sbold default"
                                                                       id="blockui_sample_3_1_1"> Unblock Below Element </a>
                                                                </p>

                                                                <div id="blockui_sample_3_1_element">
                                                                    <p> At vero eos et accusamus et iusto odio dignissimos
                                                                        ducimus qui blanditiis praesentium voluptatum
                                                                        deleniti atque corrupti quos dolores et quas
                                                                        molestias excepturi sint occaecati
                                                                        cupiditate non provident, similique. </p>

                                                                    <p> At vero eos et accusamus et iusto odio dignissimos
                                                                        ducimus qui blanditiis praesentium voluptatum
                                                                        deleniti atque corrupti quos dolores et quas
                                                                        molestias excepturi sint occaecati
                                                                        cupiditate non provident, similique. </p>

                                                                    <p> At vero eos et accusamus et iusto odio dignissimos
                                                                        ducimus qui blanditiis praesentium voluptatum
                                                                        deleniti atque corrupti quos dolores et quas
                                                                        molestias excepturi sint occaecati
                                                                        cupiditate non provident, similique. </p>

                                                                    <p> At vero eos et accusamus et iusto odio dignissimos
                                                                        ducimus qui blanditiis praesentium voluptatum
                                                                        deleniti atque corrupti quos dolores et quas
                                                                        molestias excepturi sint occaecati
                                                                        cupiditate non provident, similique. </p>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-outline sbold red"
                                                                        data-dismiss="modal">Close
                                                                </button>
                                                                <button type="button" class="btn btn-outline sbold blue">Save
                                                                    changes
                                                                </button>
                                                            </div>
                                                        </div>
                                                        {{-- /.modal-content --}}
                                                    </div>
                                                    {{-- /.modal-dialog --}}
                                                </div>
                                                <p>
                                                    <a href="javascript:;" class="btn green" id="blockui_sample_3_2"> Block
                                                        Below Element </a>
                                                    <a href="javascript:;" class="btn default" id="blockui_sample_3_2_1">
                                                        Unblock Below Element </a>
                                                </p>

                                                <div id="blockui_sample_3_2_element">
                                                    <p> At vero eos et accusamus et iusto odio dignissimos ducimus qui
                                                        blanditiis praesentium voluptatum deleniti atque corrupti quos
                                                        dolores et quas molestias excepturi sint occaecati cupiditate non
                                                        provident, similique. </p>

                                                    <p> At vero eos et accusamus et iusto odio dignissimos ducimus qui
                                                        blanditiis praesentium voluptatum deleniti atque corrupti quos
                                                        dolores et quas molestias excepturi sint occaecati cupiditate non
                                                        provident, similique. </p>

                                                    <p> At vero eos et accusamus et iusto odio dignissimos ducimus qui
                                                        blanditiis praesentium voluptatum deleniti atque corrupti quos
                                                        dolores et quas molestias excepturi sint occaecati cupiditate non
                                                        provident, similique. </p>

                                                    <p> At vero eos et accusamus et iusto odio dignissimos ducimus qui
                                                        blanditiis praesentium voluptatum deleniti atque corrupti quos
                                                        dolores et quas molestias excepturi sint occaecati cupiditate non
                                                        provident, similique. </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="portlet red-mint box">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="fa fa-cogs"></i>Custom Messages
                                                </div>
                                                <div class="tools">
                                                    <a href="javascript:;" class="collapse"> </a>
                                                    <a href="#portlet-config" data-toggle="modal" class="config"> </a>
                                                    <a href="javascript:;" class="reload"> </a>
                                                    <a href="javascript:;" class="remove"> </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body" id="blockui_sample_4_portlet_body">
                                                <p>
                                                    <a href="javascript:;" class="btn blue btn-outline sbold "
                                                       id="blockui_sample_4_1"> Block Portlet With Custom Message </a>
                                                </p>

                                                <p>
                                                    <a href="javascript:;" class="btn yellow btn-outline sbold "
                                                       id="blockui_sample_4_2"> Block Portlet With Spinning Image Only </a>
                                                </p>

                                                <p>
                                                    <a href="javascript:;" class="btn purple btn-outline sbold "
                                                       id="blockui_sample_4_3"> Block Portlet With Text Only </a>
                                                </p>

                                                <div>
                                                    <p> At vero eos et accusamus et iusto odio dignissimos ducimus qui
                                                        blanditiis praesentium voluptatum deleniti atque corrupti quos
                                                        dolores et quas molestias excepturi sint occaecati cupiditate non
                                                        provident, similique. </p>

                                                    <p> At vero eos et accusamus et iusto odio dignissimos ducimus qui
                                                        blanditiis praesentium voluptatum deleniti atque corrupti quos
                                                        dolores et quas molestias excepturi sint occaecati cupiditate non
                                                        provident, similique. </p>

                                                    <p> At vero eos et accusamus et iusto odio dignissimos ducimus qui
                                                        blanditiis praesentium voluptatum deleniti atque corrupti quos
                                                        dolores et quas molestias excepturi sint occaecati cupiditate non
                                                        provident, similique. </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- END PAGE CONTENT INNER --}}
                        </div>
                    </div>
                    {{-- END PAGE CONTENT BODY --}}
                    {{-- END CONTENT BODY --}}
                </div>
                {{-- END CONTENT --}}
                {{-- BEGIN QUICK SIDEBAR --}}
                @include('includes.sidebar')
                {{-- END QUICK SIDEBAR --}}
            </div>
            {{-- END CONTAINER --}}
        </div>
    </div>
@endsection