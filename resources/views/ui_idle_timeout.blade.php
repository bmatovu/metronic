@extends ("layouts.base")

@section('extra-css')
    @parent
    {{-- BEGIN PAGE LEVEL PLUGINS --}}
    <link href="{{ asset('plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('plugins/bootstrap-modal/css/bootstrap-modal.css') }}" rel="stylesheet" type="text/css"/>
    {{-- END PAGE LEVEL PLUGINS --}}
    {{-- BEGIN THEME GLOBAL STYLES --}}
    <link href="{{ asset('css/plugins-md.min.css') }}" rel="stylesheet" type="text/css"/>
    {{-- END THEME GLOBAL STYLES --}}
@endsection

@push('extra-js')
{{-- BEGIN PAGE LEVEL PLUGINS --}}
<script src="{{ asset('plugins/jquery-idle-timeout/jquery.idletimeout.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/jquery-idle-timeout/jquery.idletimer.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/bootstrap-modal/js/bootstrap-modalmanager.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/bootstrap-modal/js/bootstrap-modal.js') }}" type="text/javascript"></script>
{{-- END PAGE LEVEL PLUGINS --}}
{{-- BEGIN PAGE LEVEL SCRIPTS --}}
<script src="{{ asset('pages/js/ui-idletimeout.min.js') }}" type="text/javascript"></script>
{{-- END PAGE LEVEL SCRIPTS --}}
@endpush

@section('main-content')
    <div class="page-wrapper-row full-height">
        <div class="page-wrapper-middle">
            {{-- BEGIN CONTAINER --}}
            <div class="page-container">
                {{-- BEGIN CONTENT --}}
                <div class="page-content-wrapper">
                    {{-- BEGIN CONTENT BODY --}}
                    {{-- BEGIN PAGE HEAD--}}
                    <div class="page-head">
                        <div class="container">
                            {{-- BEGIN PAGE TITLE --}}
                            <div class="page-title">
                                <h1>User Idle Timeout
                                    <small>user idle timeout control</small>
                                </h1>
                            </div>
                            {{-- END PAGE TITLE --}}
                            {{-- BEGIN PAGE TOOLBAR --}}
                            @include('includes.toolbar')
                            {{-- END PAGE TOOLBAR --}}
                        </div>
                    </div>
                    {{-- END PAGE HEAD--}}
                    {{-- BEGIN PAGE CONTENT BODY --}}
                    <div class="page-content">
                        <div class="container">
                            {{-- BEGIN PAGE BREADCRUMBS --}}
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <a href="{{ url('/') }}">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <a href="#">UI Features</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>User Idle Timeout</span>
                                </li>
                            </ul>
                            {{-- END PAGE BREADCRUMBS --}}
                            {{-- BEGIN PAGE CONTENT INNER --}}
                            <div class="page-content-inner">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="note note-success">
                                            <h4 class="block">User Idle Timeout</h4>

                                            <p> This plugin allows you to detect when a user becomes idle (detection provided
                                                by Paul Irish's idletimer plugin) and notify the user his/her session is
                                                about to expire. Similar to the technique seen on
                                                Mint.com. Polling requests are automatically sent to the server at a
                                                configurable interval, maintaining the users session while s/he is using your
                                                application for long periods of time. </p>

                                            <p> This idle timeout countdown is triggered after 5 seconds. <code>Keep your
                                                    mouse and keyboard still!</code> A polling request is sent to the server
                                                every two seconds to keep the server side session alive.
                                                I've set this parameter to an extremely low number for demo purposes (timeout
                                                is only 5 seconds), but in reality, this should be much higher </p>

                                            <p> To learn more please check out
                                                <a class="btn btn-outline red"
                                                   href="https://github.com/ehynds/jquery-idle-timeout" target="_blank"> the
                                                    plugin's official homepage </a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade" id="idle-timeout-dialog" data-backdrop="static">
                                    <div class="modal-dialog modal-small">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Your session is about to expire.</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>
                                                    <i class="fa fa-warning font-red"></i> You session will be locked in
                                                    <span id="idle-timeout-counter"></span> seconds.</p>

                                                <p> Do you want to continue your session? </p>
                                            </div>
                                            <div class="modal-footer">
                                                <button id="idle-timeout-dialog-logout" type="button"
                                                        class="btn dark btn-outline sbold uppercase">No, Logout
                                                </button>
                                                <button id="idle-timeout-dialog-keepalive" type="button"
                                                        class="btn green btn-outline sbold uppercase" data-dismiss="modal">
                                                    Yes, Keep Working
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- END PAGE CONTENT INNER --}}
                        </div>
                    </div>
                    {{-- END PAGE CONTENT BODY --}}
                    {{-- END CONTENT BODY --}}
                </div>
                {{-- END CONTENT --}}
                {{-- BEGIN QUICK SIDEBAR --}}
                @include('includes.sidebar')
                {{-- END QUICK SIDEBAR --}}
            </div>
            {{-- END CONTAINER --}}
        </div>
    </div>
@endsection