@extends ("layouts.base")

@section('extra-css')
    @parent
    {{-- BEGIN THEME GLOBAL STYLES --}}
    <link href="{{ asset('css/plugins-md.min.css') }}" rel="stylesheet" type="text/css"/>
    {{-- END THEME GLOBAL STYLES --}}
    {{-- BEGIN PAGE LEVEL PLUGINS --}}
    <link href="{{ asset('plugins/fancybox/source/jquery.fancybox.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('plugins/jquery-file-upload/blueimp-gallery/blueimp-gallery.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('plugins/jquery-file-upload/css/jquery.fileupload.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('plugins/jquery-file-upload/css/jquery.fileupload-ui.css') }}" rel="stylesheet" type="text/css" />
    {{-- END PAGE LEVEL PLUGINS --}}
    {{-- BEGIN PAGE LEVEL STYLES --}}
    <link href="{{ asset('css/apps/inbox.min.css') }}" rel="stylesheet" type="text/css"/>
    {{-- END PAGE LEVEL STYLES --}}
@endsection

@push('extra-js')
{{-- BEGIN CORE PLUGINS --}}
<script src="{{ asset('plugins/jquery.blockui.min.js') }}" type="text/javascript"></script>
{{-- END CORE PLUGINS --}}
{{-- BEGIN PAGE LEVEL PLUGINS --}}
<script src="{{ asset('plugins/fancybox/source/jquery.fancybox.pack.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/jquery-file-upload/js/vendor/tmpl.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/jquery-file-upload/js/vendor/load-image.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/jquery-file-upload/js/vendor/canvas-to-blob.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/jquery-file-upload/blueimp-gallery/jquery.blueimp-gallery.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('plugins/jquery-file-upload/js/jquery.iframe-transport.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/jquery-file-upload/js/jquery.fileupload.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/jquery-file-upload/js/jquery.fileupload-process.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/jquery-file-upload/js/jquery.fileupload-image.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/jquery-file-upload/js/jquery.fileupload-audio.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/jquery-file-upload/js/jquery.fileupload-video.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/jquery-file-upload/js/jquery.fileupload-validate.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/jquery-file-upload/js/jquery.fileupload-ui.js') }}" type="text/javascript"></script>
{{-- END PAGE LEVEL PLUGINS --}}
{{-- BEGIN PAGE LEVEL SCRIPTS --}}
{{--<script src="{{ asset('js/apps/inbox.js') }}" type="text/javascript"></script>--}}
<script src="{{ asset('js/apps/inbox.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        var inbox_urls = {
            list_messages : '{{ url('app_inbox_inbox') }}',
            composer_message : '{{ url('app_inbox_compose') }}',
            reply_message : '{{ url('app_inbox_reply') }}',
            view_message : '{{ url('app_inbox_view') }}',
        };
        AppInbox.init(inbox_urls);
    });
</script>
{{-- END PAGE LEVEL SCRIPTS --}}
@endpush

@section('main-content')
    <div class="page-wrapper-row full-height">
        <div class="page-wrapper-middle">
            {{-- BEGIN CONTAINER --}}
            <div class="page-container">
                {{-- BEGIN CONTENT --}}
                <div class="page-content-wrapper">
                    {{-- BEGIN CONTENT BODY --}}
                    {{-- BEGIN PAGE HEAD--}}
                    <div class="page-head">
                        <div class="container">
                            {{-- BEGIN PAGE TITLE --}}
                            <div class="page-title">
                                <h1>Inbox
                                    <small>user inbox</small>
                                </h1>
                            </div>
                            {{-- END PAGE TITLE --}}
                            {{-- BEGIN PAGE TOOLBAR --}}
                            @include('includes.toolbar')
                            {{-- END PAGE TOOLBAR --}}
                        </div>
                    </div>
                    {{-- END PAGE HEAD--}}
                    {{-- BEGIN PAGE CONTENT BODY --}}
                    <div class="page-content">
                        <div class="container">
                            {{-- BEGIN PAGE BREADCRUMBS --}}
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <a href="{{ url('/') }}">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <a href="#">Pages</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Apps</span>
                                </li>
                            </ul>
                            {{-- END PAGE BREADCRUMBS --}}
                            {{-- BEGIN PAGE CONTENT INNER --}}
                            <div class="page-content-inner">
                                <div class="inbox">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="inbox-sidebar">
                                                <a href="javascript:;" data-title="Compose"
                                                   class="btn red compose-btn btn-block">
                                                    <i class="fa fa-edit"></i> Compose </a>
                                                <ul class="inbox-nav">
                                                    <li class="active">
                                                        <a href="javascript:;" data-type="inbox" data-title="Inbox"> Inbox
                                                            <span class="badge badge-success">3</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;" data-type="important" data-title="Inbox">
                                                            Important </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;" data-type="sent" data-title="Sent"> Sent </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;" data-type="draft" data-title="Draft"> Draft
                                                            <span class="badge badge-danger">8</span>
                                                        </a>
                                                    </li>
                                                    <li class="divider"></li>
                                                    <li>
                                                        <a href="javascript:;" class="sbold uppercase" data-title="Trash">
                                                            Trash
                                                            <span class="badge badge-info">23</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;" data-type="inbox" data-title="Promotions">
                                                            Promotions
                                                            <span class="badge badge-warning">2</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;" data-type="inbox" data-title="News"> News </a>
                                                    </li>
                                                </ul>
                                                <ul class="inbox-contacts">
                                                    <li class="divider margin-bottom-30"></li>
                                                    <li>
                                                        <a href="javascript:;">
                                                            <img class="contact-pic"
                                                                 src="{{ asset('pages/media/users/avatar4.jpg') }}">
                                                            <span class="contact-name">Adam Stone</span>
                                                            <span class="contact-status bg-green"></span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;">
                                                            <img class="contact-pic"
                                                                 src="{{ asset('pages/media/users/avatar2.jpg') }}">
                                                            <span class="contact-name">Lisa Wong</span>
                                                            <span class="contact-status bg-red"></span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;">
                                                            <img class="contact-pic"
                                                                 src="{{ asset('pages/media/users/avatar5.jpg') }}">
                                                            <span class="contact-name">Nick Strong</span>
                                                            <span class="contact-status bg-green"></span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;">
                                                            <img class="contact-pic"
                                                                 src="{{ asset('pages/media/users/avatar6.jpg') }}">
                                                            <span class="contact-name">Anna Bold</span>
                                                            <span class="contact-status bg-yellow"></span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;">
                                                            <img class="contact-pic"
                                                                 src="{{ asset('pages/media/users/avatar7.jpg') }}">
                                                            <span class="contact-name">Richard Nilson</span>
                                                            <span class="contact-status bg-green"></span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="inbox-body">
                                                <div class="inbox-header">
                                                    <h1 class="pull-left">Inbox</h1>

                                                    <form class="form-inline pull-right" action="{{ url('/') }}">
                                                        <div class="input-group input-medium">
                                                            <input type="text" class="form-control" placeholder="Password">
                                                                    <span class="input-group-btn">
                                                                        <button type="submit" class="btn green">
                                                                            <i class="fa fa-search"></i>
                                                                        </button>
                                                                    </span>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="inbox-content"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- END PAGE CONTENT INNER --}}
                        </div>
                    </div>
                    {{-- END PAGE CONTENT BODY --}}
                    {{-- END CONTENT BODY --}}
                </div>
                {{-- END CONTENT --}}
                {{-- BEGIN QUICK SIDEBAR --}}
                @include('includes.sidebar')
                {{-- END QUICK SIDEBAR --}}
            </div>
            {{-- END CONTAINER --}}
        </div>
    </div>
@endsection