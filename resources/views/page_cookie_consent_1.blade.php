@extends ("layouts.base")

@section('extra-css')
    @parent
    {{-- BEGIN PAGE LEVEL STYLES --}}
    <link href="{{ asset('css/plugins-md.min.css') }}" rel="stylesheet" type="text/css" />
    {{-- END PAGE LEVEL STYLES --}}
@endsection

@push('extra-js')
        {{-- BEGIN PAGE LEVEL PLUGINS --}}
    <script src="{{ asset('plugins/jquery-cookiebar/jquery.cookieBar.min.js') }}" type="text/javascript"></script>
    {{-- END PAGE LEVEL PLUGINS --}}
    {{-- BEGIN PAGE LEVEL SCRIPTS --}}
    <script src="{{ asset('js/layout/cookie-consent.min.js') }}" type="text/javascript"></script>
    {{-- END PAGE LEVEL SCRIPTS --}}
@endpush

@section('main-content')
    <div class="page-wrapper-row full-height">
        <div class="page-wrapper-middle">
            {{-- BEGIN CONTAINER --}}
            <div class="page-container">
                {{-- BEGIN CONTENT --}}
                <div class="page-content-wrapper">
                    {{-- BEGIN CONTENT BODY --}}
                    {{-- BEGIN PAGE HEAD--}}
                    <div class="page-head">
                        <div class="container">
                            {{-- BEGIN PAGE TITLE --}}
                            <div class="page-title">
                                <h1>Cookie Consent 1
                                    <small>Cookie Consent Demo</small>
                                </h1>
                            </div>
                            {{-- END PAGE TITLE --}}
                            {{-- BEGIN PAGE TOOLBAR --}}
                            @include('includes.toolbar')
                            {{-- END PAGE TOOLBAR --}}
                        </div>
                    </div>
                    {{-- END PAGE HEAD--}}
                    {{-- BEGIN PAGE CONTENT BODY --}}
                    <div class="page-content">
                        <div class="container">
                            {{-- BEGIN PAGE BREADCRUMBS --}}
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <a href="{{ url('/') }}">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                            </ul>
                            {{-- END PAGE BREADCRUMBS --}}
                            {{-- BEGIN PAGE CONTENT INNER --}}
                            <div class="page-content-inner">
                                <div class="note note-success">
                                    <h3>Cookie Consent Bar</h3>
                                    <p> A simple, lightweight jQuery plugin for creating a notification bar that is dismissable and dismiss is saved by cookie. Perfect for implementing the new eu cookielaw. For more info check
                                        <a class="btn red btn-outline"
                                           href="https://github.com/carlwoodhouse/jquery.cookieBar" target="_blank">the official documentation</a>
                                    </p>
                                </div>
                                <div class="mt-cookie-consent-bar">
                                    <div class="container">
                                        <div class="mt-cookie-consent-bar-holder">
                                            <div class="mt-cookie-consent-bar-content"> This website uses cookies to ensure you get the best experience on our website.
                                                <a href="http://www.allaboutcookies.org/cookies/" tatget="_blank">What is cookie ?</a>
                                                <a href="#">Our Cookie Policy</a>
                                            </div>
                                            <div class="mt-cookie-consent-bar-action">
                                                <a href="javascript:;" class="mt-cookie-consent-btn btn btn-circle green">Understand</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- END PAGE CONTENT INNER --}}
                        </div>
                    </div>
                    {{-- END PAGE CONTENT BODY --}}
                    {{-- END CONTENT BODY --}}
                </div>
                {{-- END CONTENT --}}
                {{-- BEGIN QUICK SIDEBAR --}}
                @include('includes.sidebar')
                {{-- END QUICK SIDEBAR --}}
            </div>
            {{-- END CONTAINER --}}
        </div>
    </div>
@endsection