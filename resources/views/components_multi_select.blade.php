@extends ("layouts.base")

@section('extra-css')
    @parent
    {{-- BEGIN PAGE LEVEL PLUGINS --}}
    <link href="{{ asset('plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('plugins/jquery-multi-select/css/multi-select.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    {{-- END PAGE LEVEL PLUGINS --}}
    {{-- BEGIN THEME GLOBAL STYLES --}}
    <link href="{{ asset('css/plugins-md.min.css') }}" rel="stylesheet" type="text/css"/>
    {{-- END THEME GLOBAL STYLES --}}
@endsection

@push('extra-js')
{{-- BEGIN PAGE LEVEL PLUGINS --}}
<script src="{{ asset('plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/jquery-multi-select/js/jquery.multi-select.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
{{-- END PAGE LEVEL PLUGINS --}}
{{-- BEGIN PAGE LEVEL SCRIPTS --}}
<script src="{{ asset('pages/js/components-multi-select.min.js') }}" type="text/javascript"></script>
{{-- END PAGE LEVEL SCRIPTS --}}
@endpush

@section('main-content')
    <div class="page-wrapper-row full-height">
        <div class="page-wrapper-middle">
            {{-- BEGIN CONTAINER --}}
            <div class="page-container">
                {{-- BEGIN CONTENT --}}
                <div class="page-content-wrapper">
                    {{-- BEGIN CONTENT BODY --}}
                    {{-- BEGIN PAGE HEAD--}}
                    <div class="page-head">
                        <div class="container">
                            {{-- BEGIN PAGE TITLE --}}
                            <div class="page-title">
                                <h1>Custom Dropdowns
                                    <small>select2, selectboxit & multi select examples</small>
                                </h1>
                            </div>
                            {{-- END PAGE TITLE --}}
                            {{-- BEGIN PAGE TOOLBAR --}}
                            @include('includes.toolbar')
                            {{-- END PAGE TOOLBAR --}}
                        </div>
                    </div>
                    {{-- END PAGE HEAD--}}
                    {{-- BEGIN PAGE CONTENT BODY --}}
                    <div class="page-content">
                        <div class="container">
                            {{-- BEGIN PAGE BREADCRUMBS --}}
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <a href="{{ url('/') }}">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <a href="#">Components</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Multi Select</span>
                                </li>
                            </ul>
                            {{-- END PAGE BREADCRUMBS --}}
                            {{-- BEGIN PAGE CONTENT INNER --}}
                            <div class="page-content-inner">
                                <div class="m-heading-1 border-green m-bordered">
                                    <h3>Bootstrap Context Menu</h3>

                                    <p> Drop-in replacement for the standard select with multiple attribute activated.
                                        <br> For more info please check out
                                        <a href="http://loudev.com/" target="_blank">the official documentation</a>. </p>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet light form-fit ">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="icon-social-dribbble font-green"></i>
                                                    <span class="caption-subject font-green bold uppercase">Multiple Select</span>
                                                </div>
                                                <div class="actions">
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-cloud-upload"></i>
                                                    </a>
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-wrench"></i>
                                                    </a>
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-trash"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body form">
                                                {{-- BEGIN FORM--}}
                                                <form action="{{ url('/') }}" class="form-horizontal form-row-seperated">
                                                    <div class="form-body">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Default</label>

                                                            <div class="col-md-9">
                                                                <select multiple="multiple" class="multi-select"
                                                                        id="my_multi_select1" name="my_multi_select1[]">
                                                                    <option>Dallas Cowboys</option>
                                                                    <option>New York Giants</option>
                                                                    <option selected>Philadelphia Eagles</option>
                                                                    <option selected>Washington Redskins</option>
                                                                    <option>Chicago Bears</option>
                                                                    <option>Detroit Lions</option>
                                                                    <option>Green Bay Packers</option>
                                                                    <option>Minnesota Vikings</option>
                                                                    <option selected>Atlanta Falcons</option>
                                                                    <option>Carolina Panthers</option>
                                                                    <option>New Orleans Saints</option>
                                                                    <option>Tampa Bay Buccaneers</option>
                                                                    <option>Arizona Cardinals</option>
                                                                    <option>St. Louis Rams</option>
                                                                    <option>San Francisco 49ers</option>
                                                                    <option>Seattle Seahawks</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group last">
                                                            <label class="control-label col-md-3">Grouped Options</label>

                                                            <div class="col-md-9">
                                                                <select multiple="multiple" class="multi-select"
                                                                        id="my_multi_select2" name="my_multi_select2[]">
                                                                    <optgroup label="NFC EAST">
                                                                        <option>Dallas Cowboys</option>
                                                                        <option>New York Giants</option>
                                                                        <option>Philadelphia Eagles</option>
                                                                        <option>Washington Redskins</option>
                                                                    </optgroup>
                                                                    <optgroup label="NFC NORTH">
                                                                        <option>Chicago Bears</option>
                                                                        <option>Detroit Lions</option>
                                                                        <option>Green Bay Packers</option>
                                                                        <option>Minnesota Vikings</option>
                                                                    </optgroup>
                                                                    <optgroup label="NFC SOUTH">
                                                                        <option>Atlanta Falcons</option>
                                                                        <option>Carolina Panthers</option>
                                                                        <option>New Orleans Saints</option>
                                                                        <option>Tampa Bay Buccaneers</option>
                                                                    </optgroup>
                                                                    <optgroup label="NFC WEST">
                                                                        <option>Arizona Cardinals</option>
                                                                        <option>St. Louis Rams</option>
                                                                        <option>San Francisco 49ers</option>
                                                                        <option>Seattle Seahawks</option>
                                                                    </optgroup>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-offset-3 col-md-9">
                                                                <button type="submit" class="btn green">
                                                                    <i class="fa fa-check"></i> Submit
                                                                </button>
                                                                <button type="button" class="btn grey-salsa btn-outline">
                                                                    Cancel
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                                {{-- END FORM--}}
                                            </div>
                                        </div>
                                        {{-- END PORTLET--}}
                                    </div>
                                </div>
                            </div>
                            {{-- END PAGE CONTENT INNER --}}
                        </div>
                    </div>
                    {{-- END PAGE CONTENT BODY --}}
                    {{-- END CONTENT BODY --}}
                </div>
                {{-- END CONTENT --}}
                {{-- BEGIN QUICK SIDEBAR --}}
                @include('includes.sidebar')
                {{-- END QUICK SIDEBAR --}}
            </div>
            {{-- END CONTAINER --}}
        </div>
    </div>
@endsection