@extends ("layouts.base")

@section('extra-css')
    @parent
        {{-- BEGIN THEME GLOBAL STYLES --}}
<link href="{{ asset('css/plugins-md.min.css') }}" rel="stylesheet" type="text/css" />
{{-- END THEME GLOBAL STYLES --}}
        {{-- BEGIN PAGE LEVEL PLUGINS --}}
<link href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('plugins/fancybox/source/jquery.fancybox.css') }}" rel="stylesheet" type="text/css" />
{{-- END PAGE LEVEL PLUGINS --}}
{{-- BEGIN PAGE LEVEL STYLES --}}
<link href="{{ asset('pages/css/search.min.css') }}" rel="stylesheet" type="text/css" />
{{-- END PAGE LEVEL STYLES --}}
@endsection

@push('extra-js')
        {{-- BEGIN PAGE LEVEL PLUGINS --}}
<script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/fancybox/source/jquery.fancybox.pack.js') }}" type="text/javascript"></script>
{{-- END PAGE LEVEL PLUGINS --}}
{{-- BEGIN PAGE LEVEL SCRIPTS --}}
<script src="{{ asset('pages/js/search.min.js') }}" type="text/javascript"></script>
{{-- END PAGE LEVEL SCRIPTS --}}
@endpush

@section('main-content')
    <div class="page-wrapper-row full-height">
        <div class="page-wrapper-middle">
            {{-- BEGIN CONTAINER --}}
            <div class="page-container">
                {{-- BEGIN CONTENT --}}
                <div class="page-content-wrapper">
                    {{-- BEGIN CONTENT BODY --}}
                    {{-- BEGIN PAGE HEAD--}}
                    <div class="page-head">
                        <div class="container">
                            {{-- BEGIN PAGE TITLE --}}
                            <div class="page-title">
                                <h1>Search Results 4
                                    <small>search results</small>
                                </h1>
                            </div>
                            {{-- END PAGE TITLE --}}
                            {{-- BEGIN PAGE TOOLBAR --}}
                            @include('includes.toolbar')
                            {{-- END PAGE TOOLBAR --}}
                        </div>
                    </div>
                    {{-- END PAGE HEAD--}}
                    {{-- BEGIN PAGE CONTENT BODY --}}
                    <div class="page-content">
                        <div class="container">
                            {{-- BEGIN PAGE BREADCRUMBS --}}
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <a href="{{ url('/') }}">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <a href="#">Pages</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <a href="#">General</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Search</span>
                                </li>
                            </ul>
                            {{-- END PAGE BREADCRUMBS --}}
                            {{-- BEGIN PAGE CONTENT INNER --}}
                            <div class="page-content-inner">
                                <div class="search-page search-content-3">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="search-filter ">
                                                <div class="search-label uppercase">Search By</div>
                                                <div class="input-icon right">
                                                    <i class="icon-magnifier"></i>
                                                    <input type="text" class="form-control" placeholder="Filter by keywords"> </div>
                                                <div class="search-label uppercase">Sort By</div>
                                                <select class="form-control">
                                                    <option>Option 1</option>
                                                    <option>Option 2</option>
                                                    <option>Option 3</option>
                                                    <option>Option 4</option>
                                                    <option>Option 5</option>
                                                </select>
                                                <div class="search-label uppercase">Date</div>
                                                <div class="input-icon right">
                                                    <i class="icon-calendar"></i>
                                                    <input class="form-control date-picker" type="text" placeholder="Any Date" /> </div>
                                                <button class="btn green bold uppercase btn-block">Update Search Results</button>
                                                <div class="search-filter-divider bg-grey-steel"></div>
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <button class="btn grey bold uppercase btn-block">Reset Search</button>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <button class="btn grey-cararra font-blue bold btn-block">Advanced Search</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="tile-container">
                                                        <div class="tile-thumbnail">
                                                            <a href="javascript:;">
                                                                <img src="{{ asset('pages/img/page_general_search/01.jpg') }}" />
                                                            </a>
                                                        </div>
                                                        <div class="tile-title">
                                                            <h3>
                                                                <a href="javascript:;">Mobile App</a>
                                                            </h3>
                                                            <a href="javascript:;">
                                                                <i class="icon-question font-blue"></i>
                                                            </a>
                                                            <a href="javascript:;">
                                                                <i class="icon-plus font-green-meadow"></i>
                                                            </a>
                                                            <div class="tile-desc">
                                                                <p>Activity:
                                                                    <a href="javascript:;">Bob Robson</a> -
                                                                    <span class="font-grey-salt">25 mins ago</span>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="tile-container">
                                                        <div class="tile-thumbnail">
                                                            <a href="javascript:;">
                                                                <img src="{{ asset('pages/img/page_general_search/1.jpg') }}" />
                                                            </a>
                                                        </div>
                                                        <div class="tile-title">
                                                            <h3>
                                                                <a href="javascript:;">Mobile App</a>
                                                            </h3>
                                                            <a href="javascript:;">
                                                                <i class="icon-question font-blue"></i>
                                                            </a>
                                                            <a href="javascript:;">
                                                                <i class="icon-plus font-green-meadow"></i>
                                                            </a>
                                                            <div class="tile-desc">
                                                                <p>Activity:
                                                                    <a href="javascript:;">Bob Robson</a> -
                                                                    <span class="font-grey-salt">25 mins ago</span>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="tile-container">
                                                        <div class="tile-thumbnail">
                                                            <a href="javascript:;">
                                                                <img src="{{ asset('pages/img/page_general_search/02.jpg') }}" />
                                                            </a>
                                                        </div>
                                                        <div class="tile-title">
                                                            <h3>
                                                                <a href="javascript:;">Mobile App</a>
                                                            </h3>
                                                            <a href="javascript:;">
                                                                <i class="icon-question font-blue"></i>
                                                            </a>
                                                            <a href="javascript:;">
                                                                <i class="icon-plus font-green-meadow"></i>
                                                            </a>
                                                            <div class="tile-desc">
                                                                <p>Activity:
                                                                    <a href="javascript:;">Bob Robson</a> -
                                                                    <span class="font-grey-salt">25 mins ago</span>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="tile-container">
                                                        <div class="tile-thumbnail">
                                                            <a href="javascript:;">
                                                                <img src="{{ asset('pages/img/page_general_search/2.jpg') }}" />
                                                            </a>
                                                        </div>
                                                        <div class="tile-title">
                                                            <h3>
                                                                <a href="javascript:;">Mobile App</a>
                                                            </h3>
                                                            <a href="javascript:;">
                                                                <i class="icon-question font-blue"></i>
                                                            </a>
                                                            <a href="javascript:;">
                                                                <i class="icon-plus font-green-meadow"></i>
                                                            </a>
                                                            <div class="tile-desc">
                                                                <p>Activity:
                                                                    <a href="javascript:;">Bob Robson</a> -
                                                                    <span class="font-grey-salt">25 mins ago</span>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="tile-container">
                                                        <div class="tile-thumbnail">
                                                            <a href="javascript:;">
                                                                <img src="{{ asset('pages/img/page_general_search/03.jpg') }}" />
                                                            </a>
                                                        </div>
                                                        <div class="tile-title">
                                                            <h3>
                                                                <a href="javascript:;">Mobile App</a>
                                                            </h3>
                                                            <a href="javascript:;">
                                                                <i class="icon-question font-blue"></i>
                                                            </a>
                                                            <a href="javascript:;">
                                                                <i class="icon-plus font-green-meadow"></i>
                                                            </a>
                                                            <div class="tile-desc">
                                                                <p>Activity:
                                                                    <a href="javascript:;">Bob Robson</a> -
                                                                    <span class="font-grey-salt">25 mins ago</span>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="tile-container">
                                                        <div class="tile-thumbnail">
                                                            <a href="javascript:;">
                                                                <img src="{{ asset('pages/img/page_general_search/3.jpg') }}" />
                                                            </a>
                                                        </div>
                                                        <div class="tile-title">
                                                            <h3>
                                                                <a href="javascript:;">Mobile App</a>
                                                            </h3>
                                                            <a href="javascript:;">
                                                                <i class="icon-question font-blue"></i>
                                                            </a>
                                                            <a href="javascript:;">
                                                                <i class="icon-plus font-green-meadow"></i>
                                                            </a>
                                                            <div class="tile-desc">
                                                                <p>Activity:
                                                                    <a href="javascript:;">Bob Robson</a> -
                                                                    <span class="font-grey-salt">25 mins ago</span>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="tile-container">
                                                        <div class="tile-thumbnail">
                                                            <a href="javascript:;">
                                                                <img src="{{ asset('pages/img/page_general_search/04.jpg') }}" />
                                                            </a>
                                                        </div>
                                                        <div class="tile-title">
                                                            <h3>
                                                                <a href="javascript:;">Mobile App</a>
                                                            </h3>
                                                            <a href="javascript:;">
                                                                <i class="icon-question font-blue"></i>
                                                            </a>
                                                            <a href="javascript:;">
                                                                <i class="icon-plus font-green-meadow"></i>
                                                            </a>
                                                            <div class="tile-desc">
                                                                <p>Activity:
                                                                    <a href="javascript:;">Bob Robson</a> -
                                                                    <span class="font-grey-salt">25 mins ago</span>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="tile-container">
                                                        <div class="tile-thumbnail">
                                                            <a href="javascript:;">
                                                                <img src="{{ asset('pages/img/page_general_search/4.jpg') }}" />
                                                            </a>
                                                        </div>
                                                        <div class="tile-title">
                                                            <h3>
                                                                <a href="javascript:;">Mobile App</a>
                                                            </h3>
                                                            <a href="javascript:;">
                                                                <i class="icon-question font-blue"></i>
                                                            </a>
                                                            <a href="javascript:;">
                                                                <i class="icon-plus font-green-meadow"></i>
                                                            </a>
                                                            <div class="tile-desc">
                                                                <p>Activity:
                                                                    <a href="javascript:;">Bob Robson</a> -
                                                                    <span class="font-grey-salt">25 mins ago</span>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="tile-container">
                                                        <div class="tile-thumbnail">
                                                            <a href="javascript:;">
                                                                <img src="{{ asset('pages/img/page_general_search/05.jpg') }}" />
                                                            </a>
                                                        </div>
                                                        <div class="tile-title">
                                                            <h3>
                                                                <a href="javascript:;">Mobile App</a>
                                                            </h3>
                                                            <a href="javascript:;">
                                                                <i class="icon-question font-blue"></i>
                                                            </a>
                                                            <a href="javascript:;">
                                                                <i class="icon-plus font-green-meadow"></i>
                                                            </a>
                                                            <div class="tile-desc">
                                                                <p>Activity:
                                                                    <a href="javascript:;">Bob Robson</a> -
                                                                    <span class="font-grey-salt">25 mins ago</span>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="search-pagination pagination-rounded">
                                                <ul class="pagination">
                                                    <li class="page-active">
                                                        <a href="javascript:;"> 1 </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;"> 2 </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;"> 3 </a>
                                                    </li>
                                                    <li>
                                                        <a href="javascript:;"> 4 </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- END PAGE CONTENT INNER --}}
                        </div>
                    </div>
                    {{-- END PAGE CONTENT BODY --}}
                    {{-- END CONTENT BODY --}}
                </div>
                {{-- END CONTENT --}}
                {{-- BEGIN QUICK SIDEBAR --}}
                @include('includes.sidebar')
                {{-- END QUICK SIDEBAR --}}
            </div>
            {{-- END CONTAINER --}}
        </div>
    </div>
@endsection