@extends ("layouts.base")

@section('extra-css')
    @parent
    {{-- BEGIN PAGE LEVEL STYLES --}}
    <link href="{{ asset('css/plugins-md.min.css') }}" rel="stylesheet" type="text/css"/>
    {{-- END PAGE LEVEL STYLES --}}
@endsection

@push('extra-js')
{{-- BEGIN PAGE LEVEL SCRIPTS --}}
<script src="http://maps.google.com/maps/api/js?key={{ config('app.google_maps_key') }}&sensor=false"></script>
<script src="{{ asset('plugins/gmaps/gmaps.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('pages/js/maps-google.min.js') }}" type="text/javascript"></script>
{{-- END PAGE LEVEL SCRIPTS --}}
@endpush

@section('main-content')
    <div class="page-wrapper-row full-height">
        <div class="page-wrapper-middle">
            {{-- BEGIN CONTAINER --}}
            <div class="page-container">
                {{-- BEGIN CONTENT --}}
                <div class="page-content-wrapper">
                    {{-- BEGIN CONTENT BODY --}}
                    {{-- BEGIN PAGE HEAD--}}
                    <div class="page-head">
                        <div class="container">
                            {{-- BEGIN PAGE TITLE --}}
                            <div class="page-title">
                                <h1>Google Maps
                                    <small>interactive google map samples</small>
                                </h1>
                            </div>
                            {{-- END PAGE TITLE --}}
                            {{-- BEGIN PAGE TOOLBAR --}}
                            @include('includes.toolbar')
                            {{-- END PAGE TOOLBAR --}}
                        </div>
                    </div>
                    {{-- END PAGE HEAD--}}
                    {{-- BEGIN PAGE CONTENT BODY --}}
                    <div class="page-content">
                        <div class="container">
                            {{-- BEGIN PAGE BREADCRUMBS --}}
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <a href="{{ url('/') }}">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <a href="#">UI Features</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Google Maps</span>
                                </li>
                            </ul>
                            {{-- END PAGE BREADCRUMBS --}}
                            {{-- BEGIN PAGE CONTENT INNER --}}
                            <div class="page-content-inner">
                                <div class="m-heading-1 border-green m-bordered">
                                    <p> gmaps.js allows you to use the potential of Google Maps in a simple way. No more
                                        extensive documentation or large amount of code. </p>

                                    <p> For more info please check out
                                        <a href="https://hpneo.github.io/gmaps/" class="btn red btn-outline" target="_blank">the
                                            official documentation</a>. </p>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        {{-- BEGIN BASIC PORTLET--}}
                                        <div class="portlet light portlet-fit ">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class=" icon-layers font-red"></i>
                                                    <span class="caption-subject font-red bold uppercase">Basic</span>
                                                </div>
                                                <div class="actions">
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-cloud-upload"></i>
                                                    </a>
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-wrench"></i>
                                                    </a>
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-trash"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div id="gmap_basic" class="gmaps"></div>
                                            </div>
                                        </div>
                                        {{-- END BASIC PORTLET--}}
                                    </div>
                                    <div class="col-md-6">
                                        {{-- BEGIN MARKERS PORTLET--}}
                                        <div class="portlet light portlet-fit ">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class=" icon-layers font-blue"></i>
                                                    <span class="caption-subject font-blue bold uppercase">Markers</span>
                                                </div>
                                                <div class="actions">
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-cloud-upload"></i>
                                                    </a>
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-wrench"></i>
                                                    </a>
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-trash"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div id="gmap_marker" class="gmaps"></div>
                                            </div>
                                        </div>
                                        {{-- END MARKERS PORTLET--}}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        {{-- BEGIN GEOLOCATION PORTLET--}}
                                        <div class="portlet light portlet-fit ">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class=" icon-layers font-green"></i>
                                                    <span class="caption-subject font-green bold uppercase">Geolocation</span>
                                                </div>
                                                <div class="actions">
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-cloud-upload"></i>
                                                    </a>
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-wrench"></i>
                                                    </a>
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-trash"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="label label-danger visible-ie8"> Not supported in Internet
                                                    Explorer 8
                                                </div>
                                                <div id="gmap_geo" class="gmaps"></div>
                                            </div>
                                        </div>
                                        {{-- END GEOLOCATION PORTLET--}}
                                    </div>
                                    <div class="col-md-6">
                                        {{-- BEGIN POLYLINES PORTLET--}}
                                        <div class="portlet light portlet-fit ">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class=" icon-layers font-red"></i>
                                                    <span class="caption-subject font-red bold uppercase">Polylines</span>
                                                </div>
                                                <div class="actions">
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-cloud-upload"></i>
                                                    </a>
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-wrench"></i>
                                                    </a>
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-trash"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div id="gmap_polylines" class="gmaps"></div>
                                            </div>
                                        </div>
                                        {{-- END POLYLINES PORTLET--}}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        {{-- BEGIN POLYGONS PORTLET--}}
                                        <div class="portlet light portlet-fit ">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class=" icon-layers font-dark"></i>
                                                    <span class="caption-subject font-dark bold uppercase">Polygons</span>
                                                </div>
                                                <div class="actions">
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-cloud-upload"></i>
                                                    </a>
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-wrench"></i>
                                                    </a>
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-trash"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="label label-danger visible-ie8"> Not supported in Internet
                                                    Explorer 8
                                                </div>
                                                <div id="gmap_polygons" class="gmaps"></div>
                                            </div>
                                        </div>
                                        {{-- END POLYGONS PORTLET--}}
                                    </div>
                                    <div class="col-md-6">
                                        {{-- BEGIN STATIC PORTLET--}}
                                        <div class="portlet light portlet-fit ">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class=" icon-layers font-yellow"></i>
                                                    <span class="caption-subject font-yellow bold uppercase">Static</span>
                                                </div>
                                                <div class="actions">
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-cloud-upload"></i>
                                                    </a>
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-wrench"></i>
                                                    </a>
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-trash"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div id="gmap_static" class="gmaps">
                                                    <div style="height:100%;overflow:hidden;display:block;background: url(http://maps.googleapis.com/maps/api/staticmap?center=48.858271,2.294264&amp;size=640x300&amp;sensor=true&amp;zoom=5) no-repeat 50% 50%;">
                                                        <img src="http://maps.googleapis.com/maps/api/staticmap?center=48.858271,2.294264&amp;size=640x300&amp;sensor=true&amp;zoom=16"
                                                             style="visibility:hidden" alt=""/></div>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- END STATIC PORTLET--}}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        {{-- BEGIN ROUTES PORTLET--}}
                                        <div class="portlet light portlet-fit ">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class=" icon-layers font-blue"></i>
                                                    <span class="caption-subject font-blue bold uppercase">Routes</span>
                                                </div>
                                                <div class="actions">
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-cloud-upload"></i>
                                                    </a>
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-wrench"></i>
                                                    </a>
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-trash"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <form class="form-inline margin-bottom-10" action="#">
                                                    <input type="button" id="gmap_routes_start" class="btn green"
                                                           value="Start Routing"/></form>
                                                <div class="label label-danger visible-ie8"> Not supported in Internet
                                                    Explorer 8
                                                </div>
                                                <div id="gmap_routes" class="gmaps"></div>
                                                <ol id="gmap_routes_instructions"></ol>
                                            </div>
                                        </div>
                                        {{-- END ROUTES PORTLET--}}
                                    </div>
                                    <div class="col-md-6">
                                        {{-- BEGIN GEOCODING PORTLET--}}
                                        <div class="portlet light portlet-fit ">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class=" icon-layers font-green"></i>
                                                    <span class="caption-subject font-green bold uppercase">Geocoding</span>
                                                </div>
                                                <div class="actions">
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-cloud-upload"></i>
                                                    </a>
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-wrench"></i>
                                                    </a>
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-trash"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <form class="form-inline margin-bottom-10" action="#">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" id="gmap_geocoding_address"
                                                               placeholder="address...">
                                                                <span class="input-group-btn">
                                                                    <button class="btn blue"
                                                                            id="gmap_geocoding_btn"></button>
                                                                        <i class="fa fa-search"></i>
                                                                </span>
                                                    </div>
                                                </form>
                                                <div id="gmap_geocoding" class="gmaps"></div>
                                            </div>
                                        </div>
                                        {{-- END GEOCODING PORTLET--}}
                                    </div>
                                </div>
                            </div>
                            {{-- END PAGE CONTENT INNER --}}
                        </div>
                    </div>
                    {{-- END PAGE CONTENT BODY --}}
                    {{-- END CONTENT BODY --}}
                </div>
                {{-- END CONTENT --}}
                {{-- BEGIN QUICK SIDEBAR --}}
                @include('includes.sidebar')
                {{-- END QUICK SIDEBAR --}}
            </div>
            {{-- END CONTAINER --}}
        </div>
    </div>
@endsection