@extends ("layouts.base")

@section('extra-css')
    @parent
        {{-- BEGIN THEME GLOBAL STYLES --}}
<link href="{{ asset('css/plugins-md.min.css') }}" rel="stylesheet" type="text/css" />
{{-- END THEME GLOBAL STYLES --}}
        {{-- BEGIN PAGE LEVEL PLUGINS --}}
<link href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('plugins/fancybox/source/jquery.fancybox.css') }}" rel="stylesheet" type="text/css" />
{{-- END PAGE LEVEL PLUGINS --}}
{{-- BEGIN PAGE LEVEL STYLES --}}
<link href="{{ asset('pages/css/search.min.css') }}" rel="stylesheet" type="text/css" />
{{-- END PAGE LEVEL STYLES --}}
@endsection

@push('extra-js')
        {{-- BEGIN PAGE LEVEL PLUGINS --}}
<script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/fancybox/source/jquery.fancybox.pack.js') }}" type="text/javascript"></script>
{{-- END PAGE LEVEL PLUGINS --}}
{{-- BEGIN PAGE LEVEL SCRIPTS --}}
<script src="{{ asset('pages/js/search.min.js') }}" type="text/javascript"></script>
{{-- END PAGE LEVEL SCRIPTS --}}
@endpush

@section('main-content')
    <div class="page-wrapper-row full-height">
        <div class="page-wrapper-middle">
            {{-- BEGIN CONTAINER --}}
            <div class="page-container">
                {{-- BEGIN CONTENT --}}
                <div class="page-content-wrapper">
                    {{-- BEGIN CONTENT BODY --}}
                    {{-- BEGIN PAGE HEAD--}}
                    <div class="page-head">
                        <div class="container">
                            {{-- BEGIN PAGE TITLE --}}
                            <div class="page-title">
                                <h1>Search Results 3
                                    <small>search results</small>
                                </h1>
                            </div>
                            {{-- END PAGE TITLE --}}
                            {{-- BEGIN PAGE TOOLBAR --}}
                            @include('includes.toolbar')
                            {{-- END PAGE TOOLBAR --}}
                        </div>
                    </div>
                    {{-- END PAGE HEAD--}}
                    {{-- BEGIN PAGE CONTENT BODY --}}
                    <div class="page-content">
                        <div class="container">
                            {{-- BEGIN PAGE BREADCRUMBS --}}
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <a href="{{ url('/') }}">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <a href="#">Pages</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <a href="#">General</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Search</span>
                                </li>
                            </ul>
                            {{-- END PAGE BREADCRUMBS --}}
                            {{-- BEGIN PAGE CONTENT INNER --}}
                            <div class="page-content-inner">
                                <div class="search-page search-content-2">
                                    <div class="search-bar ">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" placeholder="Search for...">
                                                            <span class="input-group-btn">
                                                                <button class="btn blue uppercase bold" type="button">Search</button>
                                                            </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-7">
                                            <div class="search-container ">
                                                <ul class="search-container">
                                                    <li class="search-item clearfix">
                                                        <div class="search-content text-left">
                                                            <h2 class="search-title">
                                                                <a href="javascript:;">Metronic Search Results</a>
                                                            </h2>
                                                            <p class="search-desc"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec efficitur pellentesque auctor. Morbi lobortis, leo in tristique scelerisque, mauris quam volutpat nunc </p>
                                                        </div>
                                                    </li>
                                                    <li class="search-item clearfix">
                                                        <div class="search-content text-left">
                                                            <h2 class="search-title">
                                                                <a href="javascript:;">Lorem ipsum dolor</a>
                                                            </h2>
                                                            <p class="search-desc"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec efficitur pellentesque auctor. Morbi lobortis, leo in tristique scelerisque, mauris quam volutpat nunc </p>
                                                        </div>
                                                    </li>
                                                    <li class="search-item clearfix">
                                                        <div class="search-content text-left">
                                                            <h2 class="search-title">
                                                                <a href="javascript:;">sit amet</a>
                                                            </h2>
                                                            <p class="search-desc"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec efficitur pellentesque auctor. Morbi lobortis, leo in tristique scelerisque, mauris quam volutpat nunc </p>
                                                        </div>
                                                    </li>
                                                    <li class="search-item clearfix">
                                                        <div class="search-content text-left">
                                                            <h2 class="search-title">
                                                                <a href="javascript:;">consectetur adipiscing</a>
                                                            </h2>
                                                            <p class="search-desc"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec efficitur pellentesque auctor. Morbi lobortis, leo in tristique scelerisque, mauris quam volutpat nunc </p>
                                                        </div>
                                                    </li>
                                                    <li class="search-item clearfix">
                                                        <div class="search-content text-left">
                                                            <h2 class="search-title">
                                                                <a href="javascript:;">Metronic Search Reborn</a>
                                                            </h2>
                                                            <p class="search-desc"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec efficitur pellentesque auctor. Morbi lobortis, leo in tristique scelerisque, mauris quam volutpat nunc </p>
                                                        </div>
                                                    </li>
                                                    <li class="search-item clearfix">
                                                        <div class="search-content text-left">
                                                            <h2 class="search-title">
                                                                <a href="javascript:;">tristique scelerisque</a>
                                                            </h2>
                                                            <p class="search-desc"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec efficitur pellentesque auctor. Morbi lobortis, leo in tristique scelerisque, mauris quam volutpat nunc </p>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <div class="search-pagination">
                                                    <ul class="pagination">
                                                        <li class="page-active">
                                                            <a href="javascript:;"> 1 </a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:;"> 2 </a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:;"> 3 </a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:;"> 4 </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            {{-- BEGIN PORTLET--}}
                                            <div class="portlet light ">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="icon-edit font-dark"></i>
                                                        <span class="caption-subject font-dark bold uppercase">Notes</span>
                                                    </div>
                                                    <div class="actions">
                                                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                            <i class="icon-cloud-upload"></i>
                                                        </a>
                                                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                            <i class="icon-wrench"></i>
                                                        </a>
                                                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;"> </a>
                                                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                            <i class="icon-trash"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="portlet-body">
                                                    <div class="note note-success">
                                                        <h4 class="block">Success! Some Header Goes Here</h4>
                                                        <p> Duis mollis, est non commodo luctus, nisi erat mattis consectetur purus sit amet porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum. </p>
                                                    </div>
                                                    <div class="note note-info">
                                                        <h4 class="block">Info! Some Header Goes Here</h4>
                                                        <p> Duis mollis, est non commodo luctus, nisi erat porttitor ligula, mattis consectetur purus sit amet eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum. </p>
                                                    </div>
                                                    <div class="note note-danger">
                                                        <h4 class="block">Danger! Some Header Goes Here</h4>
                                                        <p> Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit mattis consectetur purus sit amet.\ Cras mattis consectetur purus sit amet fermentum. </p>
                                                    </div>
                                                    <div class="note note-warning">
                                                        <h4 class="block">Warning! Some Header Goes Here</h4>
                                                        <p> Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit mattis consectetur purus sit amet. Cras mattis consectetur purus sit amet fermentum. </p>
                                                    </div>
                                                </div>
                                            </div>
                                            {{-- END PORTLET--}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- END PAGE CONTENT INNER --}}
                        </div>
                    </div>
                    {{-- END PAGE CONTENT BODY --}}
                    {{-- END CONTENT BODY --}}
                </div>
                {{-- END CONTENT --}}
                {{-- BEGIN QUICK SIDEBAR --}}
                @include('includes.sidebar')
                {{-- END QUICK SIDEBAR --}}
            </div>
            {{-- END CONTAINER --}}
        </div>
    </div>
@endsection