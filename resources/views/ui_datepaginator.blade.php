@extends ("layouts.base")

@section('extra-css')
    @parent
    {{-- BEGIN PAGE LEVEL STYLES --}}
    <link href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('plugins/bootstrap-datepaginator/bootstrap-datepaginator.min.css') }}" rel="stylesheet"
          type="text/css"/>
    {{-- END PAGE LEVEL STYLES --}}
    {{-- BEGIN THEME GLOBAL STYLES --}}
    <link href="{{ asset('css/plugins-md.min.css') }}" rel="stylesheet" type="text/css"/>
    {{-- END THEME GLOBAL STYLES --}}
@endsection

@push('extra-js')
{{-- BEGIN PAGE LEVEL PLUGINS --}}
<script src="{{ asset('plugins/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/bootstrap-datepaginator/bootstrap-datepaginator.min.js') }}" type="text/javascript"></script>
{{-- END PAGE LEVEL PLUGINS --}}
{{-- BEGIN PAGE LEVEL SCRIPTS --}}
<script src="{{ asset('pages/js/ui-datepaginator.min.js') }}" type="text/javascript"></script>
{{-- END PAGE LEVEL SCRIPTS --}}
@endpush

@section('main-content')
    <div class="page-wrapper-row full-height">
        <div class="page-wrapper-middle">
            {{-- BEGIN CONTAINER --}}
            <div class="page-container">
                {{-- BEGIN CONTENT --}}
                <div class="page-content-wrapper">
                    {{-- BEGIN CONTENT BODY --}}
                    {{-- BEGIN PAGE HEAD--}}
                    <div class="page-head">
                        <div class="container">
                            {{-- BEGIN PAGE TITLE --}}
                            <div class="page-title">
                                <h1>Date Paginator
                                    <small>scrollable & selectable date paginator</small>
                                </h1>
                            </div>
                            {{-- END PAGE TITLE --}}
                            {{-- BEGIN PAGE TOOLBAR --}}
                            @include('includes.toolbar')
                            {{-- END PAGE TOOLBAR --}}
                        </div>
                    </div>
                    {{-- END PAGE HEAD--}}
                    {{-- BEGIN PAGE CONTENT BODY --}}
                    <div class="page-content">
                        <div class="container">
                            {{-- BEGIN PAGE BREADCRUMBS --}}
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <a href="{{ url('/') }}">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <a href="#">UI Features</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Date Paginator</span>
                                </li>
                            </ul>
                            {{-- END PAGE BREADCRUMBS --}}
                            {{-- BEGIN PAGE CONTENT INNER --}}
                            <div class="page-content-inner">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="note note-success">
                                            <h4 class="block">Bootstrap Date Paginator</h4>

                                            <p> A jQuery plugin which takes Twitter Bootstrap's already great pagination
                                                component and injects a bit of date based magic. In the process creating a
                                                hugely simplified and modularised way of paging date
                                                based results in your application. For more info please check out
                                                <a class="btn btn-outline red"
                                                   href="http://jonathandanielmiles.com/bootstrap-datepaginator"
                                                   target="_blank"> the official documentation </a>. </p>
                                        </div>
                                        {{-- BEGIN PORTLET--}}
                                        <div class="portlet light ">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="icon-bubble font-red"></i>
                                                    <span class="caption-subject font-red sbold uppercase">Bootstrap Date Paginator Demo</span>
                                                </div>
                                                <div class="actions">
                                                    <div class="btn-group">
                                                        <a class="btn red btn-outline btn-circle btn-sm" href="javascript:;"
                                                           data-toggle="dropdown" data-hover="dropdown"
                                                           data-close-others="true"> Actions
                                                            <i class="fa fa-angle-down"></i>
                                                        </a>
                                                        <ul class="dropdown-menu pull-right">
                                                            <li>
                                                                <a href="javascript:;"> Option 1</a>
                                                            </li>
                                                            <li class="divider"></li>
                                                            <li>
                                                                <a href="javascript:;">Option 2</a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;">Option 3</a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;">Option 4</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <h3>Default Date Paginator</h3>

                                                <div id="datepaginator_sample_1"></div>
                                                <h3>On Date Selected Event Hanlder(try to select a date)</h3>

                                                <div id="datepaginator_sample_4"></div>
                                                <h3>Large Date Paginator</h3>

                                                <div id="datepaginator_sample_2"></div>
                                                <h3>Small Date Paginator</h3>

                                                <div id="datepaginator_sample_3"></div>
                                            </div>
                                        </div>
                                        {{-- END PORTLET--}}
                                    </div>
                                </div>
                            </div>
                            {{-- END PAGE CONTENT INNER --}}
                        </div>
                    </div>
                    {{-- END PAGE CONTENT BODY --}}
                    {{-- END CONTENT BODY --}}
                </div>
                {{-- END CONTENT --}}
                {{-- BEGIN QUICK SIDEBAR --}}
                @include('includes.sidebar')
                {{-- END QUICK SIDEBAR --}}
            </div>
            {{-- END CONTAINER --}}
        </div>
    </div>
@endsection