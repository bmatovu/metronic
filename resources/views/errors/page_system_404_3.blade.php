@extends ("layouts.base-min")

@section('extra-css')
    @parent
    {{-- BEGIN PAGE LEVEL STYLES --}}
    <link href="{{ asset('pages/css/error.min.css') }}" rel="stylesheet" type="text/css"/>
    {{-- END PAGE LEVEL STYLES --}}
@endsection

@push('extra-js')
{{-- BEGIN PAGE LEVEL SCRIPTS --}}

{{-- END PAGE LEVEL SCRIPTS --}}
@endpush

@section('main-content')
    <div class="page-inner">
        <img src="{{ asset('pages/media/pages/earth.jpg') }}" class="img-responsive" alt=""></div>
    <div class="container error-404">
        <h1>404</h1>

        <h2>Houston, we have a problem.</h2>

        <p> Actually, the page you are looking for does not exist. </p>

        <p>
            <a href="{{ url('/') }}" class="btn red btn-outline"> Return home </a>
            <br></p>
    </div>
@endsection