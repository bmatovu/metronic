@extends ("layouts.base-min")

@section('extra-css')
    @parent
    {{-- BEGIN PAGE LEVEL STYLES --}}
    <link href="{{ asset('pages/css/error.min.css') }}" rel="stylesheet" type="text/css" />
    {{-- END PAGE LEVEL STYLES --}}
@endsection

@push('extra-js')
{{-- BEGIN PAGE LEVEL SCRIPTS --}}

{{-- END PAGE LEVEL SCRIPTS --}}
@endpush

@section('main-content')
    <div class="row">
        <div class="col-md-12 page-404">
            <div class="number font-red"> 404 </div>
            <div class="details">
                <h3>Oops! You're lost.</h3>
                <p> We can not find the page you're looking for.
                    <br/>
                    <a href="{{ url('/') }}"> Return home </a> or try the search bar below. </p>
                <form action="#">
                    <div class="input-group input-medium">
                        <input type="text" class="form-control" placeholder="keyword...">
                            <span class="input-group-btn">
                                <button type="submit" class="btn red">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                    </div>
                    <!-- /input-group -->
                </form>
            </div>
        </div>
@endsection