@extends ("layouts.base-min")

@section('extra-css')
    @parent
    {{-- BEGIN PAGE LEVEL STYLES --}}
    <link href="{{ asset('pages/css/error.min.css') }}" rel="stylesheet" type="text/css"/>
    {{-- END PAGE LEVEL STYLES --}}
@endsection

@push('extra-js')
{{-- BEGIN PAGE LEVEL SCRIPTS --}}

{{-- END PAGE LEVEL SCRIPTS --}}
@endpush

@section('main-content')
    <div class="row">
        <div class="col-md-12 page-500">
            <div class=" number font-red"> 500</div>
            <div class=" details">
                <h3>Oops! Something went wrong.</h3>

                <p> We are fixing it! Please come back in a while.
                    <br/></p>

                <p>
                    <a href="{{ url('/') }}" class="btn red btn-outline"> Return home </a>
                    <br></p>
            </div>
        </div>
    </div>
@endsection