@extends ("layouts.base")

@section('extra-css')
    @parent
    {{-- BEGIN THEME GLOBAL STYLES --}}
    <link href="{{ asset('css/plugins-md.min.css') }}" rel="stylesheet" type="text/css"/>
    {{-- END THEME GLOBAL STYLES --}}
@endsection

@push('extra-js')
{{-- BEGIN PAGE LEVEL PLUGINS --}}
<script src="{{ asset('plugins/jquery-inputmask/jquery.inputmask.bundle.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/jquery.input-ip-address-control-1.0.min.js') }}" type="text/javascript"></script>
{{-- END PAGE LEVEL PLUGINS --}}
{{-- BEGIN PAGE LEVEL SCRIPTS --}}
<script src="{{ asset('pages/js/form-input-mask.min.js') }}" type="text/javascript"></script>
{{-- END PAGE LEVEL SCRIPTS --}}
@endpush

@section('main-content')
    <div class="page-wrapper-row full-height">
        <div class="page-wrapper-middle">
            {{-- BEGIN CONTAINER --}}
            <div class="page-container">
                {{-- BEGIN CONTENT --}}
                <div class="page-content-wrapper">
                    {{-- BEGIN CONTENT BODY --}}
                    {{-- BEGIN PAGE HEAD--}}
                    <div class="page-head">
                        <div class="container">
                            {{-- BEGIN PAGE TITLE --}}
                            <div class="page-title">
                                <h1>Form Input Mask
                                    <small>form input mask samples</small>
                                </h1>
                            </div>
                            {{-- END PAGE TITLE --}}
                            {{-- BEGIN PAGE TOOLBAR --}}
                            @include('includes.toolbar')
                            {{-- END PAGE TOOLBAR --}}
                        </div>
                    </div>
                    {{-- END PAGE HEAD--}}
                    {{-- BEGIN PAGE CONTENT BODY --}}
                    <div class="page-content">
                        <div class="container">
                            {{-- BEGIN PAGE BREADCRUMBS --}}
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <a href="{{ url('/') }}">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <a href="#">More</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Form Stuff</span>
                                </li>
                            </ul>
                            {{-- END PAGE BREADCRUMBS --}}
                            {{-- BEGIN PAGE CONTENT INNER --}}
                            <div class="page-content-inner">
                                <div class="m-heading-1 border-green m-bordered">
                                    <p> jquery.inputmask is a jQuery plugin which creates an input mask. An inputmask helps
                                        the user with the input by ensuring a predefined format. This can be useful for
                                        dates, numerics, phone numbers. </p>

                                    <p> For more info please check out
                                        <a href="http://robinherbots.github.io/jquery.inputmask/" class="btn red btn-outline"
                                           target="_blank">the official documentation</a>
                                    </p>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        {{-- BEGIN PORTLET--}}
                                        <div class="portlet light form-fit ">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="icon-social-dribbble font-green"></i>
                                                    <span class="caption-subject font-green bold uppercase">Input Masks</span>
                                                </div>
                                                <div class="actions">
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-cloud-upload"></i>
                                                    </a>
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-wrench"></i>
                                                    </a>
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-trash"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body form">
                                                {{-- BEGIN FORM--}}
                                                <form action="#" class="form-horizontal form-bordered">
                                                    <div class="form-body">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Date 1</label>

                                                            <div class="col-md-4">
                                                                <input class="form-control" id="mask_date" type="text"/>
                                                                <span class="help-block"> d/m/y </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Date 2</label>

                                                            <div class="col-md-4">
                                                                <input class="form-control" id="mask_date1" type="text"/>
                                                                <span class="help-block"> change the placeholder </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Date 3</label>

                                                            <div class="col-md-4">
                                                                <input class="form-control" id="mask_date2" type="text"/>
                                                                <span class="help-block"> multi-char placeholder </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Phone</label>

                                                            <div class="col-md-4">
                                                                <input class="form-control" id="mask_phone" type="text"/>
                                                                <span class="help-block"> (999) 999-9999 </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Tax ID</label>

                                                            <div class="col-md-4">
                                                                <input class="form-control" id="mask_tin" type="text"/>
                                                                <span class="help-block"> 99-9999999 </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Number</label>

                                                            <div class="col-md-4">
                                                                <input class="form-control" id="mask_number" type="text"/>
                                                                <span class="help-block"> mask "9" or mask "99" or ... mask "9999999999" </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Currency</label>

                                                            <div class="col-md-4">
                                                                <input class="form-control" id="mask_currency" type="text"/>
                                                                <span class="help-block"> 123456 => &euro; ___.__1.234,56 </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Currency 2</label>

                                                            <div class="col-md-4">
                                                                <input class="form-control" id="mask_currency2" type="text"/>
                                                                <span class="help-block"> 123456 => &euro; ___.__1.234,56(left aligned) </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group last">
                                                            <label class="control-label col-md-3">SSN</label>

                                                            <div class="col-md-4">
                                                                <input class="form-control" id="mask_ssn" type="text"/>
                                                                <span class="help-block"> 999-99-9999. remove the empty mask on blur or when not empty removes the optional trailing part </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-offset-3 col-md-9">
                                                                <button type="submit" class="btn blue">
                                                                    <i class="fa fa-check"></i> Submit
                                                                </button>
                                                                <button type="button" class="btn default">Cancel</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                                {{-- END FORM--}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="m-heading-1 border-blue m-bordered">
                                    <p> Plugin controls the format of IPv4 or IPv6 addresses. </p>

                                    <p> For more info please check out
                                        <a href="https://github.com/felipevolpatto/jquery-input-ip-address-control"
                                           class="btn red btn-outline" target="_blank">the official documentation</a>
                                    </p>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet light form-fit ">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="icon-settings font-dark"></i>
                                                    <span class="caption-subject font-dark sbold uppercase">IP Address Input</span>
                                                </div>
                                                <div class="actions">
                                                    <div class="btn-group btn-group-devided" data-toggle="buttons">
                                                        <label class="btn dark btn-outline btn-circle btn-sm active">
                                                            <input type="radio" name="options" class="toggle" id="option1">Settings</label>
                                                        <label class="btn 	dark btn-outline btn-circle btn-sm">
                                                            <input type="radio" name="options" class="toggle" id="option2">Tools</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="portlet-body form">
                                                {{-- BEGIN FORM--}}
                                                <form action="#" class="form-horizontal form-row-seperated">
                                                    <div class="form-body">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">IPV4</label>

                                                            <div class="col-md-4">
                                                                <input class="form-control" id="input_ipv4" type="text"/>
                                                                <span class="help-block"> 192.168.120.150 </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group last">
                                                            <label class="control-label col-md-3">IPV6</label>

                                                            <div class="col-md-4">
                                                                <input class="form-control" id="input_ipv6" type="text"/>
                                                                <span class="help-block"> 3ffe:1900:4545:3:200:f8ff:fe21:67cf </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-offset-3 col-md-9">
                                                                <a href="javascript:;" class="btn green">
                                                                    <i class="fa fa-check"></i> Submit</a>
                                                                <a href="javascript:;" class="btn btn-outline grey-salsa">Cancel</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                                {{-- END FORM--}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- END PAGE CONTENT INNER --}}
                        </div>
                    </div>
                    {{-- END PAGE CONTENT BODY --}}
                    {{-- END CONTENT BODY --}}
                </div>
                {{-- END CONTENT --}}
                {{-- BEGIN QUICK SIDEBAR --}}
                @include('includes.sidebar')
                {{-- END QUICK SIDEBAR --}}
            </div>
            {{-- END CONTAINER --}}
        </div>
    </div>
@endsection