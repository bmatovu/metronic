@extends ("layouts.base")

@section('extra-css')
    @parent
    {{-- BEGIN THEME GLOBAL STYLES --}}
    <link href="{{ asset('css/plugins-md.min.css') }}" rel="stylesheet" type="text/css"/>
    {{-- END THEME GLOBAL STYLES --}}
@endsection

@section('main-content')
    <div class="page-wrapper-row full-height">
        <div class="page-wrapper-middle">
            {{-- BEGIN CONTAINER --}}
            <div class="page-container">
                {{-- BEGIN CONTENT --}}
                <div class="page-content-wrapper">
                    {{-- BEGIN CONTENT BODY --}}
                    {{-- BEGIN PAGE HEAD--}}
                    <div class="page-head">
                        <div class="container">
                            {{-- BEGIN PAGE TITLE --}}
                            <div class="page-title">
                                <h1>User Cards Elements
                                    <small>Elements are small components to be used within a layout</small>
                                </h1>
                            </div>
                            {{-- END PAGE TITLE --}}
                            {{-- BEGIN PAGE TOOLBAR --}}
                            @include('includes.toolbar')
                            {{-- END PAGE TOOLBAR --}}
                        </div>
                    </div>
                    {{-- END PAGE HEAD--}}
                    {{-- BEGIN PAGE CONTENT BODY --}}
                    <div class="page-content">
                        <div class="container">
                            {{-- BEGIN PAGE BREADCRUMBS --}}
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <a href="{{ url('/') }}">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <a href="#">More</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Elements</span>
                                </li>
                            </ul>
                            {{-- END PAGE BREADCRUMBS --}}
                            {{-- BEGIN PAGE CONTENT INNER --}}
                            <div class="page-content-inner">
                                {{-- BEGIN : USER CARDS --}}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet light portlet-fit ">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class=" icon-layers font-green"></i>
                                                    <span class="caption-subject font-green bold uppercase">Default</span>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="mt-element-card mt-element-overlay">
                                                    <div class="row">
                                                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                                            <div class="mt-card-item">
                                                                <div class="mt-card-avatar mt-overlay-1">
                                                                    <img src="{{ asset('pages/img/avatars/team1.jpg') }}"/>

                                                                    <div class="mt-overlay">
                                                                        <ul class="mt-info">
                                                                            <li>
                                                                                <a class="btn default btn-outline"
                                                                                   href="javascript:;">
                                                                                    <i class="icon-magnifier"></i>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a class="btn default btn-outline"
                                                                                   href="javascript:;">
                                                                                    <i class="icon-link"></i>
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                <div class="mt-card-content">
                                                                    <h3 class="mt-card-name">Mark Anthony</h3>

                                                                    <p class="mt-card-desc font-grey-mint">Managing
                                                                        Director</p>

                                                                    <div class="mt-card-social">
                                                                        <ul>
                                                                            <li>
                                                                                <a href="javascript:;">
                                                                                    <i class="icon-social-facebook"></i>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="javascript:;">
                                                                                    <i class="icon-social-twitter"></i>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="javascript:;">
                                                                                    <i class="icon-social-dribbble"></i>
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                                            <div class="mt-card-item">
                                                                <div class="mt-card-avatar mt-overlay-1 mt-scroll-down">
                                                                    <img src="{{ asset('pages/img/avatars/team2.jpg') }}"/>

                                                                    <div class="mt-overlay mt-top">
                                                                        <ul class="mt-info">
                                                                            <li>
                                                                                <a class="btn default btn-outline"
                                                                                   href="javascript:;">
                                                                                    <i class="icon-magnifier"></i>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a class="btn default btn-outline"
                                                                                   href="javascript:;">
                                                                                    <i class="icon-link"></i>
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                <div class="mt-card-content">
                                                                    <h3 class="mt-card-name">Denzel Wash</h3>

                                                                    <p class="mt-card-desc font-grey-mint">Finance
                                                                        Director</p>

                                                                    <div class="mt-card-social">
                                                                        <ul>
                                                                            <li>
                                                                                <a href="javascript:;">
                                                                                    <i class="icon-social-facebook"></i>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="javascript:;">
                                                                                    <i class="icon-social-twitter"></i>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="javascript:;">
                                                                                    <i class="icon-social-dribbble"></i>
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                                            <div class="mt-card-item">
                                                                <div class="mt-card-avatar mt-overlay-1 mt-scroll-up">
                                                                    <img src="{{ asset('pages/img/avatars/team3.jpg') }}"/>

                                                                    <div class="mt-overlay">
                                                                        <ul class="mt-info">
                                                                            <li>
                                                                                <a class="btn default btn-outline"
                                                                                   href="javascript:;">
                                                                                    <i class="icon-magnifier"></i>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a class="btn default btn-outline"
                                                                                   href="javascript:;">
                                                                                    <i class="icon-link"></i>
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                <div class="mt-card-content">
                                                                    <h3 class="mt-card-name">David Goodman</h3>

                                                                    <p class="mt-card-desc font-grey-mint">Creative
                                                                        Director</p>

                                                                    <div class="mt-card-social">
                                                                        <ul>
                                                                            <li>
                                                                                <a href="javascript:;">
                                                                                    <i class="icon-social-facebook"></i>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="javascript:;">
                                                                                    <i class="icon-social-twitter"></i>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="javascript:;">
                                                                                    <i class="icon-social-dribbble"></i>
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                                            <div class="mt-card-item">
                                                                <div class="mt-card-avatar mt-overlay-1 mt-scroll-left">
                                                                    <img src="{{ asset('pages/img/avatars/team4.jpg') }}"/>

                                                                    <div class="mt-overlay">
                                                                        <ul class="mt-info">
                                                                            <li>
                                                                                <a class="btn default btn-outline"
                                                                                   href="javascript:;">
                                                                                    <i class="icon-magnifier"></i>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a class="btn default btn-outline"
                                                                                   href="javascript:;">
                                                                                    <i class="icon-link"></i>
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                <div class="mt-card-content">
                                                                    <h3 class="mt-card-name">Lucy Ling</h3>

                                                                    <p class="mt-card-desc font-grey-mint">HR Director</p>

                                                                    <div class="mt-card-social">
                                                                        <ul>
                                                                            <li>
                                                                                <a href="javascript:;">
                                                                                    <i class="icon-social-facebook"></i>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="javascript:;">
                                                                                    <i class="icon-social-twitter"></i>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="javascript:;">
                                                                                    <i class="icon-social-dribbble"></i>
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet light portlet-fit ">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class=" icon-layers font-green"></i>
                                                    <span class="caption-subject font-green bold uppercase">Rounded</span>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="mt-element-card mt-card-round mt-element-overlay">
                                                    <div class="row">
                                                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                                            <div class="mt-card-item">
                                                                <div class="mt-card-avatar mt-overlay-1">
                                                                    <img src="{{ asset('pages/img/avatars/team5.jpg') }}"/>

                                                                    <div class="mt-overlay">
                                                                        <ul class="mt-info">
                                                                            <li>
                                                                                <a class="btn default btn-outline"
                                                                                   href="javascript:;">
                                                                                    <i class="icon-magnifier"></i>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a class="btn default btn-outline"
                                                                                   href="javascript:;">
                                                                                    <i class="icon-link"></i>
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                <div class="mt-card-content">
                                                                    <h3 class="mt-card-name">Jennifer Lawrence</h3>

                                                                    <p class="mt-card-desc font-grey-mint">Creative
                                                                        Director</p>

                                                                    <div class="mt-card-social">
                                                                        <ul>
                                                                            <li>
                                                                                <a href="javascript:;">
                                                                                    <i class="icon-social-facebook"></i>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="javascript:;">
                                                                                    <i class="icon-social-twitter"></i>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="javascript:;">
                                                                                    <i class="icon-social-dribbble"></i>
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                                            <div class="mt-card-item">
                                                                <div class="mt-card-avatar mt-overlay-1 mt-scroll-down">
                                                                    <img src="{{ asset('pages/img/avatars/team6.jpg') }}"/>

                                                                    <div class="mt-overlay mt-top">
                                                                        <ul class="mt-info">
                                                                            <li>
                                                                                <a class="btn default btn-outline"
                                                                                   href="javascript:;">
                                                                                    <i class="icon-magnifier"></i>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a class="btn default btn-outline"
                                                                                   href="javascript:;">
                                                                                    <i class="icon-link"></i>
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                <div class="mt-card-content">
                                                                    <h3 class="mt-card-name">Kate Beck</h3>

                                                                    <p class="mt-card-desc font-grey-mint">Executive
                                                                        Manager</p>

                                                                    <div class="mt-card-social">
                                                                        <ul>
                                                                            <li>
                                                                                <a href="javascript:;">
                                                                                    <i class="icon-social-facebook"></i>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="javascript:;">
                                                                                    <i class="icon-social-twitter"></i>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="javascript:;">
                                                                                    <i class="icon-social-dribbble"></i>
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                                            <div class="mt-card-item">
                                                                <div class="mt-card-avatar mt-overlay-1 mt-scroll-up">
                                                                    <img src="{{ asset('pages/img/avatars/team7.jpg') }}"/>

                                                                    <div class="mt-overlay">
                                                                        <ul class="mt-info">
                                                                            <li>
                                                                                <a class="btn default btn-outline"
                                                                                   href="javascript:;">
                                                                                    <i class="icon-magnifier"></i>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a class="btn default btn-outline"
                                                                                   href="javascript:;">
                                                                                    <i class="icon-link"></i>
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                <div class="mt-card-content">
                                                                    <h3 class="mt-card-name">Hugh Jackman</h3>

                                                                    <p class="mt-card-desc font-grey-mint">Human Resource</p>

                                                                    <div class="mt-card-social">
                                                                        <ul>
                                                                            <li>
                                                                                <a href="javascript:;">
                                                                                    <i class="icon-social-facebook"></i>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="javascript:;">
                                                                                    <i class="icon-social-twitter"></i>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="javascript:;">
                                                                                    <i class="icon-social-dribbble"></i>
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                                            <div class="mt-card-item">
                                                                <div class="mt-card-avatar mt-overlay-1 mt-scroll-left">
                                                                    <img src="{{ asset('pages/img/avatars/team8.jpg') }}"/>

                                                                    <div class="mt-overlay">
                                                                        <ul class="mt-info">
                                                                            <li>
                                                                                <a class="btn default btn-outline"
                                                                                   href="javascript:;">
                                                                                    <i class="icon-magnifier"></i>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a class="btn default btn-outline"
                                                                                   href="javascript:;">
                                                                                    <i class="icon-link"></i>
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                <div class="mt-card-content">
                                                                    <h3 class="mt-card-name">Gwen Parker</h3>

                                                                    <p class="mt-card-desc font-grey-mint">Finance
                                                                        Manager</p>

                                                                    <div class="mt-card-social">
                                                                        <ul>
                                                                            <li>
                                                                                <a href="javascript:;">
                                                                                    <i class="icon-social-facebook"></i>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="javascript:;">
                                                                                    <i class="icon-social-twitter"></i>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="javascript:;">
                                                                                    <i class="icon-social-dribbble"></i>
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet light portlet-fit ">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class=" icon-layers font-green"></i>
                                                    <span class="caption-subject font-green bold uppercase">Profile</span>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="mt-element-card mt-element-overlay">
                                                    <div class="row">
                                                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                                            <div class="mt-card-item">
                                                                <div class="mt-card-avatar mt-overlay-4">
                                                                    <img src="{{ asset('pages/img/avatars/team1.jpg') }}"/>

                                                                    <div class="mt-overlay">
                                                                        <h2>Mark Anthony</h2>

                                                                        <div class="mt-info font-white">
                                                                            <div class="mt-card-content">
                                                                                <p class="mt-card-desc font-white">Managing
                                                                                    Director</p>

                                                                                <div class="mt-card-social">
                                                                                    <ul>
                                                                                        <li>
                                                                                            <a class="mt-card-btn"
                                                                                               href="javascript:;">
                                                                                                <i class="icon-social-facebook"></i>
                                                                                            </a>
                                                                                        </li>
                                                                                        <li>
                                                                                            <a class="mt-card-btn"
                                                                                               href="javascript:;">
                                                                                                <i class="icon-social-twitter"></i>
                                                                                            </a>
                                                                                        </li>
                                                                                        <li>
                                                                                            <a class="mt-card-btn"
                                                                                               href="javascript:;">
                                                                                                <i class="icon-social-dribbble"></i>
                                                                                            </a>
                                                                                        </li>
                                                                                    </ul>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                                            <div class="mt-card-item">
                                                                <div class="mt-card-avatar mt-overlay-4">
                                                                    <img src="{{ asset('pages/img/avatars/team2.jpg') }}"/>

                                                                    <div class="mt-overlay">
                                                                        <h2>Denzel Wash</h2>

                                                                        <div class="mt-info font-white">
                                                                            <div class="mt-card-content">
                                                                                <p class="mt-card-desc font-white">Finance
                                                                                    Director</p>

                                                                                <div class="mt-card-social">
                                                                                    <ul>
                                                                                        <li>
                                                                                            <a class="mt-card-btn"
                                                                                               href="javascript:;">
                                                                                                <i class="icon-social-facebook"></i>
                                                                                            </a>
                                                                                        </li>
                                                                                        <li>
                                                                                            <a class="mt-card-btn"
                                                                                               href="javascript:;">
                                                                                                <i class="icon-social-twitter"></i>
                                                                                            </a>
                                                                                        </li>
                                                                                        <li>
                                                                                            <a class="mt-card-btn"
                                                                                               href="javascript:;">
                                                                                                <i class="icon-social-dribbble"></i>
                                                                                            </a>
                                                                                        </li>
                                                                                    </ul>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                                            <div class="mt-card-item">
                                                                <div class="mt-card-avatar mt-overlay-4">
                                                                    <img src="{{ asset('pages/img/avatars/team3.jpg') }}"/>

                                                                    <div class="mt-overlay">
                                                                        <h2>David Goodman</h2>

                                                                        <div class="mt-info font-white">
                                                                            <div class="mt-card-content">
                                                                                <p class="mt-card-desc font-white">Creative
                                                                                    Director</p>

                                                                                <div class="mt-card-social">
                                                                                    <ul>
                                                                                        <li>
                                                                                            <a class="mt-card-btn"
                                                                                               href="javascript:;">
                                                                                                <i class="icon-social-facebook"></i>
                                                                                            </a>
                                                                                        </li>
                                                                                        <li>
                                                                                            <a class="mt-card-btn"
                                                                                               href="javascript:;">
                                                                                                <i class="icon-social-twitter"></i>
                                                                                            </a>
                                                                                        </li>
                                                                                        <li>
                                                                                            <a class="mt-card-btn"
                                                                                               href="javascript:;">
                                                                                                <i class="icon-social-dribbble"></i>
                                                                                            </a>
                                                                                        </li>
                                                                                    </ul>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                                            <div class="mt-card-item">
                                                                <div class="mt-card-avatar mt-overlay-4">
                                                                    <img src="{{ asset('pages/img/avatars/team4.jpg') }}"/>

                                                                    <div class="mt-overlay">
                                                                        <h2>Lucy Ling</h2>

                                                                        <div class="mt-info font-white">
                                                                            <div class="mt-card-content">
                                                                                <p class="mt-card-desc font-white">HR
                                                                                    Director</p>

                                                                                <div class="mt-card-social">
                                                                                    <ul>
                                                                                        <li>
                                                                                            <a class="mt-card-btn"
                                                                                               href="javascript:;">
                                                                                                <i class="icon-social-facebook"></i>
                                                                                            </a>
                                                                                        </li>
                                                                                        <li>
                                                                                            <a class="mt-card-btn"
                                                                                               href="javascript:;">
                                                                                                <i class="icon-social-twitter"></i>
                                                                                            </a>
                                                                                        </li>
                                                                                        <li>
                                                                                            <a class="mt-card-btn"
                                                                                               href="javascript:;">
                                                                                                <i class="icon-social-dribbble"></i>
                                                                                            </a>
                                                                                        </li>
                                                                                    </ul>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- END : USER CARDS --}}
                            </div>
                            {{-- END PAGE CONTENT INNER --}}
                        </div>
                    </div>
                    {{-- END PAGE CONTENT BODY --}}
                    {{-- END CONTENT BODY --}}
                </div>
                {{-- END CONTENT --}}
                {{-- BEGIN QUICK SIDEBAR --}}
                @include('includes.toolbar')
                {{-- END QUICK SIDEBAR --}}
            </div>
            {{-- END CONTAINER --}}
        </div>
    </div>
@endsection