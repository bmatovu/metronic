@extends ("layouts.base")

@section('extra-css')
    @parent
    {{-- BEGIN PAGE LEVEL PLUGINS --}}
    <link href="{{ asset('plugins/bootstrap-colorpicker/css/colorpicker.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('plugins/jquery-minicolors/jquery.minicolors.css') }}" rel="stylesheet" type="text/css"/>
    {{-- END PAGE LEVEL PLUGINS --}}
    {{-- BEGIN THEME GLOBAL STYLES --}}
    <link href="{{ asset('css/plugins-md.min.css') }}" rel="stylesheet" type="text/css"/>
    {{-- END THEME GLOBAL STYLES --}}
@endsection

@push('extra-js')
{{-- BEGIN PAGE LEVEL PLUGINS --}}
<script src="{{ asset('plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/jquery-minicolors/jquery.minicolors.min.js') }}" type="text/javascript"></script>
{{-- END PAGE LEVEL PLUGINS --}}
{{-- BEGIN PAGE LEVEL SCRIPTS --}}
<script src="{{ asset('pages/js/components-color-pickers.min.js') }}" type="text/javascript"></script>
{{-- END PAGE LEVEL SCRIPTS --}}
@endpush

@section('main-content')
    <div class="page-wrapper-row full-height">
        <div class="page-wrapper-middle">
            {{-- BEGIN CONTAINER --}}
            <div class="page-container">
                {{-- BEGIN CONTENT --}}
                <div class="page-content-wrapper">
                    {{-- BEGIN CONTENT BODY --}}
                    {{-- BEGIN PAGE HEAD--}}
                    <div class="page-head">
                        <div class="container">
                            {{-- BEGIN PAGE TITLE --}}
                            <div class="page-title">
                                <h1>Color Pickers
                                    <small>advanced color pickers</small>
                                </h1>
                            </div>
                            {{-- END PAGE TITLE --}}
                            {{-- BEGIN PAGE TOOLBAR --}}
                            @include('includes.toolbar')
                            {{-- END PAGE TOOLBAR --}}
                        </div>
                    </div>
                    {{-- END PAGE HEAD--}}
                    {{-- BEGIN PAGE CONTENT BODY --}}
                    <div class="page-content">
                        <div class="container">
                            {{-- BEGIN PAGE BREADCRUMBS --}}
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <a href="{{ url('/') }}">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <a href="#">Components</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Color Pickers</span>
                                </li>
                            </ul>
                            {{-- END PAGE BREADCRUMBS --}}
                            {{-- BEGIN PAGE CONTENT INNER --}}
                            <div class="page-content-inner">
                                <div class="row">
                                    <div class="col-md-12">
                                        {{-- BEGIN PORTLET--}}
                                        <div class="portlet light  form-fit">
                                            <div class="portlet-title">
                                                <div class="caption font-red-intense">
                                                    <i class="icon-speech font-red-intense"></i>
                                                    <span class="caption-subject bold uppercase">jQuery MiniColors</span>
                                                    <span class="caption-helper"></span>
                                                </div>
                                                <div class="actions">
                                                    <a href="javascript:;" class="btn btn-circle btn-default btn-sm">
                                                        <i class="fa fa-pencil"></i> Edit </a>
                                                    <a href="javascript:;" class="btn btn-circle btn-default btn-sm">
                                                        <i class="fa fa-plus"></i> Add </a>
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-wrench"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body form">
                                                <form action="#" id="form-username" class="form-horizontal form-bordered">
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Hue (default)</label>

                                                        <div class="col-md-3">
                                                            <input type="text" id="hue-demo" class="form-control demo"
                                                                   data-control="hue" value="#ff6161"></div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Saturation</label>

                                                        <div class="col-md-3">
                                                            <input type="text" id="saturation-demo" class="form-control demo"
                                                                   data-control="saturation" value="#0088cc"></div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Brightness</label>

                                                        <div class="col-md-3">
                                                            <input type="text" id="brightness-demo" class="form-control demo"
                                                                   data-control="brightness" value="#00ffff"></div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Wheel</label>

                                                        <div class="col-md-3">
                                                            <input type="text" id="wheel-demo" class="form-control demo"
                                                                   data-control="wheel" value="#ff99ee"></div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Hidden Input</label>

                                                        <div class="col-md-3">
                                                            <input type="hidden" id="hidden-input" class="demo"
                                                                   value="#db913d"></div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Bottom left (default)</label>

                                                        <div class="col-md-3">
                                                            <input type="text" id="position-bottom-left"
                                                                   class="form-control demo" data-position="bottom left"
                                                                   value="#0088cc"></div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Bottom right</label>

                                                        <div class="col-md-3">
                                                            <input type="text" id="position-top-left"
                                                                   class="form-control demo" data-position="top left"
                                                                   value="#0088cc"></div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Top left</label>

                                                        <div class="col-md-3">
                                                            <input type="text" id="position-bottom-right"
                                                                   class="form-control demo" data-position="bottom right"
                                                                   value="#0088cc"></div>
                                                    </div>
                                                    <div class="form-group last">
                                                        <label class="col-md-3 control-label">Top right</label>

                                                        <div class="col-md-3">
                                                            <input type="text" id="position-top-right"
                                                                   class="form-control demo" data-position="top right"
                                                                   value="#0088cc"></div>
                                                    </div>
                                                    <div class="form-actions">
                                                        <div class="row">
                                                            <div class="col-md-offset-3 col-md-9">
                                                                <button type="submit" class="btn green">
                                                                    <i class="fa fa-check"></i> Submit
                                                                </button>
                                                                <button type="button" class="btn default">Cancel</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        {{-- END PORTLET--}}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        {{-- BEGIN PORTLET--}}
                                        <div class="portlet light form-fit ">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i class="icon-social-dribbble font-green"></i>
                                                    <span class="caption-subject font-green bold uppercase">Bootstrap Color Pickers</span>
                                                </div>
                                                <div class="actions">
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-cloud-upload"></i>
                                                    </a>
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-wrench"></i>
                                                    </a>
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-trash"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body form">
                                                {{-- BEGIN FORM--}}
                                                <form action="#" class="form-horizontal form-bordered">
                                                    <div class="form-body">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Default</label>

                                                            <div class="col-md-3">
                                                                <input type="text" class="colorpicker-default form-control"
                                                                       value="#8fff00"/></div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">RGBA</label>

                                                            <div class="col-md-3">
                                                                <input type="text" class="colorpicker-rgba form-control"
                                                                       value="rgb(0,194,255,0.78)" data-color-format="rgba"/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">As Component</label>

                                                            <div class="col-md-3">
                                                                <div class="input-group color colorpicker-default"
                                                                     data-color="#3865a8" data-color-format="rgba">
                                                                    <input type="text" class="form-control" value="#3865a8"
                                                                           readonly>
                                                                            <span class="input-group-btn">
                                                                                <button class="btn default" type="button">
                                                                                    <i style="background-color: #3865a8;"></i>&nbsp;
                                                                                </button>
                                                                            </span>
                                                                </div>
                                                                {{-- /input-group --}}
                                                            </div>
                                                        </div>
                                                        <div class="form-group last">
                                                            <label class="control-label col-md-3"></label>

                                                            <div class="col-md-4">
                                                                <a class="btn dark btn-outline" href="#form_modal3"
                                                                   data-toggle="modal"> View colorpicker in modal
                                                                    <i class="fa fa-share"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                                <div id="form_modal3" class="modal fade" role="dialog" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                        aria-hidden="true"></button>
                                                                <h4 class="modal-title">Colorpicker in Modal</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form action="#" class="form-horizontal">
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">Default</label>

                                                                        <div class="col-md-3">
                                                                            <input type="text"
                                                                                   class="colorpicker-default form-control"
                                                                                   value="#8fff00"/></div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="control-label col-md-3">RGBA</label>

                                                                        <div class="col-md-3">
                                                                            <input type="text"
                                                                                   class="colorpicker-rgba form-control"
                                                                                   value="rgb(0,194,255,0.78)"
                                                                                   data-color-format="rgba"/></div>
                                                                    </div>
                                                                    <div class="form-group last">
                                                                        <label class="control-label col-md-3">As
                                                                            Component</label>

                                                                        <div class="col-md-3">
                                                                            <div class="input-group color colorpicker-default"
                                                                                 data-color="#3865a8"
                                                                                 data-color-format="rgba">
                                                                                <input type="text" class="form-control"
                                                                                       value="#3865a8" readonly>
                                                                                        <span class="input-group-btn">
                                                                                            <button class="btn default"
                                                                                                    type="button">
                                                                                                <i style="background-color: #3865a8;"></i>&nbsp;
                                                                                            </button>
                                                                                        </span>
                                                                            </div>
                                                                            {{-- /input-group --}}
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button class="btn dark btn-outline" data-dismiss="modal"
                                                                        aria-hidden="true">Close
                                                                </button>
                                                                <button class="btn green btn-primary" data-dismiss="modal">
                                                                    Save changes
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                {{-- END FORM--}}
                                            </div>
                                        </div>
                                        {{-- END PORTLET--}}
                                    </div>
                                </div>
                            </div>
                            {{-- END PAGE CONTENT INNER --}}
                        </div>
                    </div>
                    {{-- END PAGE CONTENT BODY --}}
                    {{-- END CONTENT BODY --}}
                </div>
                {{-- END CONTENT --}}
                {{-- BEGIN QUICK SIDEBAR --}}
                @include('includes.sidebar')
                {{-- END QUICK SIDEBAR --}}
            </div>
            {{-- END CONTAINER --}}
        </div>
    </div>
@endsection