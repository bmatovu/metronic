@extends ("layouts.base-min")

@section('extra-css')
    @parent
    {{-- BEGIN PAGE LEVEL STYLES --}}
    <link href="{{ asset('pages/css/lock-2.min.css') }}" rel="stylesheet" type="text/css"/>
    {{-- END PAGE LEVEL STYLES --}}
@endsection

@push('extra-js')
{{-- BEGIN PAGE LEVEL SCRIPTS --}}
<script src="{{ asset('plugins/backstretch/jquery.backstretch.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('pages/js/lock-2.min.js') }}" type="text/javascript"></script>
{{-- END PAGE LEVEL SCRIPTS --}}
@endpush

@section('main-content')
    <div class="page-lock">
        <div class="page-logo">
            <a class="brand" href="{{ url('/') }}">
                <img src="{{ asset('pages/img/logo-big.png') }}" alt="logo"/> </a>
        </div>
        <div class="page-body">
            <img class="page-lock-img" src="{{ asset('pages/media/profile/profile.jpg') }}" alt="">

            <div class="page-lock-info">
                <h1>Bob Nilson</h1>
                <span class="email"> bob@keenthemes.com </span>
                <span class="locked"> Locked </span>

                <form class="form-inline" action="{{ url('/') }}">
                    <div class="input-group input-medium">
                        <input type="text" class="form-control" placeholder="Password">
                            <span class="input-group-btn">
                                <button type="submit" class="btn green icn-only">
                                    <i class="m-icon-swapright m-icon-white"></i>
                                </button>
                            </span>
                    </div>
                    {{-- /input-group --}}
                    <div class="relogin">
                        <a href="{{ url('/login') }}"> Not Bob Nilson ? </a>
                    </div>
                </form>
            </div>
        </div>
        <div class="page-footer-custom"> 2014 &copy; Metronic. Admin Dashboard Template.</div>
    </div>
@endsection