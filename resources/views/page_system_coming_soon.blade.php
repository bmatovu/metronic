@extends ("layouts.base-min")

@section('extra-css')
    @parent
    {{-- BEGIN PAGE LEVEL STYLES --}}
    <link href="{{ asset('pages/css/coming-soon.min.css') }}" rel="stylesheet" type="text/css" />
    {{-- END PAGE LEVEL STYLES --}}
@endsection

@push('extra-js')
{{-- BEGIN PAGE LEVEL SCRIPTS --}}
<script src="{{ asset('plugins/countdown/jquery.countdown.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('plugins/backstretch/jquery.backstretch.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('pages/js/coming-soon.min.js') }}" type="text/javascript"></script>
{{-- END PAGE LEVEL SCRIPTS --}}
@endpush

@section('main-content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 coming-soon-header">
                <a class="brand" href="{{ url('/') }}">
                    <img src="{{ asset('pages/img/logo-big.png') }}" alt="logo" /> </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 coming-soon-content">
                <h1>Coming Soon!</h1>
                <p> At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi vehicula sem ut volutpat. Ut non libero magna fusce condimentum eleifend
                    enim a feugiat. </p>
                <br>
                <form class="form-inline" action="#">
                    <div class="input-group input-group-lg input-large">
                        <input type="text" class="form-control">
                            <span class="input-group-btn">
                                <button class="btn green" type="button"> Subscribe </button>
                            </span>
                    </div>
                </form>
                <ul class="social-icons margin-top-20">
                    <li>
                        <a href="javascript:;" data-original-title="Feed" class="rss"> </a>
                    </li>
                    <li>
                        <a href="javascript:;" data-original-title="Facebook" class="facebook"> </a>
                    </li>
                    <li>
                        <a href="javascript:;" data-original-title="Twitter" class="twitter"> </a>
                    </li>
                    <li>
                        <a href="javascript:;" data-original-title="Goole Plus" class="googleplus"> </a>
                    </li>
                    <li>
                        <a href="javascript:;" data-original-title="Pinterest" class="pintrest"> </a>
                    </li>
                    <li>
                        <a href="javascript:;" data-original-title="Linkedin" class="linkedin"> </a>
                    </li>
                    <li>
                        <a href="javascript:;" data-original-title="Vimeo" class="vimeo"> </a>
                    </li>
                </ul>
            </div>
            <div class="col-md-6 coming-soon-countdown">
                <div id="defaultCountdown"> </div>
            </div>
        </div>
        {{--/end row--}}
        <div class="row">
            <div class="col-md-12 coming-soon-footer"> 2014 &copy; Metronic. Admin Dashboard Template. </div>
        </div>
    </div>
@endsection