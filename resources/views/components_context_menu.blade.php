@extends ("layouts.base")

@section('extra-css')
    @parent
    {{-- BEGIN THEME GLOBAL STYLES --}}
    <link href="{{ asset('css/plugins-md.min.css') }}" rel="stylesheet" type="text/css"/>
    {{-- END THEME GLOBAL STYLES --}}
@endsection

@push('extra-js')
{{-- BEGIN PAGE LEVEL PLUGINS --}}
<script src="{{ asset('plugins/bootstrap-contextmenu/bootstrap-contextmenu.js') }}" type="text/javascript"></script>
{{-- END PAGE LEVEL PLUGINS --}}
{{-- BEGIN PAGE LEVEL SCRIPTS --}}
<script src="{{ asset('pages/js/components-context-menu.min.js') }}" type="text/javascript"></script>
{{-- END PAGE LEVEL SCRIPTS --}}
@endpush

@section('main-content')
    <div class="page-wrapper-row full-height">
        <div class="page-wrapper-middle">
            {{-- BEGIN CONTAINER --}}
            <div class="page-container">
                {{-- BEGIN CONTENT --}}
                <div class="page-content-wrapper">
                    {{-- BEGIN CONTENT BODY --}}
                    {{-- BEGIN PAGE HEAD--}}
                    <div class="page-head">
                        <div class="container">
                            {{-- BEGIN PAGE TITLE --}}
                            <div class="page-title">
                                <h1>Context Menu
                                    <small>context menu based on bootstrap dropdown menu</small>
                                </h1>
                            </div>
                            {{-- END PAGE TITLE --}}
                            {{-- BEGIN PAGE TOOLBAR --}}
                            @include('includes.toolbar')
                            {{-- END PAGE TOOLBAR --}}
                        </div>
                    </div>
                    {{-- END PAGE HEAD--}}
                    {{-- BEGIN PAGE CONTENT BODY --}}
                    <div class="page-content">
                        <div class="container">
                            {{-- BEGIN PAGE BREADCRUMBS --}}
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <a href="{{ url('/') }}">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <a href="#">Components</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Context Menu</span>
                                </li>
                            </ul>
                            {{-- END PAGE BREADCRUMBS --}}
                            {{-- BEGIN PAGE CONTENT INNER --}}
                            <div class="page-content-inner">
                                <div class="m-heading-1 border-green m-bordered">
                                    <h3>Bootstrap Context Menu</h3>

                                    <p> A context menu extension of Bootstrap made for everyone's convenience. For more info
                                        please check out
                                        <a href="https://github.com/sydcanem/bootstrap-contextmenu" target="_blank">the
                                            official documentation</a>. </p>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        {{-- BEGIN PORTLET--}}
                                        <div class="portlet light ">
                                            <div class="portlet-title">
                                                <div class="caption font-purple-plum">
                                                    <i class="icon-speech font-purple-plum"></i>
                                                    <span class="caption-subject bold uppercase"> Demo 1</span>
                                                    <span class="caption-helper">right click inside the box</span>
                                                </div>
                                                <div class="actions">
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-cloud-upload"></i>
                                                    </a>
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-wrench"></i>
                                                    </a>
                                                    <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                                        <i class="icon-trash"></i>
                                                    </a>
                                                    <a class="btn btn-circle btn-icon-only btn-default fullscreen"
                                                       href="javascript:;" data-original-title="" title=""> </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div id="context" data-toggle="context" data-target="#context-menu">
                                                    <p> Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget
                                                        lacinia odio sem nec elit. Cras mattis consectetur purus sit amet
                                                        fermentum. Duis mollis, est non commodo luctus, nisi erat
                                                        porttitor ligula, eget lacinia odio sem nec elit. Cras mattis
                                                        consectetur purus sit. </p>
                                                </div>
                                                {{-- Your custom menu with dropdown-menu as default styling --}}
                                                <div id="context-menu">
                                                    <ul class="dropdown-menu" role="menu">
                                                        <li>
                                                            <a href="javascript:;">
                                                                <i class="icon-user"></i> New User </a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:;">
                                                                <i class="icon-present"></i> New Event
                                                                <span class="badge badge-success">4</span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:;">
                                                                <i class="icon-basket"></i> New order </a>
                                                        </li>
                                                        <li class="divider"></li>
                                                        <li>
                                                            <a href="javascript:;">
                                                                <i class="icon-flag"></i> Pending Orders
                                                                <span class="badge badge-danger">4</span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:;">
                                                                <i class="icon-users"></i> Pending Users
                                                                <span class="badge badge-warning">12</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- END PORTLET--}}
                                    </div>
                                    <div class="col-md-6">
                                        {{-- BEGIN PORTLET--}}
                                        <div class="portlet light ">
                                            <div class="portlet-title">
                                                <div class="caption font-purple-plum">
                                                    <i class="icon-speech font-purple-plum"></i>
                                                    <span class="caption-subject bold uppercase"> Demo 2</span>
                                                    <span class="caption-helper">right click inside the box</span>
                                                </div>
                                                <div class="actions">
                                                    <div class="btn-group">
                                                        <a class="btn btn-circle btn-default btn-sm" href="javascript:;"
                                                           data-toggle="dropdown">
                                                            <i class="fa fa-user"></i> User
                                                            <i class="fa fa-angle-down"></i>
                                                        </a>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li>
                                                                <a href="javascript:;">
                                                                    <i class="icon-user"></i> New User </a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;">
                                                                    <i class="icon-present"></i> New Event
                                                                    <span class="badge badge-success">4</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;">
                                                                    <i class="icon-basket"></i> New order </a>
                                                            </li>
                                                            <li class="divider"></li>
                                                            <li>
                                                                <a href="javascript:;">
                                                                    <i class="icon-flag"></i> Pending Orders
                                                                    <span class="badge badge-danger">4</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="javascript:;">
                                                                    <i class="icon-users"></i> Pending Users
                                                                    <span class="badge badge-warning">12</span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <a href="javascript:;" class="btn btn-circle red-sunglo btn-sm">
                                                        <i class="fa fa-plus"></i> Add </a>
                                                    <a class="btn btn-circle btn-icon-only btn-default fullscreen"
                                                       href="javascript:;" data-original-title="" title=""> </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div id="main" data-toggle="context"> This is an area where the context menu
                                                    is active.
                                                    <span class="label label-danger">However, we wont allow it here.</span>
                                                    But anywhere else in this text should be perfectly fine. This one is
                                                    started with only javascript
                                                </div>
                                                <div id="context-menu2">
                                                    <ul class="dropdown-menu pull-left" role="menu">
                                                        <li>
                                                            <a href="javascript:;">
                                                                <i class="icon-user"></i> New User </a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:;">
                                                                <i class="icon-present"></i> New Event
                                                                <span class="badge badge-success">4</span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:;">
                                                                <i class="icon-basket"></i> New order </a>
                                                        </li>
                                                        <li class="divider"></li>
                                                        <li>
                                                            <a href="javascript:;">
                                                                <i class="icon-flag"></i> Pending Orders
                                                                <span class="badge badge-danger">4</span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:;">
                                                                <i class="icon-users"></i> Pending Users
                                                                <span class="badge badge-warning">12</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- END PORTLET--}}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        {{-- BEGIN PORTLET--}}
                                        <div class="portlet light ">
                                            <div class="portlet-title">
                                                <div class="caption font-red-intense">
                                                    <i class="icon-speech font-red-intense"></i>
                                                    <span class="caption-subject bold uppercase"> Demo 3</span>
                                                    <span class="caption-helper">fetch clicked menu item</span>
                                                </div>
                                                <div class="tools">
                                                    <a href="" class="collapse" data-original-title="" title=""> </a>
                                                    <a href="#portlet-config" data-toggle="modal" class="config"
                                                       data-original-title="" title=""> </a>
                                                    <a href="" class="reload" data-original-title="" title=""> </a>
                                                    <a href="" class="fullscreen" data-original-title="" title=""> </a>
                                                    <a href="" class="remove" data-original-title="" title=""> </a>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div id="context2" data-toggle="context" data-target="#context-menu">
                                                    <p> Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget
                                                        lacinia odio sem nec elit. Cras mattis consectetur purus sit amet
                                                        fermentum. Duis mollis, est non commodo luctus, nisi erat
                                                        porttitor ligula, eget lacinia odio sem nec elit. Cras mattis
                                                        consectetur purus sit. </p>
                                                </div>
                                                {{-- Your custom menu with dropdown-menu as default styling --}}
                                                <div id="context-menu">
                                                    <ul class="dropdown-menu" role="menu">
                                                        <li>
                                                            <a href="javascript:;">
                                                                <i class="icon-user"></i> New User </a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:;">
                                                                <i class="icon-present"></i> New Event
                                                                <span class="badge badge-success">4</span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:;">
                                                                <i class="icon-basket"></i> New order </a>
                                                        </li>
                                                        <li class="divider"></li>
                                                        <li>
                                                            <a href="javascript:;">
                                                                <i class="icon-flag"></i> Pending Orders
                                                                <span class="badge badge-danger">4</span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:;">
                                                                <i class="icon-users"></i> Pending Users
                                                                <span class="badge badge-warning">12</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- END PORTLET--}}
                                    </div>
                                </div>
                            </div>
                            {{-- END PAGE CONTENT INNER --}}
                        </div>
                    </div>
                    {{-- END PAGE CONTENT BODY --}}
                    {{-- END CONTENT BODY --}}
                </div>
                {{-- END CONTENT --}}
                {{-- BEGIN QUICK SIDEBAR --}}
                @include('includes.sidebar')
                {{-- END QUICK SIDEBAR --}}
            </div>
            {{-- END CONTAINER --}}
        </div>
    </div>
@endsection